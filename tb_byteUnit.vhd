LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE IEEE.std_logic_textio.ALL;
USE STD.textio.ALL;

ENTITY tb_byte IS
END tb_byte;

ARCHITECTURE byte OF tb_byte IS
  
  FILE logger : TEXT OPEN WRITE_MODE IS "transcript_byte.log";
  
  COMPONENT byte
    PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    clk: IN STD_ULOGIC;
    enable : IN STD_LOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_LOGIC);
  END COMPONENT;
  
  SIGNAL ra : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rb : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rc : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rt : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL opcode : STD_LOGIC_VECTOR(0 TO 4);
  SIGNAL result : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL zero : STD_LOGIC;
  SIGNAL clk:  STD_LOGIC :='1';
  SIGNAL enable: STD_LOGIC := '1';
  
  PROCEDURE print(opcode : STD_LOGIC_VECTOR(0 TO 4);
                  ra : STD_LOGIC_VECTOR(0 TO 127);
                  rb : STD_LOGIC_VECTOR(0 TO 127); 
                  rc : STD_LOGIC_VECTOR(0 TO 127);
                  rt : STD_LOGIC_VECTOR(0 TO 127);               
                  result : STD_LOGIC_VECTOR(0 TO 127);
                  zero : STD_ULOGIC) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
  BEGIN
    swrite(line_out, "opcode = ");
    write(line_out, opcode);
    writeline(logger, line_out);
    swrite(line_out, "ra =     ");
    hwrite(line_out, ra);
    writeline(logger, line_out);
    swrite(line_out, "rb =     ");
    hwrite(line_out, rb);
    writeline(logger, line_out);
    swrite(line_out, "rc =     ");
    hwrite(line_out, rc);
    writeline(logger, line_out);
    swrite(line_out, "rt =     ");
    hwrite(line_out, rt);
    writeline(logger, line_out);
    swrite(line_out, "result = ");
    hwrite(line_out, result);
    writeline(logger, line_out);
    swrite(line_out, "zero =   ");
    write(line_out, zero);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;
  
  -- definition of a clock period
  CONSTANT clk_period : time := 10 ns;
  
BEGIN
  -- component instantiation
  BYTEUNIT: byte PORT MAP (
    ra => ra,
    rb => rb,
    rc => rc,
    rt => rt,
    clk => clk,
    enable => enable,
    opcode => opcode,
    result => result,
    zero => zero);
    
    CLK_PROCESS : PROCESS
    BEGIN
				  
		  clk <= '0';
		  WAIT FOR clk_period/2;
		    
		  clk <= '1';
		  WAIT FOR clk_period/2;

    END PROCESS;
    
    SIMULATION : PROCESS
    BEGIN
      enable <= '1';
      
      -- (1) cntb (Count Ones in Bytes)
      opcode <= "00001";
      ra <= X"0000000000000000000000000000000F";
      rb <= X"00000000000000000000000000000000";
      rc(0 TO 127) <= (OTHERS => 'X');
      rt(0 TO 127) <= (OTHERS => 'X');
     -- WAIT FOR 10 ns;
     -- print(opcode, ra, rb, rc, rt, result, zero);
      
      -- (2) sumb (Sum Bytes Into Halfwords)
    --  opcode <= "00010";
    --  ra <= X"0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F";
    --  rb <= X"F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0";
   --   rc(0 TO 127) <= (OTHERS => 'X');
   --   rt(0 TO 127) <= (OTHERS => 'X');
      WAIT FOR 10 ns;
      print(opcode, ra, rb, rc, rt, result, zero);
      
      -- (3) ceqb (Compare Equal Bytes)
      opcode <= "00011";
      ra <= X"FF00FF00FF00FF00FF00FF00FF00FF00";
      rb <= X"00000000000000000000000000000000";
      rc(0 TO 127) <= (OTHERS => 'X');
      rt(0 TO 127) <= (OTHERS => 'X');
      WAIT FOR 10 ns;
      print(opcode, ra, rb, rc, rt, result, zero);
      
      -- (4) rotqby (Rotate Quadword by Bytes)
      opcode <= "00100";
      ra <= X"F0000000000000000000000000000000";
      rb(32 TO 127) <= X"000000000000000000000000";
      rb(24 TO 31) <= "00000001";
      rb(0 TO 23) <= X"000000";
      rc(0 TO 127) <= (OTHERS => 'X');
      rt(0 TO 127) <= (OTHERS => 'X');
      WAIT FOR 10 ns;
      print(opcode, ra, rb, rc, rt, result, zero);
      
      -- (5) shlqby (Shift Left Quadword by Bytes)
      opcode <= "00101";
      ra <= X"0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F";
      rb(32 TO 127) <= X"000000000000000000000000";
      rb(24 TO 31) <= "00000001";
      rb(0 TO 23) <= X"000000";
      rc(0 TO 127) <= (OTHERS => 'X');
      rt(0 TO 127) <= (OTHERS => 'X');
      WAIT FOR 10 ns;
      print(opcode, ra, rb, rc, rt, result, zero);
      
      REPORT "Test Completed";
      --enable <= '0';
      WAIT;

END PROCESS;
END;