/*
 *  parse.cpp
 *
 *  Created on: Apr 30, 2013
 *  Author: a.sri
*/

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <stdlib.h>

using namespace std;

stringstream binstream;
int immflag;

void tobinary(int number){
	int remainder;
	string bin;

	if(number <= 1) {
		binstream << number;
		return;
	}

	remainder = number%2;
	tobinary(number >> 1);
	binstream << remainder;

}

vector<string> splitString(const string in){
    istringstream iss(in);
    istream_iterator<string> first(iss), last;
    vector<string> tokens;

    copy(first, last, back_inserter(tokens));
    return tokens;
}

string _getOpcode(const string in){

	string opbin;

	if (in.compare("a") == 0){
		opbin = "00011000000";
	}else if(in.compare("ah") == 0){
		opbin = "00011001000";
	}else if(in.compare("and") == 0){
		opbin = "00011000001";
	}else if(in.compare("or") == 0){
		opbin = "00001000001";
	}else if(in.compare("nand") == 0){
		opbin = "00011001001";
	}else if(in.compare("nor") == 0){
		opbin = "00001001001";
	}else if(in.compare("xor") == 0){
		opbin = "01001000001";
	}else if(in.compare("sfh") == 0){
		opbin = "00001001000";
	}else if(in.compare("sf") == 0){
		opbin = "00001000000";
	}else if(in.compare("bg") == 0){
		opbin = "00001000010";
	}else if(in.compare("eqv") == 0){
		opbin = "01001001001";
	}else if(in.compare("ceq") == 0){
		opbin = "01111000000";
	}else if(in.compare("shl") == 0){
		opbin = "00001011011";
	}else if(in.compare("rot") == 0){
		opbin = "00001011000";
	}else if(in.compare("shlqbi") == 0){
		opbin = "00111011011";
	}else if(in.compare("rotqbi") == 0){
		opbin = "00111011000";
	}else if(in.compare("gb") == 0){
		opbin = "00110110000";
	}else if(in.compare("gbh") == 0){
		opbin = "00110110001";
	}else if(in.compare("gbb") == 0){
		opbin = "00110110010";
	}else if(in.compare("cntb") == 0){
		opbin = "01010110100";
	}else if(in.compare("sumb") == 0){
		opbin = "01001010011";
	}else if(in.compare("ceqb") == 0){
		opbin = "01111010000";
	}else if(in.compare("rotqby") == 0){
		opbin = "00111011100";
	}else if(in.compare("shlqby") == 0){
		opbin = "00111011111";
	}else if(in.compare("fa") == 0){
		opbin = "01011000100";
	}else if(in.compare("fs") == 0){
		opbin = "01011000101";
	}else if(in.compare("fm") == 0){
		opbin = "01011000110";
	}else if(in.compare("fceq") == 0){
		opbin = "01111000010";
	}else if(in.compare("lqx") == 0){
		opbin = "00111000100";
	}else if(in.compare("stqx") == 0){
		opbin = "00101000100";
	}else if(in.compare("lqa") == 0){
		opbin = "001100001";
		immflag = 16;
	}else if(in.compare("lqr") == 0){
		opbin = "001100111";
		immflag = 16;
	}else if(in.compare("stqa") == 0){
		opbin = "001000001";
		immflag = 16;
	}else if(in.compare("stqr") == 0){
		opbin = "001000111";
		immflag = 16;
	}else if(in.compare("br") == 0){
		opbin = "001100100";
		immflag = 16;
	}else if(in.compare("bra") == 0){
		opbin = "001100000";
		immflag = 16;
	}else if(in.compare("ahi") == 0){
		opbin = "00011101";
		immflag = 10;
	}else if(in.compare("ai") == 0){
		opbin = "00011100";
		immflag = 10;
	}else if(in.compare("sfhi") == 0){
		opbin = "00001101";
		immflag = 10;
	}else if(in.compare("sfi") == 0){
		opbin = "00001100";
		immflag = 10;
	}else if(in.compare("lqd") == 0){
		opbin = "00110100";
		immflag = 10;
	}else if(in.compare("stqd") == 0){
		opbin = "00100100";
		immflag = 10;
	}else{
		opbin = "00000000";
	}

	return opbin;
}

string _getRegisters(string in){

	int size = 0;
	int reg = 0;
	int imm = 0;
	int immsize = 0;
	string bin;
	string result;

	if(in[0] == 'r'){
		//Registers
		//Remove the 'r'
		in.erase(0,1);
		reg = atoi(in.c_str());

		//Convert to Binary
		binstream.str("");
		binstream.clear();
		tobinary(reg);
		bin = binstream.str();

		//Format to 7 bit binary
		size = bin.size();
		if(size != 7){
			for (int i = 0 ; i < (7 - size) ; i++){
				result.append("0");
			}
		}
		result.append(bin);

	}else{
		//Immediate values

		immsize = immflag;
		immflag= 0;
		//Convert to Binary
		binstream.str("");
		binstream.clear();

		imm = atoi(in.c_str());
		tobinary(imm);
		bin = binstream.str();

		//Format
		size = bin.size();

		if(size != immsize){
			for (int i = 0 ; i < (immsize - size) ; i++){
				result.append("0");
			}
		}

		result.append(bin);
	}

	return result;
}

string _generateBinary(string instr){

	int size = 0;
	int i = 0;
	string binary;

	vector<string> tokens;
	//Split the string into tokens
	tokens = splitString(instr);

	//Vaidate Input
	size = tokens.size();
	if(size > 4){
		cout << "Invalid Instruction" << endl; //Insert no op
	}

	//Parse OPCODE
	vector<string>::iterator it = tokens.begin();
	for(i = 0 ; i < size ; i++){
		if(i == 0){
			binary.append(_getOpcode(it[i]));
		}else{
			binary.append(_getRegisters(it[i]));
		}
	}

	return binary;
}

int main(int argc, char * argv[]) {

	string instr;
	string instr_bin;
	string filename;
	int size;

	ofstream ofs;
	ofs.open ("./bin.txt");

	if(argc != 2){
		cout << "Usage: ASMparser <filename>" << endl;
		return 1;
	}else{
		filename = argv[1];
	}

	ifstream ifs(filename.c_str(), ifstream::in);
	if (ifs.good()){
		while (!ifs.eof()) {
			getline(ifs, instr);
            instr_bin = _generateBinary(instr);
            size = instr_bin.size();
            if(size != 32){
            	for (int i = 0 ; i < (32 - size) ; i++){
            		instr_bin.append("0");
            	}
            }
            ofs << instr_bin << "\n";
        }
		ifs.close();
    }else{
    	cout << "ERROR: can't open file." << endl;
    }
	ofs.close();
	return 0;
}
