
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity FWE01 is
    Port (
        clk : in STD_LOGIC;
        reset : in STD_LOGIC;   
			  fwd_datain : in  STD_LOGIC_VECTOR (0 to 127);  
				out_enable : in STD_LOGIC;
				fwd_dataout : out STD_LOGIC_VECTOR (0 to 127);
				fwd_regout : out STD_LOGIC_VECTOR (0 to 127)); 
end FWE01;

architecture behave of FWE01 is BEGIN
 PROCESS(clk)
   begin

if clk'event and clk ='1'
    then
      
      IF out_enable = '1' THEN
      fwd_dataout <= fwd_datain;
      else
        fwd_dataout <= (fwd_dataout'range => 'Z');
      end if;
      
     if reset = '1' THEN
     fwd_dataout <= (fwd_dataout'range => 'Z');
     fwd_regout <= (fwd_regout'range => 'Z');
   else
     fwd_regout <= fwd_datain; 

     
    end if;
    else
    --fwd_regout <= (others => 'Z');
    end if;
  end process;
end behave;

