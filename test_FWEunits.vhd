
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.textio.all;  
use IEEE.std_logic_textio.all;

Entity test_FWEunits is

end test_FWEunits;

architecture tb_FWEunits of test_FWEunits is

component FWEunit is
  
port(
    
   fpu_result: IN STD_LOGIC_VECTOR(0 TO 127);
    fxu_result: IN STD_LOGIC_VECTOR(0 TO 127);
    byte_result: IN STD_LOGIC_VECTOR(0 TO 127);
    reset : IN STD_LOGIC;
    fwe1_enable : IN STD_LOGIC;
    fwe2_enable : IN STD_LOGIC;
    fwe3_enable : IN STD_LOGIC;
   fwe1_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe2_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe3_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe_out_bus : OUT STD_LOGIC_VECTOR(0 TO 127);    
    clk: IN STD_LOGIC
     
     );
     
end component;

    signal fpu_result: STD_LOGIC_VECTOR(0 TO 127);
    signal fxu_result: STD_LOGIC_VECTOR(0 TO 127);
    signal byte_result: STD_LOGIC_VECTOR(0 TO 127);
    signal reset : STD_LOGIC;
    signal fwe1_enable : STD_LOGIC;
    signal fwe2_enable : STD_LOGIC;
    signal fwe3_enable : STD_LOGIC;
    
    signal fwe1_dataout_bus : STD_LOGIC_VECTOR(0 TO 127);
    signal fwe2_dataout_bus : STD_LOGIC_VECTOR(0 TO 127);
    signal fwe3_dataout_bus : STD_LOGIC_VECTOR(0 TO 127);
    signal fwe_out_bus : STD_LOGIC_VECTOR(0 TO 127); 
    signal clk: STD_LOGIC;
    constant clk_period : time := 10 ns;
    begin
    uut: FWEunit PORT MAP (
     fpu_result   => fpu_result,
     reset => reset,
     fxu_result => fxu_result,
     byte_result => byte_result,
     fwe1_enable => fwe1_enable,
     fwe2_enable => fwe2_enable,
     fwe3_enable => fwe3_enable,
     fwe1_dataout_bus  => fwe1_dataout_bus,
     fwe2_dataout_bus  => fwe2_dataout_bus,
     fwe3_dataout_bus  => fwe3_dataout_bus,
     fwe_out_bus => fwe_out_bus,
     clk => clk
        );
     
    clk_process :process
    begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   
   stim_proc: process is
   variable my_line : line;  -- type 'line' comes from textio

   begin		
--     print($time,, " A1=%d, A2=%d, A3=%d, WD3=%d, WD4=%d, RD1=%d, RD2=%d We3= %d , We4= %d ",A1,A2,A3,WD3,WD4,RD1,RD2,We3,We4,);
      -- hold reset state for 100 ns.
     -- wait for 100 ns;	
          byte_result  <= (others => 'Z');
        fpu_result  <= (others => 'Z');
         fxu_result  <= (others => 'Z');
    wait for clk_period;
     reset <= '0';
     fwe1_enable <= '1';
     --   wait for clk_period*10;
         byte_result  <= X"543216789198765432100005252fefef";
          wait for clk_period;
     fwe2_enable <= '1';
       byte_result  <= (others => 'Z');
        fpu_result  <= (others => 'Z');
         fxu_result  <= (others => 'Z');
       
    wait for clk_period;
    fxu_result  <= X"1234567891987654321000052524747f";
    wait for clk_period;
     fxu_result  <= (others => 'Z');
 fwe3_enable <= '1';
    wait for clk_period;
    fpu_result  <= X"1234567891987654321087012450789f";
    -- wait for 10 ns;
   --  fwe3_enable <= '0';
--   wait for clk_period;
   --  fwe2_enable <= '0';
   
   --    fwe1_enable <= '0';
   
   --  fwe2_enable <= '0';
 
    --  reset <= '1';
    wait ;
  end process;
end;
    
        
