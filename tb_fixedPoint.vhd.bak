LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE IEEE.std_logic_textio.ALL;
USE STD.textio.ALL;

ENTITY tb_fxu IS
END tb_fxu;

ARCHITECTURE fxu OF tb_fxu IS
  
  FILE logger : TEXT OPEN WRITE_MODE IS "transcript_FXU.log";
  
  COMPONENT fxu
    PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_ULOGIC);
  END COMPONENT;
  
  SIGNAL ra : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rb : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rc : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rt : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL opcode : STD_LOGIC_VECTOR(0 TO 4);
  SIGNAL result : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL zero : STD_ULOGIC;
  SIGNAL clk :  STD_ULOGIC;
  SIGNAL enable :  STD_ULOGIC;
  
  PROCEDURE print(opcode : STD_LOGIC_VECTOR(0 TO 4);
                  ra : STD_LOGIC_VECTOR(0 TO 127);
                  rb : STD_LOGIC_VECTOR(0 TO 127);
                  rc : STD_LOGIC_VECTOR(0 TO 127);
                  rt : STD_LOGIC_VECTOR(0 TO 127);           
                  result : STD_LOGIC_VECTOR(0 TO 127);
                  zero : STD_ULOGIC) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
  BEGIN
    swrite(line_out, "opcode = ");
    write(line_out, opcode);
    writeline(logger, line_out);
    swrite(line_out, "ra =     ");
    hwrite(line_out, ra);
    writeline(logger, line_out);
    swrite(line_out, "rb =     ");
    hwrite(line_out, rb);
    writeline(logger, line_out);
    swrite(line_out, "rc =     ");
    hwrite(line_out, rc);
    writeline(logger, line_out);
    swrite(line_out, "rt =     ");
    hwrite(line_out, rt);
    writeline(logger, line_out);
    swrite(line_out, "result = ");
    hwrite(line_out, result);
    writeline(logger, line_out);
    swrite(line_out, "zero =   ");
    write(line_out, zero);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;
  
  -- definition of a clock period
  CONSTANT clk_period : time := 10 ns;
  
BEGIN
  -- component instantiation
  FXUnit: fxu PORT MAP (
    ra => ra,
    rb => rb,
    rc => rc,
    rt => rt,
    clk => clk,
    enable => enable,
    opcode => opcode,
    result => result,
    zero => zero);
    
    CLK_PROCESS : PROCESS
    BEGIN
		  clk <= '1';
		  WAIT FOR clk_period/2;
		  
		  clk <= '0';
		  WAIT FOR clk_period/2;
    END PROCESS;
    
    SIMULATION : PROCESS
    BEGIN
    
    enable <= '1';
  
  -- (1) a (add word)
  opcode <= "00001";
  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rb(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result(0 TO 127) = X"0000001E0000001E0000001E0000001E")
    REPORT "add word failed!" SEVERITY NOTE;

  -- (2) ah (add half word)
  opcode <= "00010";
  ra(0 TO 127) <= X"000F000F000F000F000F000F000F000F";
  rb(0 TO 127) <= X"000F000F000F000F000F000F000F000F";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result(0 TO 127) = X"001E001E001E001E001E001E001E001E")
    REPORT "add half word failed!" SEVERITY NOTE;
  
  -- (3) and (and word)
  opcode <= "00011";
  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rb(0 TO 127) <= X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result(0 TO 127) = X"0000000F0000000F0000000F0000000F")
    REPORT "and word failed!" SEVERITY NOTE;

  -- (4) or (or word)
  opcode <= "00100";
  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rb(0 TO 127) <= X"00000000000000000000000000000000";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result(0 TO 127) = X"0000000F0000000F0000000F0000000F")
    REPORT "or word failed!" SEVERITY NOTE;

  -- (5) nand (nand word)
  opcode <= "00101";
  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rb(0 TO 127) <= X"00000000000000000000000000000000";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result(0 TO 127) = X"0000000F0000000F0000000F0000000F")
    REPORT "nand word failed!" SEVERITY NOTE;

  -- (6) nor (nor word)
  opcode <= "00110";
  ra(0 TO 127) <= X"FFFFFFF0FFFFFFF0FFFFFFF0FFFFFFF0";
  rb(0 TO 127) <= X"FFFFFFF0FFFFFFF0FFFFFFF0FFFFFFF0";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result = X"0000000F0000000F0000000F0000000F")
    REPORT "nor word failed!" SEVERITY NOTE;

  -- (7) xor (xor word)
  opcode <= "00111";
  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rb(0 TO 127) <= X"00000000000000000000000000000000";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result = X"0000000F0000000F0000000F0000000F")
    REPORT "xor word failed!" SEVERITY NOTE;

  -- (8) sfh (subtract from half word)
  opcode <= "01000";    
  ra <= X"000F000F000F000F000F000F000F000F";
  rb <= X"000F000F000F000F000F000F000F000F";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result = X"00000000000000000000000000000000")
    REPORT "sfh failed!" SEVERITY NOTE;
  
  -- (9) sf (subtract from word)
  opcode <= "01001";
  ra <= X"0000000F0000000F0000000F0000000F";
  rb <= X"0000000F0000000F0000000F0000000F";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result(0 TO 127) = X"00000000000000000000000000000000")
    REPORT "sf failed!" SEVERITY NOTE;
  
  -- (10) bg (Borrow Generate)
  opcode <= "01010";   
  ra <= X"00000F000000000000000F0000000000";
  rb <= X"00000000000000000000000000000000";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result = X"00000000800000000000000080000000")
    REPORT "bg failed!" SEVERITY NOTE;
  
  -- (11) eqv (equivalent)
  opcode <= "01011";
  ra <= X"FFFFFFFF000000000000000000000000";
  rb <= X"00000000000000000000000000000000";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result = X"00000000FFFFFFFFFFFFFFFFFFFFFFFF")
    REPORT "eqv failed!" SEVERITY NOTE;
  
  -- (12) ceq (Compare Equal Word)
  opcode <= "01100"; 
  ra <= X"00000000000000000000000000000000";
  rb <= X"FFFFFFFF00000000FFFFFFFF00000000";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result = X"00000000FFFFFFFF00000000FFFFFFFF")
    REPORT "ceq failed!" SEVERITY NOTE;
  
  -- (13) shl (Shift Left Word) -- FIX      
  opcode <= "01101";        
  ra <= X"000000000000000000000000FFFFFFFF";
  rb(0 TO 23) <= X"000000";
  rb(24 TO 31) <= "00000001";
  rb(32 TO 127) <= X"000000000000000000000000";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
    
  -- (14) rot (Rotate Word)
  opcode <= "01110";    
  ra <= X"000000000000000000000000FFFFFFFF";
  rb(0 TO 23) <= X"000000";
  rb(24 TO 31) <= "00000001";
  rb(32 TO 127) <= X"000000000000000000000000";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  
  -- IMMEDIATE TYPE INSTRUCTIONS
  -- (15) ahi (Add Half Word Immediate)
  opcode <= "01111";
  ra(0 TO 127) <= X"000F000F000F000F000F000F000F000F";
  rb(0 TO 127) <= (OTHERS => 'X');
  rc(0 TO 127) <= X"000F000F000F000F000F000F000F000F";
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result(0 TO 127) = X"001E001E001E001E001E001E001E001E")
    REPORT "ahi failed!" SEVERITY NOTE;
      
  -- (16) ai (Add Word Immediate)
  opcode <= "10000";
  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rb(0 TO 127) <= (OTHERS => 'X');
  rc(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rt(0 TO 127) <= (OTHERS => 'X');  
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result(0 TO 127) = X"0000001E0000001E0000001E0000001E")
    REPORT "ai failed!" SEVERITY NOTE;
  
  -- (17) sfhi (Subtract From Halfword Immediate)
  opcode <= "10001";
  ra <= X"000F000F000F000F000F000F000F000F";
  rb(0 TO 127) <= (OTHERS => 'X');
  rc <= X"000F000F000F000F000F000F000F000F";
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result = X"00000000000000000000000000000000")
    REPORT "sfhi failed!" SEVERITY NOTE;
 
  -- (18) sfi (Subtract From Word Immediate)
  opcode <= "10010";
  ra <= X"0000000F0000000F0000000F0000000F";
  rb(0 TO 127) <= (OTHERS => 'X');
  rc <= X"0000000F0000000F0000000F0000000F";
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result, zero);
  ASSERT (result = X"00000000000000000000000000000000")
    REPORT "sfi failed!" SEVERITY NOTE;
  
  REPORT "Test Completed";
  enable <= '0';
  WAIT;
  
END PROCESS;
END;