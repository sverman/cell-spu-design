library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.textio.all;  
use IEEE.std_logic_textio.all;
entity localstore is
Generic(W : natural := 32);
	PORT
	(	clk			: IN  std_logic;
		enable			: IN  std_logic;
		datain			: IN  std_logic_vector(0 TO 127);
		rw_address	: IN  std_logic_vector( 0 TO W-1);
   -- read_address	: IN  std_logic_vector(0 TO 31);
		wr1_e	: IN  std_logic;
		wr2_e : IN std_logic;
		dataout1		: OUT std_logic_vector(0 TO 127); -- LSU access
		dataout2  : OUT std_logic_vector(0 TO 1023) -- ILB load
	);
END localstore;
architecture arch_localstore of localstore is
TYPE RAM IS ARRAY(0 TO 262143) OF std_logic_vector(0 TO 7) ; -- address bus width is 18 and data size is byte
SIGNAL datablock : RAM := ( 0 to 127 => (others => 'Z'), 128 to 262143 => "01110101");
signal rw_address_s : std_logic_vector ( 0 to 17);

 
  -- process for reading data into memory---------------------------------------------------
  function to_std_logic(c: character) return std_logic is 
    variable sl: std_logic;
    begin
      case c is
        when 'U' => 
           sl := 'U'; 
        when 'X' =>
           sl := 'X';
        when '0' => 
           sl := '0';
        when '1' => 
           sl := '1';
        when 'Z' => 
           sl := 'Z';
        when 'W' => 
           sl := 'W';
        when 'L' => 
           sl := 'L';
        when 'H' => 
           sl := 'H';
        when '-' => 
           sl := '-';
        when others =>
           sl := 'X'; 
    end case;
   return sl;
  end to_std_logic;
  
  
function to_std_logic_vector(s: string) return std_logic_vector is 
  variable slv: std_logic_vector(s'high-s'low downto 0);
  variable k: integer;
begin
   k := s'high-s'low;
  for i in s'range loop
     slv(k) := to_std_logic(s(i));
     k      := k - 1;
  end loop;
  return slv;
end to_std_logic_vector;  

  begin
 ----------------------------------------------------------------------------------------------
     read_input_file:  process  --source ->http://www.ee.sunysb.edu/~jochoa/vhd_writefile_tutorial.htm
      variable inline:line;
			variable character_variable:character;
			variable line_content,line_content2 : string(1 to 32);
			variable end_of_line:boolean;
			variable i,j : integer:=0;
			variable content : std_logic_vector ( 1 to 32);
			file myfile: text open read_mode is "C:\Users\SAMBHAV\545\source_SPU\spu\bin.txt";
	   
	    begin
	     
      for m in 0 to 31 loop -- while not endfile(myfile) loop
      readline(myfile, inline);
      read(inline, line_content);
      content := to_std_logic_vector(line_content);
      datablock(4*m) <= content (1 to 8);
     
    	 datablock((m*4+1)) <= content (9 to 16);
     
      datablock((m*4+2)) <= content (17 to 24);
     
      datablock((m*4+3)) <= content (25 to 32);
     
     --  j := j + 4;
      end loop;
      
   
      wait;
        
           
end process;
   
  
-----------------------------------------------------------------------------------------------  

 rw_address_s <= rw_address ( 14 to 31);
  
PROCESS (clk)
  VARIABLE i : INTEGER:=0;
  VARIABLE x : INTEGER:=0;
  
  VARIABLE j : INTEGER:=0;
  VARIABLE y : INTEGER:=0;
  
BEGIN
IF clk'event AND clk = '1' THEN
  if enable ='1' then
			IF wr1_e = '1' THEN
			  FOR t IN 0 TO 15 LOOP
			    x := (t * 8);
			   datablock(to_integer(unsigned(rw_address_s)+t)) <= datain (x to x+7 );
			  end loop;
   		   elsif
			   wr1_e = '0' then
			    FOR v IN 0 TO 15 LOOP
			       x := (v * 8);
			   dataout1 (x to x+7 ) <= datablock(to_integer(unsigned(rw_address_s)+v));
			end loop;
	 	   
			END IF;
			end if;--enable
			IF    wr2_e = '0' then ---ILB burst read -- set via some main module logic
			      FOR j IN 0 TO 127 LOOP
			        y := (j * 8);
			 --   dataout2  (y to y+7 ) <= datablock (to_integer(unsigned(rw_address_s)+j));
			 dataout2  (y to y+7 ) <= datablock (j);
 			    end loop;
			END IF;
			
	  end if;--clk
	  
end process;
end architecture;