
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity ILB_SPU is
  
    port (	clk : in STD_LOGIC;
				ins_in : in  STD_LOGIC_VECTOR (0 to 511); -- 64 B read from Latch. 
				in_enable : in STD_LOGIC;
				out_enable : in STD_LOGIC;
				ins_out : out STD_LOGIC_VECTOR (0 to 63)); -- 2 instructions in 1 cycle
				
end ILB_SPU;

architecture Behavioral of ILB_SPU is
constant clk_period : time := 10 ns;
signal sig_datain : STD_LOGIC_VECTOR (0 to 511);
signal sig_dataout : STD_LOGIC_VECTOR (0 to 63);
signal d : INTEGER := 0;
begin

mycode: process (clk)
variable i : INTEGER:=0;
variable count : INTEGER:= -64;


begin

if clk = '1' then
			
		if in_enable = '1' then
				
		sig_datain <= ins_in;
		end if;
		
		if out_enable = '1'
		then
			if d = 448  then 
				--i:= 0;
				d <= 0;
				elsif
				d = 0
				then
				d <= d+64 after clk_period;
					elsif d > 0
					then
				d <=	d+64;
			
			end if;
		ins_out(0 to 31) <= sig_datain( d to (31+d));
		ins_out(32 to 63) <= sig_datain( (32+d) to (63+d));
		
		else
		 -- ins_out(0 to 31)<= (others => 'Z');
		--  ins_out(32 to 63)<= (others => 'Z');
		end if;
	
		else 
	
	--	ins_out (0 to 63) <= (OTHERS =>'X');
		
		

end if;
--end if;
end process mycode;

end Behavioral;

