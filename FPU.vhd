library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use ieee.float_pkg.all;
 
Entity floatingunit is
  
PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_ULOGIC;
    clk: IN STD_ULOGIC);
END floatingunit;

ARCHITECTURE behave OF floatingunit IS


component FU_register is
   port (FUR_indata   : in std_logic_vector ( 0 to 127);
               reset  : in std_logic :='0';
               clk    : in std_logic;
          FUR_outdata : out std_logic_vector ( 0 to 127));
end component;

signal  result_s1 : STD_LOGIC_VECTOR(0 TO 127);
signal  result_s2 : STD_LOGIC_VECTOR(0 TO 127);
signal  result_s3 : STD_LOGIC_VECTOR(0 TO 127);

signal reset_s1 : std_logic :='0';
signal reset_s2 : std_logic :='0';
--signal reset_s3 : std_logic :='0';
BEGIN
  
  reg1map : FU_register PORT MAP
   (
    FUR_indata  => result_s2,
    reset => reset_s1,
    clk   => clk,
    FUR_outdata => result_s3
  );
  
   reg2map : FU_register PORT MAP
   (
    FUR_indata  => result_s3,
    reset => reset_s2,
    clk   => clk,
    FUR_outdata => result --_s3
  ); 
  
--  reg3map : FU_register PORT MAP
--   (
--    FUR_indata  => result_s3,
--    reset => reset_s3,
--    clk   => clk,
--    FUR_outdata => result
--  );
  
 PROCESS(clk)

  VARIABLE x1 : float(8 DOWNTO -23);
  VARIABLE x2 : float(8 DOWNTO -23);
  VARIABLE x3 : float(8 DOWNTO -23);
  VARIABLE x4 : float(8 DOWNTO -23);
  VARIABLE y1 : float(8 DOWNTO -23);
  VARIABLE y2 : float(8 DOWNTO -23);
  VARIABLE y3 : float(8 DOWNTO -23);
  VARIABLE y4 : float(8 DOWNTO -23);
  VARIABLE z1 : float(8 DOWNTO -23);
  VARIABLE z2 : float(8 DOWNTO -23);
  VARIABLE z3 : float(8 DOWNTO -23);
  VARIABLE z4 : float(8 DOWNTO -23);
  
  begin 
    
    -- buffer values in preferred slots to 32 bit float variables
    
    x1 := to_float (ra(0 to 31));   
    x2 := to_float (ra(32 to 63));   
    x3 := to_float (ra(64 to 95));   
    x4 := to_float (ra(96 to 127));   
    y1 := to_float (rb(0 to 31));   
    y2 := to_float (rb(32 to 63));   
    y3 := to_float (rb(64 to 95));   
    y4 := to_float (rb(96 to 127));   
    
    if clk'event and clk ='1'
    then
  --    IF enable = '1' THEN
  
      if opcode = "00001" -- floating add
      then
  
        z1 := x1 + y1;
        z2 := x2 + y2;
        z3 := x3 + y3;
        z4 := x4 + y4;  

        result_s1(0 to 31)<= to_slv(z1);
        result_s1(32 to 63)<= to_slv(z2);
        result_s1(64 to 95)<= to_slv(z3);
        result_s1(96 to 127)<= to_slv(z4);
  
      elsif opcode = "00010" -- floating subtract
      then
  
        z1 := x1 - y1;
        z2 := x2 - y2;
        z3 := x3 - y3;
        z4 := x4 - y4;  

        result_s1(0 to 31)<= to_slv(z1);
        result_s1(32 to 63)<= to_slv(z2);
        result_s1(64 to 95)<= to_slv(z3);
        result_s1(96 to 127)<= to_slv(z4);
        
      elsif opcode = "00011" -- floating multiply
      then
        
        z1 := x1 * y1;
        z2 := x2 * y2;
        z3 := x3 * y3;
        z4 := x4 * y4;  

        result_s1(0 to 31)<= to_slv(z1);
        result_s1(32 to 63)<= to_slv(z2);
        result_s1(64 to 95)<= to_slv(z3);
        result_s1(96 to 127)<= to_slv(z4);

      elsif opcode = "00100" -- floating compare
      then
        -- compare bits in ra and rb and set rt if all of them are one
        if x1=y1 
        then
          result_s1(0 to 31) <= X"FFFFFFFF";
        else 
          result_s1(0 to 31) <= X"00000000";
        end if;

        if x2=y2 
        then
          result_s1(32 to 63) <= X"FFFFFFFF";
        else 
          result_s1(32 to 63) <= X"00000000";
        end if;

        if x3=y3 
        then
          result_s1(64 to 95) <= X"FFFFFFFF";
        else 
          result_s1(64 to 95) <= X"00000000";
        end if;

        if x4=y4 
        then
          result_s1(96 to 127) <= X"FFFFFFFF";
        else 
          result_s1(96 to 127) <= X"00000000";
        end if;
        
      end if; -- opcode compare ends

      ELSE
    result <= (OTHERS => 'Z');
            if enable = '1' then
        result_s2 <= result_s1;
         else
         result_s2 <= (OTHERS => 'Z');
      end if;
   
  --END IF; -- enable
    end if; -- opcode ends
  end process; 
end behave; 