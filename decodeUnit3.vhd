LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY decoder3 IS
  PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    opcode_0, opcode_1 : OUT STD_LOGIC_VECTOR(0 TO 8);
    
    ra1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_2    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    -- Register R/W & Memory R/W Signals
    RegWr_ins0  : OUT BIT;
    MemRd_ins0  : OUT BIT;
    MemWr_ins0  : OUT BIT;
    
    RegWr_ins1  : OUT BIT;
    MemRd_ins1  : OUT BIT;
    MemWr_ins1  : OUT BIT;
        
    delayILB : OUT STD_ULOGIC;
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC;
    IMM_SEL1  : OUT BIT_VECTOR (0 to 1) ; -- 00 for rc -- 01 immv10 -- 02 immv16
    IMM_SEL2  : OUT BIT_VECTOR  (0 to 1) ;
      
    i_inst : IN STD_LOGIC_VECTOR(0 TO 31);
    o_inst : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    i_inst_sel : IN STD_ULOGIC;
    o_inst_sel : OUT STD_ULOGIC
    );
END decoder3;

ARCHITECTURE behave OF decoder3 IS
  
  -- Declaration of Variables
    SIGNAL in_0 : UNSIGNED(0 TO 31);
    SIGNAL in_1 : UNSIGNED(0 TO 31);
    SIGNAL op_0 : UNSIGNED(0 TO 8);
    SIGNAL op_1 : UNSIGNED(0 TO 8);
    
    SIGNAL ra_0  : UNSIGNED(0 TO 6);
    SIGNAL rb_0  : UNSIGNED(0 TO 6);
    SIGNAL rc_0  : UNSIGNED(0 TO 6);
    SIGNAL rt_0  : UNSIGNED(0 TO 6);
    SIGNAL i10_0 : UNSIGNED(0 TO 9);
    SIGNAL i16_0 : UNSIGNED(0 TO 15);
    
    SIGNAL ra_1  : UNSIGNED(0 TO 6);
    SIGNAL rb_1  : UNSIGNED(0 TO 6);
    SIGNAL rc_1  : UNSIGNED(0 TO 6);
    SIGNAL rt_1  : UNSIGNED(0 TO 6); 
    SIGNAL i10_1 : UNSIGNED(0 TO 9);
    SIGNAL i16_1 : UNSIGNED(0 TO 15);
    
    -- Flags
    SIGNAL vRegWr_ins0 : BIT;
    SIGNAL vMemRd_ins0 : BIT;
    SIGNAL vMemWr_ins0 : BIT;
    
    SIGNAL vRegWr_ins1 : BIT;
    SIGNAL vMemRd_ins1 : BIT;
    SIGNAL vMemWr_ins1 : BIT;
    
     
    signal imm_selv1 : BIT_VECTOR (0 to 1);
    signal imm_selv2 : BIT_VECTOR (0 to 1);
    

  
BEGIN
  PROCESS(inst_0, inst_1, i_inst, i_inst_sel, clk, enable)
        
  BEGIN
  IF clk'event AND clk = '1' THEN -- clk
    IF enable = '1' THEN   
  
      -- initialize
      IF i_inst_sel = '0' THEN
        in_0 <= UNSIGNED(inst_0);
        in_1 <= UNSIGNED(inst_1);
      ELSE
        in_0 <= UNSIGNED(i_inst);
        in_1 <= (OTHERS => 'X');
      END IF;
      
      IF i_inst_sel = '1' THEN
        o_inst_sel <= '0';
        delayILB <= '0';
      END IF;
      
      -- Check Instruction 1
      
      -- check for noop
        IF in_0(0 TO 10) = "00000000000" THEN -- noop
        op_0 <= "000000000";
        ra_0 <= "0000000";
        rb_0 <= "0000000";
        rc_0 <= "0000000";
        rt_0 <= "0000000";
        i10_0 <= (OTHERS => '0');
        i16_0 ( 0 to 15) <= (OTHERS => '0');
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
        
      -- Check for FXU
      elsIF in_0(0 TO 10) = "00011000000" THEN -- a
        op_0 <= "000011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
        
      ELSIF in_0(0 TO 10) = "00011001000" THEN  -- ah
        op_0 <= "000101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00011000001" THEN -- and
        op_0 <= "000111010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001000001" THEN -- or
        op_0 <= "001001010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00011001001" THEN -- nand
        op_0 <= "001011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001001001" THEN -- nor
        op_0 <= "001101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01001000001" THEN -- xor
        op_0 <= "001111010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001001000" THEN -- sfh
        op_0 <= "010001010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001000000" THEN -- sf
        op_0 <= "010011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
    
      ELSIF in_0(0 TO 10) = "00001000010" THEN -- bg
        op_0 <= "010101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01001001001" THEN -- eqv
        op_0 <= "010111010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01111000000" THEN -- ceq
        op_0 <= "011001010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001011011" THEN -- shl
        op_0 <= "011011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001011000" THEN -- rot
        op_0 <= "011101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
                
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Check for Permute Instructions
      ELSIF in_0(0 TO 10) = "00111011011" THEN -- shlqbi
        op_0 <= "000011001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00111011000" THEN -- rotqbi
        op_0 <= "000101001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00110110000" THEN -- gb
        op_0 <= "000111001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00110110001" THEN -- gbh
        op_0 <= "001001001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00110110010" THEN -- gbb
        op_0 <= "001011001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Check for Byte Instructions
      ELSIF in_0(0 TO 10) = "01010110100" THEN  -- cntb
        op_0 <= "000011110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
        
      ELSIF in_0(0 TO 10) = "01001010011" THEN -- sumb
        op_0 <= "000101110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01111010000" THEN -- ceqb
        op_0 <= "000111110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00111011100" THEN -- rotqby
        op_0 <= "001001110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00111011111" THEN -- shlqby
        op_0 <= "001011110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Check for FPU Instructions
      ELSIF in_0(0 TO 10) = "01011000100" THEN -- fa
        op_0 <= "000010110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01011000101" THEN  -- fs
        op_0 <= "000100110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01011000110" THEN -- fm
        op_0 <= "000110110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01111000010" THEN -- fceq
        op_0 <= "001010110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Load Instruction x-form
      ELSIF in_0(0 TO 10) = "00111000100" THEN -- lqx
        op_0 <= "000100101";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '1';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
        
      -- Store Instruction x-form
      ELSIF in_0(0 TO 10) = "00101000100" THEN -- stqx
        op_0 <= "001100101";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '1';
        imm_selv1 <= "11";
      
      -- Load Instruction a-form and  r-form
      ELSIF in_0(0 TO 8) = "001100001" THEN -- lqa
        op_0 <= "000110101";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '1';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "10";
      
      ELSIF in_0(0 TO 8) = "001100111" THEN -- lqr
        op_0 <= "001000101";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '1';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "10";
      
      -- Store Instruction a-form and  r-form
      ELSIF in_0(0 TO 8) = "001000001" THEN -- stqa
        op_0 <= "001110101";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '1';
        imm_selv1 <= "10";
      
      ELSIF in_0(0 TO 8) = "001000111" THEN -- stqr
        op_0 <= "010000101";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '1';
        imm_selv1 <= "10";
        
      -- BRANCH UNIT Instructions
      ELSIF in_0(0 TO 8) = "001100100" THEN -- br
        op_0 <= "000010011";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= (OTHERS => 'X');
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "10";
      
      ELSIF in_0(0 TO 8) = "001100000" THEN -- bra
        op_0 <= "000100011";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= (OTHERS => 'X');
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "10";
      
      -- FXU Immediate Type Instructions
      ELSIF in_0(0 TO 7) = "00011101" THEN -- ahi
        op_0 <= "011111010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      ELSIF in_0(0 TO 7) = "00011100" THEN -- ai
        op_0 <= "100001010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      ELSIF in_0(0 TO 7) = "00001101" THEN -- sfhi
        op_0 <= "100011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
        
      ELSIF in_0(0 TO 7) = "00001100" THEN -- sfi
        op_0 <= "100101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      -- Load Instruction d-form
      ELSIF in_0(0 TO 7) = "00110100" THEN -- lqd
        op_0 <= "000010101";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '1';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      -- Store Instruction d-form      
      ELSIF in_0(0 TO 7) = "00100100" THEN -- stqd
        op_0 <= "001010101";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '1';
        imm_selv1 <= "01";
      
      ELSE
        op_0 <= (OTHERS => '0');
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= (OTHERS => 'X');
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
        
      END IF;
      
      -- Check for Instruction 2
      IF in_1(0 TO 10) = "00000000000" THEN -- noop
        op_1 <= "000000000";
        ra_1 <= "0000000";
        rb_1 <= "0000000";
        rc_1 <= "0000000";
        rt_1 <= "0000000";
        i10_1 <= (OTHERS => '0');
        i16_1 ( 0 to 15) <= (OTHERS => '0');
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
        
    
      -- Check for FXU
      ELSIF in_1(0 TO 10) = "00011000000" THEN -- a
        op_1 <= "000011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00011001000" THEN  -- ah
        op_1 <= "000101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00011000001" THEN -- and
        op_1 <= "000111010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00001000001" THEN -- or
        op_1 <= "001001010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00011001001" THEN -- nand
        op_1 <= "001011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00001001001" THEN -- nor
        op_1 <= "001101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "01001000001" THEN -- xor
        op_1 <= "001111010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00001001000" THEN -- sfh
        op_1 <= "010001010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00001000000" THEN -- sf
        op_1 <= "010011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
    
      ELSIF in_1(0 TO 10) = "00001000010" THEN -- bg
        op_1 <= "010101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "01001001001" THEN -- eqv
        op_1 <= "010111010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "01111000000" THEN -- ceq
        op_1 <= "011001010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00001011011" THEN -- shl
        op_1 <= "011011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00001011000" THEN -- rot
        op_1 <= "011101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";      
      -- Check for Permute Instructions
      ELSIF in_1(0 TO 10) = "00111011011" THEN -- shlqbi
        op_1 <= "000011001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00111011000" THEN -- rotqbi
        op_1 <= "000101001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00110110000" THEN -- gb
        op_1 <= "000111001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00110110001" THEN -- gbh
        op_1 <= "001001001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
              
      ELSIF in_1(0 TO 10) = "00110110010" THEN -- gbb
        op_1 <= "001011001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      -- Check for Byte Instructions
      ELSIF in_1(0 TO 10) = "01010110100" THEN  -- cntb
        op_1 <= "000011110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "01001010011" THEN -- sumb
        op_1 <= "000101110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "01111010000" THEN -- ceqb
        op_1 <= "000111110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00111011100" THEN -- rotqby
        op_1 <= "001001110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "00111011111" THEN -- shlqby
        op_1 <= "001011110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      -- Check for FPU Instructions
      ELSIF in_1(0 TO 10) = "01011000100" THEN -- fa
        op_1 <= "000010110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        
imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "01011000101" THEN  -- fs
        op_1 <= "000100110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "01011000110" THEN -- fm
        op_1 <= "000110110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      ELSIF in_1(0 TO 10) = "01111000010" THEN -- fceq
        op_1 <= "001010110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      -- Load Instruction x-form
      ELSIF in_1(0 TO 10) = "00111000100" THEN -- lqx
        op_1 <= "000100101";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '1';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      
      -- Store Instruction x-form
      ELSIF in_1(0 TO 10) = "00101000100" THEN -- stqx
        op_1 <= "001100101";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '1';
        imm_selv2 <= "11";
      
      -- Load Instruction a-form and  r-form
      ELSIF in_1(0 TO 8) = "001100001" THEN -- lqa
        op_1 <= "000110101";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '1';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "10";
      
      ELSIF in_1(0 TO 8) = "001100111" THEN -- lqr
        op_1 <= "001000101";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '1';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "10";
      
      -- Store Instruction a-form and  r-form
      ELSIF in_1(0 TO 8) = "001000001" THEN -- stqa
        op_1 <= "001110101";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '1';
        imm_selv2 <= "10";
              
      ELSIF in_1(0 TO 8) = "001000111" THEN -- stqr
        op_1 <= "010000101";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '1';
        imm_selv2 <= "10";
      
      -- BRANCH UNIT Instructions
      ELSIF in_1(0 TO 8) = "001100100" THEN -- br
        op_1 <= "000010011";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= (OTHERS => 'X');
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "10";
      
      ELSIF in_1(0 TO 8) = "001100000" THEN -- bra
        op_1 <= "000100011";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= (OTHERS => 'X');
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "10";
      
      -- FXU Immediate Type Instructions
      ELSIF in_1(0 TO 7) = "00011101" THEN -- ahi
        op_1 <= "011111010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      ELSIF in_1(0 TO 7) = "00011100" THEN -- ai
        op_1 <= "100001010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      ELSIF in_1(0 TO 7) = "00001101" THEN -- sfhi
        op_1 <= "100011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";      
      ELSIF in_1(0 TO 7) = "00001100" THEN -- sfi
        op_1 <= "100101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      -- Load Instruction d-form
      ELSIF in_1(0 TO 7) = "00110100" THEN -- lqd
        op_1 <= "000010101";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '1';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      -- Store Instruction d-form      
      ELSIF in_1(0 TO 7) = "00100100" THEN -- stqd
        op_1 <= "001010101";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '1';
        imm_selv2 <= "01";      
      ELSE
        op_1 <= (OTHERS => '0');
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= (OTHERS => 'X');
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";        
      END IF;
      
      -- Check for Structural Hazard
      -- and assign variables to output signals
      IF op_0(8) = op_1(8) THEN
        -- Structural Hazard
        IF op_0(8) = '1' THEN
          -- Both Instructions are for odd pipe
          opcode_0 <= (OTHERS => '0');
          ra1_addr <= (OTHERS => 'Z');
          rb1_addr <= (OTHERS => 'Z');
          rc1_addr <= (OTHERS => 'Z');
          rt1_addr <= (OTHERS => 'Z');
          imm7_1   <= (OTHERS => 'Z');
          imm8_1   <= (OTHERS => 'Z');
          imm10_1  <= (OTHERS => 'Z');
          imm16_1  <= (OTHERS => 'Z');      
          RegWr_ins0 <= '0';
          MemRd_ins0 <= '0';
          MemWr_ins0 <= '0';
          IMM_SEL1 <= "11";
          
          opcode_1 <= STD_LOGIC_VECTOR(op_1);
          ra2_addr <= STD_LOGIC_VECTOR(ra_1);
          rb2_addr <= STD_LOGIC_VECTOR(rb_1);
          rc2_addr <= STD_LOGIC_VECTOR(rc_1);
          rt2_addr <= STD_LOGIC_VECTOR(rt_1);
          imm7_2   <= (OTHERS => 'X');
          imm8_2   <= (OTHERS => 'X');
          imm10_2  <= STD_LOGIC_VECTOR(i10_1);
          imm16_2  <= STD_LOGIC_VECTOR(i16_1);
          RegWr_ins1 <= vRegWr_ins1;
          MemRd_ins1 <= vMemRd_ins1;
          MemWr_ins1 <= vMemWr_ins1;
          IMM_SEL2 <= imm_selv2;
          delayILB <= '1';
          
          --ISSUE opcode_0 in the next cycle
          o_inst_sel <= '1';
          o_inst <= inst_0;
          --END
        
        ELSIF op_0(8)= '0' THEN
          -- Both Instructions are for even pipe
          opcode_0 <= STD_LOGIC_VECTOR(op_0);
          ra1_addr <= STD_LOGIC_VECTOR(ra_0);
          rb1_addr <= STD_LOGIC_VECTOR(rb_0);
          rc1_addr <= STD_LOGIC_VECTOR(rc_0);
          rt1_addr <= STD_LOGIC_VECTOR(rt_0);
          imm7_1   <= (OTHERS => 'X');
          imm8_1   <= (OTHERS => 'X');
          imm10_1  <= STD_LOGIC_VECTOR(i10_0);
          imm16_1  <= STD_LOGIC_VECTOR(i16_0);
          RegWr_ins0 <= vRegWr_ins0 AFTER 10 ns;
          MemRd_ins0 <= vMemRd_ins0 AFTER 10 ns;
          MemWr_ins0 <= vMemWr_ins0 AFTER 10 ns;
          IMM_SEL1 <= imm_selv1;
          opcode_1 <= (OTHERS => '0');
          ra2_addr <= (OTHERS => 'Z');
          rb2_addr <= (OTHERS => 'Z');
          rc2_addr <= (OTHERS => 'Z');
          rt2_addr <= (OTHERS => 'Z');
          imm7_2   <= (OTHERS => 'Z');
          imm8_2   <= (OTHERS => 'Z');
          imm10_2  <= (OTHERS => 'Z');
          imm16_2  <= (OTHERS => 'Z');
          RegWr_ins1 <= '0';
          MemRd_ins1 <= '0';
          MemWr_ins1 <= '0';
          IMM_SEL2 <= "11";
          delayILB <= '1';
          
          --ISSUE opcode_1 in the next cycle
          o_inst_sel <= '1';
          o_inst <= inst_1;
          --END
                    
        END IF;       
      ELSE
        
        -- No Structural Hazard
        IF op_0(8) = '0' THEN
        
        opcode_0 <= STD_LOGIC_VECTOR(op_0);
        ra1_addr <= STD_LOGIC_VECTOR(ra_0);
        rb1_addr <= STD_LOGIC_VECTOR(rb_0);
        rc1_addr <= STD_LOGIC_VECTOR(rc_0);
        rt1_addr <= STD_LOGIC_VECTOR(rt_0);
        imm7_1   <= (OTHERS => 'X');
        imm8_1   <= (OTHERS => 'X');
        imm10_1  <= STD_LOGIC_VECTOR(i10_0);
        imm16_1  <= STD_LOGIC_VECTOR(i16_0);
        
        RegWr_ins0 <= vRegWr_ins0;
        MemRd_ins0 <= vMemRd_ins0;
        MemWr_ins0 <= vMemWr_ins0;
        IMM_SEL1 <= imm_selv1;
        
        
        opcode_1 <= STD_LOGIC_VECTOR(op_1);
        ra2_addr <= STD_LOGIC_VECTOR(ra_1);
        rb2_addr <= STD_LOGIC_VECTOR(rb_1);
        rc2_addr <= STD_LOGIC_VECTOR(rc_1);
        rt2_addr <= STD_LOGIC_VECTOR(rt_1);
        imm7_2   <= (OTHERS => 'X');
        imm8_2   <= (OTHERS => 'X');
        imm10_2  <= STD_LOGIC_VECTOR(i10_1);
        imm16_2  <= STD_LOGIC_VECTOR(i16_1);
        
        RegWr_ins1 <= vRegWr_ins1;
        MemRd_ins1 <= vMemRd_ins1;
        MemWr_ins1 <= vMemWr_ins1;
        IMM_SEL2 <= imm_selv2;
   
        ELSE
   
        opcode_0 <= STD_LOGIC_VECTOR(op_1);
        ra1_addr <= STD_LOGIC_VECTOR(ra_1);
        rb1_addr <= STD_LOGIC_VECTOR(rb_1);
        rc1_addr <= STD_LOGIC_VECTOR(rc_1);
        rt1_addr <= STD_LOGIC_VECTOR(rt_1);
        imm7_1   <= (OTHERS => 'X');
        imm8_1   <= (OTHERS => 'X');
        imm10_1  <= STD_LOGIC_VECTOR(i10_1);
        imm16_1  <= STD_LOGIC_VECTOR(i16_1);
        
        RegWr_ins0 <= vRegWr_ins1;
        MemRd_ins0 <= vMemRd_ins1;
        MemWr_ins0 <= vMemWr_ins1;
        IMM_SEL1 <= imm_selv2;
        
        
        opcode_1 <= STD_LOGIC_VECTOR(op_0);
        ra2_addr <= STD_LOGIC_VECTOR(ra_0);
        rb2_addr <= STD_LOGIC_VECTOR(rb_0);
        rc2_addr <= STD_LOGIC_VECTOR(rc_0);
        rt2_addr <= STD_LOGIC_VECTOR(rt_0);
        imm7_2   <= (OTHERS => 'X');
        imm8_2   <= (OTHERS => 'X');
        imm10_2  <= STD_LOGIC_VECTOR(i10_0);
        imm16_2  <= STD_LOGIC_VECTOR(i16_0);
        
        RegWr_ins1 <= vRegWr_ins0;
        MemRd_ins1 <= vMemRd_ins0;
        MemWr_ins1 <= vMemWr_ins0;
        IMM_SEL2 <= imm_selv1;
   
        END IF;
        delayILB <= '0';
        
        o_inst_sel <= '0';
        o_inst <= (OTHERS => 'X');
        
      END IF;
      
    ELSE -- Enable is 0
      opcode_0 <= (OTHERS => 'X');
      opcode_1 <= (OTHERS => 'X');
      ra1_addr <= (OTHERS => 'X');
      rb1_addr <= (OTHERS => 'X');
      rc1_addr <= (OTHERS => 'X');
      rt1_addr <= (OTHERS => 'X');
      ra2_addr <= (OTHERS => 'X');
      rb2_addr <= (OTHERS => 'X');
      rc2_addr <= (OTHERS => 'X');
      rt2_addr <= (OTHERS => 'X');
      IMM_SEL1 <= "11";
     
      imm7_1   <= (OTHERS => 'X');
      imm8_1   <= (OTHERS => 'X');
      imm10_1  <= (OTHERS => 'X');
      imm16_1  <= (OTHERS => 'X');
      imm7_2   <= (OTHERS => 'X');
      imm8_2   <= (OTHERS => 'X');
      imm10_2  <= (OTHERS => 'X');
      imm16_2  <= (OTHERS => 'X');
      
      RegWr_ins0 <= '0';
      MemRd_ins0 <= '0';
      MemWr_ins0 <= '0';
      RegWr_ins1 <= '0';
      MemRd_ins1 <= '0';
      MemWr_ins1 <= '0';
      IMM_SEL2 <= "11";
      delayILB <= 'X';
      
      o_inst_sel <= '0';
      o_inst <= (OTHERS => 'X');
      
    END IF; -- ENABLE
  END IF; -- CLK
END PROCESS; 
END behave;