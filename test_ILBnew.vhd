LIBRARY ieee;
USE ieee.STD_LOGIC_1164.ALL;
  
ENTITY test_ILB IS
END test_ILB;
 
ARCHITECTURE behavior OF test_ILB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT ILB
    PORT(
         clk : IN  STD_LOGIC;
         ins_in : IN  STD_LOGIC_VECTOR(0 TO 1023);
         in_enable : IN  STD_LOGIC;
         out_enable : IN  STD_LOGIC;
         ins_out : OUT  STD_LOGIC_VECTOR(0 TO 63);
         pc_ilb : OUT STD_LOGIC_VECTOR (0 TO 63);
         flush : IN BIT;
                           pc_write : IN BIT;
                           offset : IN STD_LOGIC_VECTOR (0 TO 4)
        );
    END COMPONENT;
    

   --Inputs
   SIGNAL clk : STD_LOGIC := '1';
   SIGNAL ins_in : STD_LOGIC_VECTOR(0 TO 1023) := (OTHERS => 'X');
   SIGNAL in_enable : STD_LOGIC := '0';
   SIGNAL out_enable : STD_LOGIC := '0';
   SIGNAL flush : BIT := '0';
         SIGNAL pc_write : BIT := '0';
         SIGNAL offset : STD_LOGIC_VECTOR (0 TO 4) := (OTHERS => 'X');

        --Outputs
   SIGNAL ins_out : STD_LOGIC_VECTOR(0 TO 63);
   SIGNAL pc_ilb : STD_LOGIC_VECTOR(0 TO 63);
   
   -- Clock period definitions
   CONSTANT clk_period : time := 10 ns;
 
BEGIN
 
         -- Instantiate the Unit Under Test (UUT)
   uut: ILB PORT MAP (
          clk => clk,
          ins_in => ins_in,
          in_enable => in_enable,
          out_enable => out_enable,
          ins_out => ins_out,
          pc_ilb => pc_ilb,
          flush => flush,
          pc_write => pc_write,
          offset => offset
        );

   -- Clock process definitions
   clk_process :PROCESS
   BEGIN
                clk <= '0';
                WAIT FOR clk_period/2;
                clk <= '1';
                WAIT FOR clk_period/2;
   END PROCESS;

   -- Stimulus process
   stim_proc: PROCESS
   BEGIN
                
                  in_enable <= '1';
      ins_in <= X"12ff345F6789F1023987F546432F12ff982F2567F5423875F4596852F21ff451F5825369F0023020F0012300F00ff000F0076300F002301234578960F002468F12ff345F6789F1023987F546432F12ff982F2567F5423875F4596852F21ff451F5825369F0023020F0012300F00ff000F0076300F002301234578960F002468F";
      
      WAIT FOR clk_period;
      WAIT FOR clk_period;
      
      out_enable <= '1';
      
      WAIT FOR clk_period;
      WAIT FOR clk_period;
      in_enable <= '0';
      pc_write <= '1';
      offset <= "01000";
       
      WAIT FOR clk_period;
      WAIT FOR clk_period;
      
      pc_write <= '0';
      offset <= (OTHERS => 'X');
      
      WAIT FOR clk_period;
      WAIT FOR clk_period;
      
      flush <= '1';
            
            WAIT;
   END PROCESS;
END;