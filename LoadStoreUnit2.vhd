library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity loadstoreunit is
  
PORT (
    ra, rb, rc, rt: IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    result_address : OUT STD_LOGIC_VECTOR(0 TO 31);
    clk: IN STD_ULOGIC);
END loadstoreunit;

ARCHITECTURE behave OF loadstoreunit IS


component FU_register is
   port (FUR_indata   : in std_logic_vector ( 0 to 127);
               reset  : in std_logic :='0';
               clk    : in std_logic;
          FUR_outdata : out std_logic_vector ( 0 to 127));
end component;

signal  result_s1 : STD_LOGIC_VECTOR(0 TO 31);
signal  result_s2 : STD_LOGIC_VECTOR(0 TO 31);
signal dummy1 : STD_LOGIC_VECTOR ( 32 to 127) := (others => 'Z');
--signal  result_s3 : STD_LOGIC_VECTOR(0 TO 127);

signal reset_s1 : std_logic :='0';
signal reset_s2 : std_logic :='0';
  
BEGIN
  
   reg1map : FU_register PORT MAP
   (
    FUR_indata ( 0 to 31) => result_s1,
    FUR_indata ( 32 to 127) =>  dummy1 ( 32 to 127),
    reset => reset_s1,
    clk   => clk,
    FUR_outdata ( 0 to 31) => result_s2 ( 0 to 31),
    FUR_outdata ( 32 to 127) =>  dummy1 ( 32 to 127)
  );
  
   reg2map : FU_register PORT MAP
   (
    FUR_indata ( 0 to 31) => result_s2,
     FUR_indata ( 32 to 127) =>  dummy1 ( 32 to 127),
    reset => reset_s2,
    clk   => clk,
    FUR_outdata ( 0 to 31 ) => result_address ( 0 to 31),--_s3
    FUR_outdata ( 32 to 127) => dummy1 ( 32 to 127)
  ); 
  PROCESS(clk)

  variable immv10 :  UNSIGNED(0 TO 9);
  variable immv8 :   UNSIGNED(0 TO 7);
  variable immv16 :  UNSIGNED(0 TO 15);
  variable ext :     UNSIGNED(0 TO 13);
  variable ext2 :    UNSIGNED(0 TO 17);
  variable zeroext : UNSIGNED(0 TO 3) := "0000";

  variable in_ra :   UNSIGNED(0 TO 31);
  variable in_rb :   UNSIGNED(0 TO 31);
  variable value1 :  UNSIGNED(0 TO 31);
  variable value2 :  UNSIGNED(0  TO 31);
  variable lsa :     UNSIGNED(0 TO 31);
  
  

  begin -- process
    if clk'event and clk = '1'
    then -- clk
     
  
        immv16 ( 0 to 15) := UNSIGNED(rc(0 to 15));  
        immv10 ( 0 to 9) :=  UNSIGNED(rc(0 to 9));  
        
        
      
      if opcode = "00001"   -- load d form
      then

        in_ra (0 to 31) := UNSIGNED(ra (0 to 31));
        ext (0 to 13) := immv10(0 to 9) & zeroext( 0 to 3);
        value1(0 to 31):= (14 to 31 => ext(13)) & ext;
        value2(0 to 31) := value1(0 to 31) + in_ra(0 to 31);
        lsa(0 to 31) := value2 and X"0003FFFF" and X"FFFFFFF0";

        -- load data from this address
       -- result_address <= STD_LOGIC_VECTOR(lsa);
        
      elsif opcode = "00010"   -- load x form
      then
  
        in_ra(0 to 31) := UNSIGNED(ra (0 to 31));
        in_rb(0 to 31) := UNSIGNED(rb (0 to 31));
        lsa(0 to 31) := (in_ra + in_rb) and X"0003FFFF" and X"FFFFFFF0";
       -- result_address <= STD_LOGIC_VECTOR(lsa);

      elsif opcode = "00011"   -- load a form
      then

        ext2(0 to 17) := immv16 & b"00";
        value1(0 to 31):= (18 to 31 => ext2(17)) & ext2;
        lsa := value1 and X"0003FFFF" and X"FFFFFFF0";
       -- result_address <= STD_LOGIC_VECTOR(lsa);

      elsif opcode = "00100" -- load ira form
      then  
        
        ext2(0 to 17) := immv16 & b"00";
        value1(0 to 31):= ((18 to 31 => ext2(17)) & ext2)+ UNSIGNED(ra(0 to 31));  -- PC is multiplexed at ra input. remember
        lsa := value1 and X"0003FFFF" and X"FFFFFFF0";
       -- result_address <= STD_LOGIC_VECTOR(lsa);

      elsif opcode = "00101" -- store d form
      then

        ext(0 to 13) := immv10 & b"0000";
        value1(0 to 31):= ((14 to 31 => ext(13)) & ext)+ UNSIGNED(ra(0 to 31));  -- PC is multiplexed at ra input. remember
        lsa := value1 and X"0003FFFF" and X"FFFFFFF0";
       -- result_address <= STD_LOGIC_VECTOR(lsa);

      elsif opcode = "00110"   -- store x form
      then
  
        in_ra(0 to 31) := UNSIGNED(ra (0 to 31));
        in_rb(0 to 31) := UNSIGNED(rb (0 to 31));
        lsa(0 to 31) := (in_ra + in_rb) and X"0003FFFF" and X"FFFFFFF0";
     --   result_address <= STD_LOGIC_VECTOR(lsa);

      elsif opcode = "00111"   -- store a form
      then

        ext2(0 to 17) := immv16 & b"00";
        value1(0 to 31):= (31 downto 18 => ext2(17)) & ext2;
        lsa := value1 and X"0003FFFF" and X"FFFFFFF0";
       -- result_address <= STD_LOGIC_VECTOR(lsa);

      elsif opcode = "01000" -- store ira form
      then

        ext2(0 to 17) := immv16 & b"00";
        value1(0 to 31):= ((31 downto 18 => ext2(17)) & ext2)+ UNSIGNED(ra(0 to 31));  -- PC is multiplexed at ra input. remember
        lsa := value1 and X"0003FFFF" and X"FFFFFFF0";
     --   result_address <= STD_LOGIC_VECTOR(lsa);
    
      end if;
       IF enable = '1' THEN
         result_s1 <= STD_LOGIC_VECTOR(lsa);
      ELSE
        result_s1<= (OTHERS => 'Z');
      end if;
      
      else -- clk
  
    --  END IF; -- enable
    end if; -- end clk
  end process;
end behave;  