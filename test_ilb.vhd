--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   01:04:09 03/29/2013
-- Design Name:   
-- Module Name:   C:/Users/SAMBHAV/xilinx_work/ILB_SPU/test_ILB.vhd
-- Project Name:  ILB_SPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ILB_SPU
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_ILB IS
END test_ILB;
 ARCHITECTURE behavior OF test_ILB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ILB_SPU
    PORT(
         clk : IN  std_logic;
         ins_in : IN  std_logic_vector(511 downto 0);
         in_enable : IN  std_logic;
         out_enable : IN  std_logic;
         ins_out : OUT  std_logic_vector(63 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '1';
   signal ins_in : std_logic_vector(511 downto 0) := (others => 'X');
   signal in_enable : std_logic := '0';
   signal out_enable : std_logic := '0';

 	--Outputs
   signal ins_out : std_logic_vector(63 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ILB_SPU PORT MAP (
          clk => clk,
          ins_in => ins_in,
          in_enable => in_enable,
          out_enable => out_enable,
          ins_out => ins_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
   --   wait for 100 ns;	
		in_enable <= '1';
		--	out_enable <= '1';
     -- wait for clk_period*10;
		  --opcode <= "00011";
      ins_in <= X"12ff345F6789F1023987F546432F12ff982F2567F5423875F4596852F21ff451F5825369F0023020F0012300F00ff000F0076300F002301234578960F002468F";
      
		--out_enable <= '0';
     -- WAIT FOR 10 ns;
--		 ins_in <= X"23ff000F0000000F0023000F0000002300ff000F0000000F0023000F0000000F00ff000F0000000F0023000F0000000F00ff000F0000000F0023000F0000000F";
     WAIT FOR clk_period;
		out_enable <= '1';
      
		-- in_enable <= '1';
		--out_enable <= '1';
		-- ins_in <= X"23ff000F0000000F0023000F0000002323ff000F0000000F0023000F0000002323ff000F0000000F0023000F0000002323ff000F0000000F0023000F00000023";
	   --WAIT FOR 10 ns;
		-- ins_in <= X"23ff000F0000000F0023000F0000002323ff000F0000000F0023000F0000002323ff000F0000000F0023000F0000002323ff000F0000000F0023000F00000023";
   
   -- insert stimulus here 

      wait;
   end process;

END;
