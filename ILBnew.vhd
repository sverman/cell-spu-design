LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY ILB IS
  
    port(
      clk : IN STD_LOGIC;
                        in_enable : IN STD_LOGIC;
                        out_enable : IN STD_LOGIC;
                        
                        ins_in : IN  STD_LOGIC_VECTOR (0 TO 1023);
                        ins_out : OUT STD_LOGIC_VECTOR (0 TO 63);
                        pc_ilb : OUT STD_LOGIC_VECTOR (0 TO 63);
                        
                        flush : IN BIT;
                        pc_write : IN BIT;
                        offset : IN STD_LOGIC_VECTOR (0 TO 4):= (others => '0')
                        );
                                
END ILB;

ARCHITECTURE Behavioral OF ILB IS
  
  TYPE VECTOR_ARRAY IS ARRAY(0 TO 31) OF STD_LOGIC_VECTOR(0 TO 31);
  SIGNAL INST_ARRAY : VECTOR_ARRAY := (OTHERS=>(OTHERS=>'0')); -- VECTOR_ARRAY is a 32 element array of 32-bit vecTOrs.
  CONSTANT clk_period : time := 10 ns;
  SIGNAL index : INTEGER := 0;

BEGIN

  PROCESS(clk)
  BEGIN

    IF clk = '1' THEN
                  
                  IF flush = '1' THEN
                    -- Purge Instruction Array
                    INST_ARRAY <= (OTHERS=>(OTHERS=>'Z'));
                    index <= 0;
                    -- SIGNAL TO load from Local STOre has been sent TO Local STOre form the branch unit
                    -- Reset flust TO 0 After clock period
                    -- flush <= '0' AFTER clk_period;
                  END IF;
                  
                  IF pc_write = '1' THEN
                    -- Branch Instruction in Progress
                    -- Reset InstruciTOn Array Index
                    index <= TO_INTEGER(UNSIGNED(offset));
                  END IF;
                  
                  -- Load instructions from Local STOre
                  IF in_enable = '1' AND flush = '0' THEN
                    -- Populate the INSTRUCTION ARRAY
                    FOR i IN 0 TO 31 LOOP
                      INST_ARRAY(i) <= ins_in((i*32) TO ((i*32)+31));
                    END LOOP;
                  END IF;
                
                  -- Start Instruction Issue
                  IF out_enable = '1' AND flush = '0' THEN
                  -- Dual Issue
                  ins_out(0 TO 31) <= INST_ARRAY(index);
                  ins_out(32 TO 63) <= INST_ARRAY(index + 1);
                  -- Update PC
                    pc_ilb(0 TO 31) <= INST_ARRAY(index);
                    pc_ilb(32 TO 63) <= INST_ARRAY(index + 1); 
                    
                  -- Manage Instruction Array Index
                
                    IF index >= 0  and index  <= 29 THEN
                      index <= index + 2 AFTER clk_period;
                       ELSIF index > 29 THEN
                      index <= 0;
                     
                  -- load new ilb data
                    END IF;
                          
                  ELSE
                    ins_out <= (OTHERS => 'Z');
                    pc_ilb <= (OTHERS => 'Z');
                  END IF;
         
         END IF; -- clk
END PROCESS;
END Behavioral;
