library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity SPU is  
PORT (
        clk : in STD_LOGIC;
	    		ins_stream : in  STD_LOGIC_VECTOR (0 to 1023); -- 64 B read from Local Store to ILB. 
				in_enable  : in STD_LOGIC; -- signals for ILB
				out_enable : in STD_LOGIC -- signals for ILB
      );
end SPU;
 
ARCHITECTURE behave of SPU is
  -- signals for SPU
 signal clk_SPU : STD_LOGIC;
  
  
  -- signals for ILB
 --signal clk_ilb :         STD_LOGIC;                       
 signal ins_in_ilbs :      STD_LOGIC_VECTOR (0 to 1023); 
 signal in_enable_ilbs :   STD_LOGIC;                       
 signal out_enable_ilbs :  STD_LOGIC;          
 signal flush_ilbs : bit:='0';
 signal pc_write_ilbs : bit;
                     
 signal ins_out_ilbs :     STD_LOGIC_VECTOR (0 to 63):=X"0000000000000000";
 signal offset_ilbs : std_logic_vector (0 to 4):= "00000";

-- signals for ILB end here

-- signals for decode unit begin here
   signal inst_0_dec, inst_1_dec :  STD_LOGIC_VECTOR(0 TO 31):=X"00000000";
   signal opcode_0_dec, opcode_1_dec : STD_LOGIC_VECTOR(0 TO 8):="000000000";
    
   signal ra1_addr_dec  :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal rb1_addr_dec  :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal rc1_addr_dec  :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal rt1_addr_dec  :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal imm7_1_dec    :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal imm8_1_dec    :  STD_LOGIC_VECTOR(0 TO 7):= "00000000";
   signal imm10_1_dec   :  STD_LOGIC_VECTOR(0 TO 9):= "0000000000";
   signal imm16_1_dec   :  STD_LOGIC_VECTOR(0 TO 15):= "0000000000000000";
    
   signal ra2_addr_dec  :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal rb2_addr_dec  :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal rc2_addr_dec  :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal rt2_addr_dec  :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal imm7_2_dec    :  STD_LOGIC_VECTOR(0 TO 6):= "0000000";
   signal imm8_2_dec    :  STD_LOGIC_VECTOR(0 TO 7):= "00000000";
   signal imm10_2_dec   :  STD_LOGIC_VECTOR(0 TO 9):= "0000000000";
   signal imm16_2_dec   :  STD_LOGIC_VECTOR(0 TO 15):= "0000000000000000";
   
   signal RegWr_ins0_dec  : BIT;
   signal MemRd_ins0_dec  : BIT;
   signal MemWr_ins0_dec  : BIT;
    
   signal RegWr_ins1_dec  : BIT;
   signal MemRd_ins1_dec  : BIT;
   signal MemWr_ins1_dec  : BIT;
   
   signal i_inst_dec     : STD_LOGIC_VECTOR(0 TO 31);
   signal o_inst_dec     : STD_LOGIC_VECTOR(0 TO 31);
    
   signal i_inst_sel_dec : STD_ULOGIC :='0';
   signal o_inst_sel_dec : STD_ULOGIC :='0';
   
    
   signal IMM_SEL1_dec  : BIT_vector ( 0 to 1 );
   signal IMM_SEL2_dec  : BIT_vector ( 0 to 1 );
          
   signal delayILB_dec  :      STD_ULOGIC;
   signal enable_decunit_dec : STD_ULOGIC;
   
    --signals for decode unit end here
    
    --signals for ID_DEPchkreg
    
    signal inst_0_s1, inst_1_s1 : STD_LOGIC_VECTOR(0 TO 31):= X"00000000";
    
    signal opcode_0_s1, opcode_1_s1 :  STD_LOGIC_VECTOR(0 TO 8);   
    signal ra1_addr_s1 :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb1_addr_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc1_addr_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt1_addr_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_1_s1    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_s1    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_s1   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_s1   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal ra2_addr_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb2_addr_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc2_addr_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt2_addr_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_2_s1    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_2_s1    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_2_s1   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_2_s1  : STD_LOGIC_VECTOR(0 TO 15);
    
    signal PC_s1 :  STD_LOGIC_VECTOR(0 TO 31);
    
    signal RegWr_ins0_s1  : BIT;     
    signal MemRd_ins0_s1  : BIT;     
    signal MemWr_ins0_s1  : BIT;     
                                 
    signal RegWr_ins1_s1  : BIT;     
    signal MemRd_ins1_s1  : BIT;     
    signal MemWr_ins1_s1  : BIT;     
                                 
    signal IMM_SEL1_s1  : BIT_vector ( 0 to 1);     
    signal IMM_SEL2_s1  : BIT_vector( 0 to 1);     

    
    signal opcode_0_PR1_s1, opcode_1_PR1_s1 :  STD_LOGIC_VECTOR(0 TO 8);    
    signal ra1_addr_PR1_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb1_addr_PR1_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc1_addr_PR1_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt1_addr_PR1_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_1_PR1_s1    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_PR1_s1    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_PR1_s1   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_PR1_s1   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal ra2_addr_PR1_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb2_addr_PR1_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc2_addr_PR1_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt2_addr_PR1_s1  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_2_PR1_s1    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_2_PR1_s1    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_2_PR1_s1   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_2_PR1_s1   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal RegWr_ins0_PR1_s1  : BIT;     
    signal MemRd_ins0_PR1_s1  : BIT;     
    signal MemWr_ins0_PR1_s1  : BIT;     
                                 
    signal RegWr_ins1_PR1_s1  : BIT;     
    signal MemRd_ins1_PR1_s1  : BIT;     
    signal MemWr_ins1_PR1_s1  : BIT;     
                                 
    signal IMM_SEL1_PR1_s1  : BIT_vector ( 0 to 1);     
    signal IMM_SEL2_PR1_s1  : BIT_vector ( 0 to 1);  
    
    signal PC_PR1_s1 :  STD_LOGIC_VECTOR(0 TO 31);
    
    signal reset_s1 :   STD_ULOGIC := '0';

    --signals for ID_DEPchkreg end here

    -- SIGNALS for DATA HAZARD stage (DH2) start here
    signal inst_0_s2, inst_1_s2 : STD_LOGIC_VECTOR(0 TO 31):= X"00000000";
    signal opcode_0_DH_s2  : STD_LOGIC_VECTOR(0 TO 8);   
    signal ra0_addr_DH_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rb0_addr_DH_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rc0_addr_DH_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rt0_addr_DH_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_0_DH_s2    : STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_0_DH_s2    : STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_0_DH_s2   : STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_0_DH_s2   : STD_LOGIC_VECTOR(0 TO 15);
    signal RegWr_ins0_DH_s2  : BIT;
    signal MemRd_ins0_DH_s2  : BIT;
    signal MemWr_ins0_DH_s2  : BIT;
    
    signal opcode_1_DH_s2  : STD_LOGIC_VECTOR(0 TO 8);
    signal ra1_addr_DH_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rb1_addr_DH_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rc1_addr_DH_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rt1_addr_DH_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_1_DH_s2    : STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_DH_s2    : STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_DH_s2   : STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_DH_s2   : STD_LOGIC_VECTOR(0 TO 15);    
    signal RegWr_ins1_DH_s2  : BIT;
    signal MemRd_ins1_DH_s2  : BIT;
    signal MemWr_ins1_DH_s2  : BIT;
    
    signal PC_DH_s2 : STD_LOGIC_VECTOR(0 TO 31);
    
   
    signal opcode_0_ISS_s2  :  STD_LOGIC_VECTOR(0 TO 8);    
    signal ra0_addr_ISS_s2  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb0_addr_ISS_s2  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc0_addr_ISS_s2  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt0_addr_ISS_s2  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_0_ISS_s2    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_0_ISS_s2    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_0_ISS_s2   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_0_ISS_s2   :  STD_LOGIC_VECTOR(0 TO 15);
    signal RegWr_ins0_ISS_s2  :  BIT;
    signal MemRd_ins0_ISS_s2  :  BIT;
    signal MemWr_ins0_ISS_s2  :  BIT;
    
    
    signal opcode_1_ISS_s2  : STD_LOGIC_VECTOR(0 TO 8);
    signal ra1_addr_ISS_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rb1_addr_ISS_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rc1_addr_ISS_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal rt1_addr_ISS_s2  : STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_1_ISS_s2    : STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_ISS_s2    : STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_ISS_s2   : STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_ISS_s2   : STD_LOGIC_VECTOR(0 TO 15);
   
    signal RegWr_ins1_ISS_s2  : BIT;
    signal MemRd_ins1_ISS_s2  : BIT;
    signal MemWr_ins1_ISS_s2  : BIT;
    
    signal PC_ISS_s2 :  STD_LOGIC_VECTOR(0 TO 31);
    signal reset_s2 :  std_logic;
    signal firstInstr_s2 :  BIT;
    signal enable_s2 :  STD_LOGIC := '1';
  

    -- SIGNALS for DATA HAZARD stage end here
    
    -- SIGNALS for Depchk_rfreg stage start here

 signal inst_0_s25, inst_1_s25 : STD_LOGIC_VECTOR(0 TO 31):= X"00000000";
    
    signal opcode_0_s25, opcode_1_s25 :  STD_LOGIC_VECTOR(0 TO 8);   
    signal ra1_addr_s25 :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb1_addr_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc1_addr_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt1_addr_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_1_s25    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_s25    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_s25   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_s25   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal ra2_addr_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb2_addr_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc2_addr_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt2_addr_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_2_s25    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_2_s25    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_2_s25   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_2_s25  : STD_LOGIC_VECTOR(0 TO 15);
    
    signal PC_s25 :  STD_LOGIC_VECTOR(0 TO 31);
    
    signal RegWr_ins0_s25  : BIT;     
    signal MemRd_ins0_s25  : BIT;     
    signal MemWr_ins0_s25  : BIT;     
                                 
    signal RegWr_ins1_s25  : BIT;     
    signal MemRd_ins1_s25  : BIT;     
    signal MemWr_ins1_s25  : BIT;     
                                 
    signal IMM_SEL1_s25  : BIT_vector ( 0 to 1);     
    signal IMM_SEL2_s25  : BIT_vector( 0 to 1);     

    
    signal opcode_0_PR5_s25, opcode_1_PR5_s25 :  STD_LOGIC_VECTOR(0 TO 8);    
    signal ra1_addr_PR5_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb1_addr_PR5_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc1_addr_PR5_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt1_addr_PR5_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_1_PR5_s25    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_PR5_s25    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_PR5_s25   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_PR5_s25   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal ra2_addr_PR5_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb2_addr_PR5_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc2_addr_PR5_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt2_addr_PR5_s25  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_2_PR5_s25    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_2_PR5_s25    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_2_PR5_s25   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_2_PR5_s25   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal RegWr_ins0_PR5_s25  : BIT;     
    signal MemRd_ins0_PR5_s25  : BIT;     
    signal MemWr_ins0_PR5_s25  : BIT;     
                                 
    signal RegWr_ins1_PR5_s25  : BIT;     
    signal MemRd_ins1_PR5_s25  : BIT;     
    signal MemWr_ins1_PR5_s25  : BIT;     
                                 
    signal IMM_SEL1_PR5_s25  : BIT_vector ( 0 to 1);     
    signal IMM_SEL2_PR5_s25  : BIT_vector ( 0 to 1);  
    
    signal PC_PR5_s25 :  STD_LOGIC_VECTOR(0 TO 31);
    
    signal reset_s25 :   STD_ULOGIC := '0';


    
    -- SIGNALS for Depchk_rfreg stage end here
    
    
    --signals for register file begin here
    signal ra1_addr_s3  : std_logic_vector(0 to 6);
    signal rb1_addr_s3  : std_logic_vector(0 to 6);
    signal rc1_addr_s3  : std_logic_vector(0 to 6);
    signal rt1_addr_s3  : std_logic_vector(0 to 6);
     
    signal ra2_addr_s3  : std_logic_vector(0 to 6);
    signal rb2_addr_s3  : std_logic_vector(0 to 6);
    signal rc2_addr_s3  : std_logic_vector(0 to 6);
    signal rt2_addr_s3  : std_logic_vector(0 to 6);
     
    signal ra1_data_s3 :  std_logic_vector(0 to 127);-- the six read ports
    signal rb1_data_s3 :  std_logic_vector(0 to 127);--
    signal rc1_data_s3 :  std_logic_vector(0 to 127);--
     
    signal ra2_data_s3 : std_logic_vector(0 to 127);--
    signal rb2_data_s3 : std_logic_vector(0 to 127);--
    signal rc2_data_s3 : std_logic_vector(0 to 127);--
     
    signal rt1_data_s3 : std_logic_vector(0 to 127);-- the two write ports
    signal rt2_data_s3 : std_logic_vector(0 to 127);--
    
    signal rt1_enable_s3 :  bit;
    signal rt2_enable_s3 :  bit;
     
    --signal PC_s1 : std_logic;
    --signals for register file end here
    
    -- --signals for decoder multiplexer begin here
    
    signal mux_rc1 :  STD_LOGIC_VECTOR(0 TO 127);
    signal mux_imm10_1 :  STD_LOGIC_VECTOR(0 TO 9);
    signal mux_imm16_1:  STD_LOGIC_VECTOR(0 TO 15);
    signal mux_out1 : STD_LOGIC_VECTOR(0 TO 127);
    signal mux_sel1 : BIT;
    --clk:  STD_LOGIC;
    
    signal mux_rc2 : STD_LOGIC_VECTOR(0 TO 127);
    signal mux_imm10_2  : STD_LOGIC_VECTOR(0 TO 9);
    signal mux_imm16_2  : STD_LOGIC_VECTOR(0 TO 15);
   signal  mux_out2 :  STD_LOGIC_VECTOR(0 TO 127);
   signal  mux_sel2 :  BIT;
    --clk:  STD_LOGIC;
    --signals for decoder multiplexer end here

    --signals for RF_EXregister begin here
    signal ra1_s4, rb1_s4, rc1_s4, rt1_s4 :  STD_LOGIC_VECTOR(0 TO 127);
    signal imm7_1_s4    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_s4    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_s4   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_s4   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal ra2_s4, rb2_s4, rc2_s4, rt2_s4 :  STD_LOGIC_VECTOR(0 TO 127);
    signal imm7_2_s4    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_2_s4    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_2_s4   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_2_s4   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal opcode_0_s4 :  STD_LOGIC_VECTOR(0 TO 7);
    signal opcode_1_s4 :  STD_LOGIC_VECTOR(0 TO 7);
    
     signal RegWr_ins0_s4  : BIT;  
     signal MemRd_ins0_s4  : BIT;  
     signal MemWr_ins0_s4  : BIT;  
                               
     signal RegWr_ins1_s4  : BIT;  
     signal MemRd_ins1_s4  : BIT;  
     signal MemWr_ins1_s4  : BIT;  
                               
     signal IMM_SEL1_s4  : BIT_vector( 0 to 1);    
     signal IMM_SEL2_s4  : BIT_vector ( 0 to 1);    

     
     signal PC_s4 :  STD_LOGIC_VECTOR(0 TO 31);
    
     signal ra1_PR3_s4, rb1_PR3_s4, rc1_PR3_s4, rt1_PR3_s4 : STD_LOGIC_VECTOR(0 TO 127);
     signal imm7_1_PR3_s4    :  STD_LOGIC_VECTOR(0 TO 6);
     signal imm8_1_PR3_s4    :  STD_LOGIC_VECTOR(0 TO 7);
     signal imm10_1_PR3_s4   :  STD_LOGIC_VECTOR(0 TO 9);
     signal imm16_1_PR3_s4   :  STD_LOGIC_VECTOR(0 TO 15);
    
     signal ra2_PR3_s4, rb2_PR3_s4, rc2_PR3_s4, rt2_PR3_s4 : STD_LOGIC_VECTOR(0 TO 127);
    signal imm7_2_PR3_s4    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_2_PR3_s4    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_2_PR3_s4   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_2_PR3_s4   :  STD_LOGIC_VECTOR(0 TO 15);
    
    signal opcode_0_PR3_s4 :  STD_LOGIC_VECTOR(0 TO 8);
    signal opcode_1_PR3_s4 :  STD_LOGIC_VECTOR(0 TO 8);
    
    signal RegWr_ins0_PR3_s4  : BIT;
    signal MemRd_ins0_PR3_s4  : BIT;
    signal MemWr_ins0_PR3_s4  : BIT;
                                 
    signal RegWr_ins1_PR3_s4  : BIT;
    signal MemRd_ins1_PR3_s4  : BIT;
    signal MemWr_ins1_PR3_s4  : BIT;
                                 
    signal IMM_SEL1_PR3_s4  : BIT_vector( 0 to 1);  
    signal IMM_SEL2_PR3_s4  : BIT_vector ( 0 to 1);  

    
    signal PC_PR3_s4 :  STD_LOGIC_VECTOR(0 TO 31);    
    
    signal reset_s4 :  STD_ULOGIC :='0';
    --signals for RF_EXregister end here
    
    --signals for frwdmux_even begin here--
    signal	fwe1_s5      	: std_logic_vector ( 0 to 127);
		signal fwe2_s5	      : std_logic_vector ( 0 to 127);
		signal fwe3_s5      	: std_logic_vector ( 0 to 127);
		signal fwo1_s5      	: std_logic_vector ( 0 to 127);
		signal fwo2_s5      	: std_logic_vector ( 0 to 127);
		signal fwo3_s5      	: std_logic_vector ( 0 to 127);
		signal rf_s5	        : std_logic_vector ( 0 to 127);
		signal frwd_cntrl_s5_a1	: bit_vector (0 to 2):="000";
		signal frwd_cntrl_s5_b1	: bit_vector (0 to 2):="000";
		signal frwd_cntrl_s5_c1	: bit_vector (0 to 2):="000";
		signal regout_s5     :	std_logic_vector (0 to 127);
    --signals for frwdmux_even end here
    
    --signals for frwdmux_odd begin here
    signal	fwe1_s6      	: std_logic_vector ( 0 to 127);
		signal fwe2_s6	      : std_logic_vector ( 0 to 127);
		signal fwe3_s6      	: std_logic_vector ( 0 to 127);
		signal fwo1_s6      	: std_logic_vector ( 0 to 127);
		signal fwo2_s6      	: std_logic_vector ( 0 to 127);
		signal fwo3_s6      	: std_logic_vector ( 0 to 127);
		signal rf_s6	        : std_logic_vector ( 0 to 127);
    signal frwd_cntrl_s5_a2	: bit_vector (0 to 2):="000";
		signal frwd_cntrl_s5_b2	: bit_vector (0 to 2):="000";
		signal frwd_cntrl_s5_c2	: bit_vector (0 to 2):="000";
			signal regout_s6     :	std_logic_vector (0 to 127);
--    --signals for frwdmux_odd end here
    
    --signals for evenpipe begin here
    signal ra_s7, rb_s7, rc_s7, rt_s7 :  STD_LOGIC_VECTOR(0 TO 127);
    signal sel_s7 :  STD_ULOGIC;
    signal opcode_s7 :  STD_LOGIC_VECTOR(0 TO 7);
    signal RegWr_ins0_ep_s7  : BIT;
    signal MemRd_ins0_ep_s7  : BIT;
    signal MemWr_ins0_ep_s7  : BIT;
    signal PC_ep_s7 : STD_LOGIC_VECTOR(0 TO 31);
    signal RegWr_ins0_ep_o_s7  : BIT;
    signal MemRd_ins0_ep_o_s7  : BIT;
    signal MemWr_ins0_ep_o_s7  : BIT;
    signal PC_ep_o_s7 : STD_LOGIC_VECTOR(0 TO 31);
    signal result_data1_s7 :  STD_LOGIC_VECTOR ( 0 to 127);
    signal result_data2_s7 :  STD_LOGIC_VECTOR ( 0 to 127);
    signal result_data3_s7 :  STD_LOGIC_VECTOR ( 0 to 127);
    signal regout_s5_ra1  :  STD_LOGIC_VECTOR ( 0 to 127);
    signal regout_s5_rb1  :  STD_LOGIC_VECTOR ( 0 to 127);
    signal regout_s5_rc1  :  STD_LOGIC_VECTOR ( 0 to 127);
--    --signals for evenpipe end here
    
    --signals for oddpipe begin here
    signal ra_s8, rb_s8, rc_s8, rt_s8 :  STD_LOGIC_VECTOR(0 TO 127);
    signal opcode_s8 : STD_LOGIC_VECTOR(0 TO 7);
    signal sel_s8 :  STD_ULOGIC;
    signal RegWr_ins1_ep_s8  : BIT;
    signal MemRd_ins1_ep_s8  : BIT;
    signal MemWr_ins1_ep_s8  : BIT;
    signal PC_ep_s8 : STD_LOGIC_VECTOR(0 TO 31);
    signal RegWr_ins1_ep_o_s8  : BIT;
    signal MemRd_ins1_ep_o_s8  : BIT;
    signal MemWr_ins1_ep_o_s8  : BIT;
    signal PC_ep_o_s8 : STD_LOGIC_VECTOR(0 TO 31);

    signal result_address1_s8 :  STD_LOGIC_VECTOR(0 TO 31);
    signal result_address2_s8 :  STD_LOGIC_VECTOR(0 TO 31);
    signal result_data_s8 :  STD_LOGIC_VECTOR ( 0 to 127);
    signal regout_s5_ra2  :  STD_LOGIC_VECTOR ( 0 to 127);
    signal regout_s5_rb2  :  STD_LOGIC_VECTOR ( 0 to 127);
    signal regout_s5_rc2  :  STD_LOGIC_VECTOR ( 0 to 127);
    signal tag_address_s8 : STD_LOGIC_VECTOR(0 TO 31); -- program counter
    signal flush_s8 : BIT;
    signal load_LS_s8 : BIT;
    signal pc_write_s8 : BIT;
    signal offset_s8 : STD_LOGIC_VECTOR(0 TO 4);
  
    --signals for oddpipe end here
    
    --signals for fwe_even begin here
    signal fpu_result_s9:  STD_LOGIC_VECTOR(0 TO 127);
    signal fxu_result_s9:  STD_LOGIC_VECTOR(0 TO 127);
    signal byte_result_s9: STD_LOGIC_VECTOR(0 TO 127);
    signal reset_s9 :  STD_LOGIC := '0';
    signal fwe1_enable_s9 :  STD_LOGIC;
    signal fwe2_enable_s9 :  STD_LOGIC;
    signal fwe3_enable_s9 :  STD_LOGIC;
    signal fwe1_dataout_bus_s9 :  STD_LOGIC_VECTOR(0 TO 127);
    signal fwe2_dataout_bus_s9 :  STD_LOGIC_VECTOR(0 TO 127);
    signal fwe3_dataout_bus_s9 :  STD_LOGIC_VECTOR(0 TO 127);
    signal fwe_out_bus_s9 :  STD_LOGIC_VECTOR(0 TO 127);    
    --signals for fwe_even end here
    
    --signals for fwe_odd begin here
    signal lsu_result_s10 :  STD_LOGIC_VECTOR(0 TO 31);
    signal bru_result_s10 :  STD_LOGIC_VECTOR(0 TO 31);
    signal perm_result_s10 :  STD_LOGIC_VECTOR(0 TO 127);
    signal reset_s10 :  STD_LOGIC :='0';
    signal fwo1_enable_s10 :  STD_LOGIC;
    signal fwo2_enable_s10 :  STD_LOGIC;
    signal fwo3_enable_s10 :  STD_LOGIC;
    signal fwo1_dataout_bus_s10 :  STD_LOGIC_VECTOR(0 TO 127);
    signal fwo2_dataout_bus_s10 :  STD_LOGIC_VECTOR(0 TO 127);
    signal fwo3_dataout_bus_s10 :  STD_LOGIC_VECTOR(0 TO 127);
    signal fwo_out_bus_s10 :  STD_LOGIC_VECTOR(0 TO 127); 
    --signals for fwe_odd end here
    
    --signals for EX_LSxs reg begin here
    signal result1_s11 :  STD_LOGIC_VECTOR(0 TO 127); 
    signal result2_s11 :  STD_LOGIC_VECTOR(0 TO 127);
    
    signal rt1_s11 :  STD_LOGIC_VECTOR(0 TO 6); 
    signal rt2_s11 :  STD_LOGIC_VECTOR(0 TO 6); 
    signal MemReg1_s11 : std_logic;
    signal MemReg2_s11 : std_logic;
    
    signal wr1_ls_s11 : std_logic;
    signal wr2_ls_s11 : std_logic;
            
    signal w_rt1_s11 :  STD_ULOGIC;
    signal w_rt2_s11 :  STD_ULOGIC;
    
    signal PC_s11 :     STD_LOGIC_VECTOR(0 TO 31);
	  signal PC_wr_s11 :  STD_ULOGIC; 
    
    signal result1_PR4_s11  :  STD_LOGIC_VECTOR(0 TO 127);
    signal result2_PR4_s11  :  STD_LOGIC_VECTOR(0 TO 127);
    
    signal RegWrite1_PR4_s11 :  BIT;
    signal RegWrite2_PR4_s11 :  BIT;
   
    
    signal rt1_PR4_s11 :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt2_PR4_s11 :  STD_LOGIC_VECTOR(0 TO 6);
    
    signal MemReg1_PR4_s11 : std_logic;
    signal MemReg2_PR4_s11 : std_logic;
    signal enable_ls_PR4_s11 : std_logic;
    signal wr1_ls_PR4_s11 : std_logic;
    signal wr2_ls_PR4_s11 : std_logic;    
    signal w_rt1_PR4_s11 :  STD_ULOGIC;
    signal w_rt2_PR4_s11 :  STD_ULOGIC;
	  signal PC_wr_PR4_s11 :  STD_ULOGIC;
    
    signal PC_PR4_s11 :  STD_LOGIC_VECTOR(0 TO 31);
    
    signal reset_s11 :  STD_ULOGIC;
--  signals for EX_LSxs reg end here


--  signals for Local Store Access begin here

    signal datain_s12  : std_logic_vector(0 TO 127);
    signal rw_address_s12  :  std_logic_vector (0 to 31);
    signal wr1_e_s12  :  std_logic;
    signal wr2_e_s12  :  std_logic;
    signal dataout1_s12  :  std_logic_vector(0 to 127);
    signal dataout2_s12  :  std_logic_vector(0 to 1023);

--  signals for Local Store Access end here


--  signals for LSxs_WB reg begin here
    signal result1_s13, rt1_s13 :  STD_LOGIC_VECTOR(0 TO 127);
    signal result2_s13, rt2_s13 :  STD_LOGIC_VECTOR(0 TO 127);
    
    signal w_rt1_s13 :  STD_ULOGIC;
    signal w_rt2_s13 :  STD_ULOGIC;
    
    signal PC_s13 :  STD_LOGIC_VECTOR(0 TO 31);
	  signal PC_wr_s13 :  STD_ULOGIC; 
    
    signal result1_PR7_s13, rt1_PR7_s13 :  STD_LOGIC_VECTOR(0 TO 127);
    signal result2_PR7_s13, rt2_PR7_s13 :  STD_LOGIC_VECTOR(0 TO 127);
    
    signal w_rt1_PR7_s13 :  STD_ULOGIC;
    signal w_rt2_PR7_s13 :  STD_ULOGIC;
	  signal PC_wr_PR7_s13 :  STD_ULOGIC;
    
    signal PC_PR7_s13 :  STD_LOGIC_VECTOR(0 TO 31);
    
    signal reset_s13  :  STD_ULOGIC;
--   
--signals for LS_WBxs reg end here
   
   
--COMPONENT declarations go here
   

component ILB  is
 Port (	 clk :        in STD_LOGIC;
        pc_ilb : out std_logic_vector ( 0 to 63);
        offset : in std_logic_vector ( 0 to 4);
        flush : in bit;
        pc_write : in bit;
				 ins_in :     in  STD_LOGIC_VECTOR (0 to 1023); -- 64 B read from Latch. 
				 in_enable :  in STD_LOGIC;
				 out_enable : in STD_LOGIC;
				 ins_out :    out STD_LOGIC_VECTOR (0 to 63)); -- 2 instructions in 1 cycle
				
end component;

-- the decode unit for instruction decoding 

--component decoder2 IS
--  PORT(
--    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
--    opcode_0, opcode_1 : OUT STD_LOGIC_VECTOR(0 TO 8);
--    
--    ra1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
--    rb1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
--    rc1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
--    rt1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
--    imm7_1    : OUT STD_LOGIC_VECTOR(0 TO 6);
--    imm8_1    : OUT STD_LOGIC_VECTOR(0 TO 7);
--    imm10_1   : OUT STD_LOGIC_VECTOR(0 TO 9);
--    imm16_1   : OUT STD_LOGIC_VECTOR(0 TO 15);
--    
--    ra2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
--    rb2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
--    rc2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
--    rt2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
--    imm7_2    : OUT STD_LOGIC_VECTOR(0 TO 6);
--    imm8_2    : OUT STD_LOGIC_VECTOR(0 TO 7);
--    imm10_2   : OUT STD_LOGIC_VECTOR(0 TO 9);
--    imm16_2   : OUT STD_LOGIC_VECTOR(0 TO 15);
--    
--    RegWr_ins0  : OUT BIT;
--    MemRd_ins0  : OUT BIT;
--    MemWr_ins0  : OUT BIT;
--    
--    RegWr_ins1  : OUT BIT;
--    MemRd_ins1  : OUT BIT;
--    MemWr_ins1  : OUT BIT;
--    
--    IMM_SEL1  : OUT BIT_vector( 0 to 1);
--    IMM_SEL2  : OUT BIT_vector( 0 to 1);
--    delayILB : OUT STD_ULOGIC;
--    enable : IN STD_ULOGIC;
--    clk: IN STD_ULOGIC);
--END component;

component decoder3 IS  PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    opcode_0, opcode_1 : OUT STD_LOGIC_VECTOR(0 TO 8);
    
    ra1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_2    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    -- Register R/W & Memory R/W Signals
    RegWr_ins0  : OUT BIT;
    MemRd_ins0  : OUT BIT;
    MemWr_ins0  : OUT BIT;
    
    RegWr_ins1  : OUT BIT;
    MemRd_ins1  : OUT BIT;
    MemWr_ins1  : OUT BIT;
        
    delayILB : OUT STD_ULOGIC;
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC;
    IMM_SEL1  : OUT BIT_VECTOR (0 to 1) ; -- 00 for rc -- 01 immv10 -- 02 immv16
    IMM_SEL2  : OUT BIT_VECTOR  (0 to 1) ;
      
    i_inst : IN STD_LOGIC_VECTOR(0 TO 31);
    o_inst : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    i_inst_sel : IN STD_ULOGIC;
    o_inst_sel : OUT STD_ULOGIC
    );
END component;

-- pipeline register for ID-Depchk stage
--
component ID_DepChk IS
  PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    
    opcode_0, opcode_1 : IN STD_LOGIC_VECTOR(0 TO 8);   
    ra1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_1    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_2    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    RegWr_ins0  : IN BIT;
    MemRd_ins0  : IN BIT;
    MemWr_ins0  : IN BIT;
    
    RegWr_ins1  : IN BIT;
    MemRd_ins1  : IN BIT;
    MemWr_ins1  : IN BIT;
    
    IMM_SEL1  : IN BIT_vector( 0 to 1);
    IMM_SEL2  : IN BIT_vector ( 0 to 1);
    
    PC : IN STD_LOGIC_VECTOR(0 TO 31);
    
    opcode_0_PR1, opcode_1_PR1 : OUT STD_LOGIC_VECTOR(0 TO 8);    
    ra1_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_PR1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_PR1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_PR1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_PR1   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_2_PR1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2_PR1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2_PR1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2_PR1   : OUT STD_LOGIC_VECTOR(0 TO 15);

    RegWr_ins0_PR1  : OUT BIT;
    MemRd_ins0_PR1  : OUT BIT;
    MemWr_ins0_PR1  : OUT BIT;
    
    RegWr_ins1_PR1  : OUT BIT;
    MemRd_ins1_PR1  : OUT BIT;
    MemWr_ins1_PR1  : OUT BIT;
    
    IMM_SEL1_PR1  : OUT BIT_vector( 0 to 1);
    IMM_SEL2_PR1  : OUT BIT_vector( 0 to 1);
    
    
    PC_PR1 : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    reset : IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
END component;

-- Issue stage for instruction issue

component DH2 is
port (
    opcode_0_DH  : IN STD_LOGIC_VECTOR(0 TO 8);   
    ra0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_0_DH    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_0_DH    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_0_DH   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_0_DH   : IN STD_LOGIC_VECTOR(0 TO 15);
    RegWr_ins0_DH  : IN BIT;
    MemRd_ins0_DH  : IN BIT;
    MemWr_ins0_DH  : IN BIT;
    
    opcode_1_DH  : IN STD_LOGIC_VECTOR(0 TO 8);
    ra1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_DH    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_DH    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_DH   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_DH   : IN STD_LOGIC_VECTOR(0 TO 15);    
    RegWr_ins1_DH  : IN BIT;
    MemRd_ins1_DH  : IN BIT;
    MemWr_ins1_DH  : IN BIT;
    
    PC_DH : IN STD_LOGIC_VECTOR(0 TO 31);
    
   
    opcode_0_ISS  : OUT STD_LOGIC_VECTOR(0 TO 8);    
    ra0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_0_ISS    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_0_ISS    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_0_ISS   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_0_ISS   : OUT STD_LOGIC_VECTOR(0 TO 15);
    RegWr_ins0_ISS  : OUT BIT;
    MemRd_ins0_ISS  : OUT BIT;
    MemWr_ins0_ISS  : OUT BIT;
    
    
    opcode_1_ISS  : OUT STD_LOGIC_VECTOR(0 TO 8);
    ra1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_ISS    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_ISS    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_ISS   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_ISS   : OUT STD_LOGIC_VECTOR(0 TO 15);
   
    RegWr_ins1_ISS  : OUT BIT;
    MemRd_ins1_ISS  : OUT BIT;
    MemWr_ins1_ISS  : OUT BIT;
    
    PC_ISS : OUT STD_LOGIC_VECTOR(0 TO 31);
  
    reset : in std_logic;
    firstInstr : IN BIT;
    enable : IN STD_LOGIC;
    clk: IN STD_LOGIC
  );
end component;


--Issue register for depchk_iss stage

  component DepChk_ISSreg IS PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    
    opcode_0, opcode_1 : IN STD_LOGIC_VECTOR(0 TO 8);   
    ra1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_1    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_2    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    RegWr_ins0  : IN BIT;
    MemRd_ins0  : IN BIT;
    MemWr_ins0  : IN BIT;
    
    RegWr_ins1  : IN BIT;
    MemRd_ins1  : IN BIT;
    MemWr_ins1  : IN BIT;
    
    IMM_SEL1  : in BIT_VECTOR (0 to 1);
    IMM_SEL2  : in BIT_VECTOR (0 to 1);
    
    PC : IN STD_LOGIC_VECTOR(0 TO 31);
    
    opcode_0_PR5, opcode_1_PR5 : OUT STD_LOGIC_VECTOR(0 TO 8);    
    ra1_addr_PR5  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_PR5  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_PR5  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_PR5  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_PR5    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_PR5    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_PR5   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_PR5   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr_PR5  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr_PR5  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr_PR5  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr_PR5  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_2_PR5    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2_PR5    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2_PR5   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2_PR5   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    RegWr_ins0_PR5  : OUT BIT;
    MemRd_ins0_PR5  : OUT BIT;
    MemWr_ins0_PR5  : OUT BIT;
    
    RegWr_ins1_PR5  : OUT BIT;
    MemRd_ins1_PR5  : OUT BIT;
    MemWr_ins1_PR5  : OUT BIT;
    
    IMM_SEL1_PR5  : OUT BIT_VECTOR (0 to 1);
    IMM_SEL2_PR5  : OUT BIT_VECTOR (0 to 1);
    
    PC_PR5 : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    reset : IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
END component;

-- the register file

component rf is

port(
     ra1_addr  : in std_logic_vector(0 to 6);
     rb1_addr  : in std_logic_vector(0 to 6);
     rc1_addr  : in std_logic_vector(0 to 6);
     rt1_addr  : in std_logic_vector(0 to 6);
     
     ra2_addr  : in std_logic_vector(0 to 6);
     rb2_addr  : in std_logic_vector(0 to 6);
     rc2_addr  : in std_logic_vector(0 to 6);
     rt2_addr  : in std_logic_vector(0 to 6);
     
     ra1_data : out std_logic_vector(0 to 127);-- the six read ports
     rb1_data : out std_logic_vector(0 to 127);--
     rc1_data : out std_logic_vector(0 to 127);--
     
     ra2_data : out std_logic_vector(0 to 127);--
     rb2_data : out std_logic_vector(0 to 127);--
     rc2_data : out std_logic_vector(0 to 127);--
     
     rt1_data : in std_logic_vector(0 to 127);-- the two write ports
     rt2_data : in std_logic_vector(0 to 127);--
     
     clk : in std_logic;
     
     rt1_enable : in bit;
     rt2_enable : in bit
          
     );
End component;

-- multiplexer to input rc data and immediate values

-- take from decode register as of now or change later
component dec_mux IS
  PORT ( 
    mux_in1 : IN STD_LOGIC_VECTOR(0 TO 127);
    mux_in2 : IN STD_LOGIC_VECTOR(0 TO 9);
    mux_in3 : IN STD_LOGIC_VECTOR(0 TO 15);
    mux_out : OUT STD_LOGIC_VECTOR(0 TO 127);
    sel: IN BIT_vector(0 to 1);
    clk: IN STD_LOGIC
    );
END component;

-- RF-EX pipeline register

component RF_EX IS
  PORT(
    ra1, rb1, rc1, rt1 : IN STD_LOGIC_VECTOR(0 TO 127);
    imm7_1    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    ra2, rb2, rc2, rt2 : IN STD_LOGIC_VECTOR(0 TO 127);
    imm7_2    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    opcode_0 : IN STD_LOGIC_VECTOR(0 TO 8);
    opcode_1 : IN STD_LOGIC_VECTOR(0 TO 8);
    
    RegWr_ins0  : IN BIT;
    MemRd_ins0  : IN BIT;
    MemWr_ins0  : IN BIT;
    
    RegWr_ins1  : IN BIT;
    MemRd_ins1  : IN BIT;
    MemWr_ins1  : IN BIT;
    
    IMM_SEL1  : in BIT_vector( 0 to 1 );
    IMM_SEL2  : in BIT_vector( 0 to 1 );
    
    PC : IN STD_LOGIC_VECTOR(0 TO 31);
    
    ra1_PR3, rb1_PR3, rc1_PR3, rt1_PR3 : OUT STD_LOGIC_VECTOR(0 TO 127);
    imm7_1_PR3    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_PR3    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_PR3   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_PR3   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_PR3, rb2_PR3, rc2_PR3, rt2_PR3 : OUT STD_LOGIC_VECTOR(0 TO 127);
    imm7_2_PR3    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2_PR3    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2_PR3   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2_PR3   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    opcode_0_PR3 : OUT STD_LOGIC_VECTOR(0 TO 8);
    opcode_1_PR3 : OUT STD_LOGIC_VECTOR(0 TO 8);
          
    RegWr_ins0_PR3  : OUT BIT;
    MemRd_ins0_PR3  : OUT BIT;
    MemWr_ins0_PR3  : OUT BIT;
    
    RegWr_ins1_PR3  : OUT BIT;
    MemRd_ins1_PR3  : OUT BIT;
    MemWr_ins1_PR3  : OUT BIT;
    
    IMM_SEL1_PR3  : OUT BIT_vector ( 0 to 1);
    IMM_SEL2_PR3  : OUT BIT_vector ( 0 to 1);
    
    PC_PR3 : OUT STD_LOGIC_VECTOR(0 TO 31);    
    
    reset : IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
END component;

-- forwarding multiplexers
--
component frwd_mux is 
 port(
		fwe1      	: in	std_logic_vector ( 0 to 127);
		fwe2	      : in	std_logic_vector ( 0 to 127);
		fwe3      	: in	std_logic_vector ( 0 to 127);
		fwo1      	: in	std_logic_vector ( 0 to 127);
		fwo2      	: in	std_logic_vector ( 0 to 127);
		fwo3      	: in	std_logic_vector ( 0 to 127);
		rf	        : in	std_logic_vector ( 0 to 127);
		frwd_cntrl	: in	bit_vector ( 0 to 2);
		regout     : out	std_logic_vector ( 0 to 127)
	);
end component;

-- even pipe
--
component evenpipe is  
PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    sel : IN STD_ULOGIC;
    opcode : IN STD_LOGIC_VECTOR(0 TO 7);
    RegWr_ins0_ep  : IN BIT;
    MemRd_ins0_ep  : IN BIT;
    MemWr_ins0_ep  : IN BIT;
    PC_ep : IN STD_LOGIC_VECTOR(0 TO 31);
    RegWr_ins0_ep_o  : OUT BIT;
    MemRd_ins0_ep_o  : OUT BIT;
    MemWr_ins0_ep_o  : OUT BIT;
    PC_ep_o : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_data1 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data2 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data3 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    clk: IN STD_ULOGIC);
end component;

-- odd pipe
component oddpipe is
PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 7);
    sel : IN STD_ULOGIC;
    RegWr_ins1_ep  : IN BIT;
    MemRd_ins1_ep  : IN BIT;
    MemWr_ins1_ep  : IN BIT;
    PC_ep : IN STD_LOGIC_VECTOR(0 TO 31);
    RegWr_ins1_ep_o  : OUT BIT;
    MemRd_ins1_ep_o  : OUT BIT;
    MemWr_ins1_ep_o  : OUT BIT;
    flush : OUT BIT;
    load_LS : OUT BIT;
    pc_write : OUT BIT;
    offset : OUT STD_LOGIC_VECTOR(0 TO 4);
    PC_ep_o : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_address1 : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_address2 : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_data : OUT STD_LOGIC_VECTOR ( 0 to 127);
    clk: IN STD_ULOGIC);
  end component;
--  

-- forwarding unit even
component FWEunit is 
PORT ( 
    fpu_result: IN STD_LOGIC_VECTOR(0 TO 127);
    fxu_result: IN STD_LOGIC_VECTOR(0 TO 127);
    byte_result: IN STD_LOGIC_VECTOR(0 TO 127);
    reset : IN STD_LOGIC;
    fwe1_enable : IN STD_LOGIC;
    fwe2_enable : IN STD_LOGIC;
    fwe3_enable : IN STD_LOGIC;
    fwe1_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe2_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe3_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe_out_bus : OUT STD_LOGIC_VECTOR(0 TO 127);    
    clk: IN STD_LOGIC);
END component;

-- forwarding unit odd
component FWOunit is 
PORT ( 
    lsu_result: IN STD_LOGIC_VECTOR(0 TO 31);
    bru_result: IN STD_LOGIC_VECTOR(0 TO 31);
    perm_result: IN STD_LOGIC_VECTOR(0 TO 127);
    reset : IN STD_LOGIC;
    fwo1_enable : IN STD_LOGIC;
    fwo2_enable : IN STD_LOGIC;
    fwo3_enable : IN STD_LOGIC;
    fwo1_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo2_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo3_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo_out_bus : OUT STD_LOGIC_VECTOR(0 TO 127); 
    clk: IN STD_LOGIC);
END component;

-- EX_LSxs stage pipeline register
  component EX_LSxs IS
  PORT(
    result1 : IN STD_LOGIC_VECTOR(0 TO 127); -- even pipe
    result2 : IN STD_LOGIC_VECTOR(0 TO 127); -- odd pipe
    
    rt1 : IN STD_LOGIC_VECTOR(0 TO 6); -- even pipe rt address to write to the register
    rt2 : IN STD_LOGIC_VECTOR(0 TO 6); -- odd pipe rt address to write to the register
    
    w_rt1 : IN STD_LOGIC; -- write signal to register file for rt1
    w_rt2 : IN STD_LOGIC; -- write signal to register file for rt2
 
 ----------------------------------
 
    MemRd1 : IN BIT; --only from odd pipe
    MemWr1 : IN BIT; --only from odd pipe

 ----------------------------------   
    RegWrite1 : IN BIT;
    RegWrite2 : IN BIT;
   
    PC : IN STD_LOGIC_VECTOR(0 TO 31); 
    
    result1_PR4 : OUT STD_LOGIC_VECTOR(0 TO 127); 
    result2_PR4 : OUT STD_LOGIC_VECTOR(0 TO 127); 
    rt1_PR4 : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_PR4 : OUT STD_LOGIC_VECTOR(0 TO 6);
    w_rt1_PR4  : OUT STD_LOGIC;
    w_rt2_PR4  : OUT STD_LOGIC;
    wr1_ls_PR4 : OUT STD_LOGIC;-- output register derived from Memrd/Memwr coming from previous pipeline stages
    enable_ls_PR4 :  OUT STD_LOGIC;
    RegWrite1_PR4 : OUT BIT;
    RegWrite2_PR4 : OUT BIT;
    PC_PR4 : OUT STD_LOGIC_VECTOR(0 TO 31); 
    
    reset : IN STD_LOGIC;
    clk: IN STD_LOGIC);
END component;

-- Local Store Unit-------------
component localstore is
	PORT
	(	clk			: IN  std_logic;
		datain			: IN  std_logic_vector(0 TO 127);
		rw_address	: IN  std_logic_vector( 0 TO 31);
		enable : IN std_logic;
  		wr1_e	: IN  std_logic;
		wr2_e : IN std_logic;
		dataout1		: OUT std_logic_vector(0 TO 127); -- LSU access
		dataout2  : OUT std_logic_vector(0 TO 1023) -- ILB load
	);
end component;
---------------------------------


--LSxs_WB  stage-----------------    
    
component LSxs_WB IS
  PORT(
    result1, rt1 : IN STD_LOGIC_VECTOR(0 TO 127); 
    result2, rt2 : IN STD_LOGIC_VECTOR(0 TO 127);
    
    w_rt1 : IN STD_ULOGIC;
    w_rt2 : IN STD_ULOGIC;
    
    PC : IN STD_LOGIC_VECTOR(0 TO 31);
	  PC_wr : IN STD_ULOGIC; 
    
    result1_PR7, rt1_PR7 : OUT STD_LOGIC_VECTOR(0 TO 127);
    result2_PR7, rt2_PR7 : OUT STD_LOGIC_VECTOR(0 TO 127);
    
    w_rt1_PR7 : OUT STD_ULOGIC;
    w_rt2_PR7 : OUT STD_ULOGIC;
	  PC_wr_PR7: OUT STD_ULOGIC;
    
    PC_PR7 : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    reset : IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
END component;
-----------------------------------------
 
  -- Instantiations go here
    
begin 
  
  -- map signals to ports
  
  ins_in_ilbs <= ins_stream;
  in_enable_ilbs <= in_enable;
  out_enable_ilbs <= delayILB_dec;
  clk_SPU <= clk;
  
  --Port map ILB   
  
ILBmap : ILB port map(
    	     clk => clk_SPU,
  	       ins_in => dataout2_s12,--ins_in_ilbs,   -- to be decided later. We pull up this signal in tb for a pulse duration.
          in_enable => in_enable_ilbs,
          offset => offset_ilbs,
          flush => flush_ilbs,
          pc_write => pc_write_ilbs,
          out_enable => out_enable_ilbs,
          ins_out => ins_out_ilbs
        );
        
decodermap :  decoder3  PORT map(
    inst_0 => ins_out_ilbs (0 to 31), 
    inst_1 => ins_out_ilbs (32 to 63), 
    opcode_0 => opcode_0_dec, 
    opcode_1 => opcode_1_dec,
    ra1_addr => ra1_addr_dec,
    rb1_addr => rb1_addr_dec,
    rc1_addr => rc1_addr_dec,
    rt1_addr => rt1_addr_dec,
    imm7_1 => imm7_1_dec,
    imm8_1 => imm8_1_dec,
    imm10_1 => imm10_1_dec,
    imm16_1 => imm16_1_dec,
    
    ra2_addr => ra2_addr_dec,
    rb2_addr => rb2_addr_dec,
    rc2_addr => rc2_addr_dec,
    rt2_addr => rt2_addr_dec,
    imm7_2 => imm7_2_dec,
    imm8_2 => imm8_2_dec,   
    imm10_2 => imm10_2_dec,
    imm16_2 => imm16_2_dec,
    
    RegWr_ins0 => RegWr_ins0_dec,
    MemRd_ins0 => MemRd_ins0_dec,
    MemWr_ins0 => MemWr_ins0_dec,
    
    RegWr_ins1 => RegWr_ins1_dec,
    MemRd_ins1 => MemRd_ins1_dec,
    MemWr_ins1 => MemWr_ins1_dec,
    
    IMM_SEL1 => IMM_SEL1_dec,
    IMM_SEL2 => IMM_SEL2_dec,

    i_inst => o_inst_dec,   
    o_inst => i_inst_dec,
                                            
    i_inst_sel => o_inst_sel_dec,
    o_inst_sel => i_inst_sel_dec,

    
    delayILB => delayILB_dec,
    enable => enable_decunit_dec,
    clk => clk_SPU
    );
    
ID_Depchkregmap : ID_DepChk port map (
    inst_0 => inst_0_dec,
    inst_1 => inst_1_dec,
    opcode_0 => opcode_0_dec,
    opcode_1 => opcode_1_dec,
    ra1_addr => ra1_addr_dec,
    rb1_addr => rb1_addr_dec,
    rc1_addr => rc1_addr_dec,
    rt1_addr => rt1_addr_dec, 
    imm7_1 => imm7_1_dec,   
    imm8_1 => imm8_1_dec,  
    imm10_1 => imm10_1_dec,  
    imm16_1 => imm16_1_dec,
    
    ra2_addr => ra2_addr_dec, 
    rb2_addr => rb2_addr_dec, 
    rc2_addr => rc2_addr_dec,
    rt2_addr => rt2_addr_dec,
    imm7_2   => imm7_2_dec, 
    imm8_2   => imm8_2_dec,
    imm10_2  => imm10_2_dec,
    imm16_2  => imm16_2_dec,
    
    RegWr_ins0  => RegWr_ins0_dec,
    MemRd_ins0  => MemRd_ins0_dec,
    MemWr_ins0  => MemWr_ins0_dec,
    
    RegWr_ins1 => RegWr_ins1_dec,
    MemRd_ins1 => MemRd_ins1_dec,
    MemWr_ins1 => MemWr_ins1_dec,
    
    IMM_SEL1 => IMM_SEL1_dec,
    IMM_SEL2 => IMM_SEL2_dec,
    
    PC => PC_s1, 
    
    opcode_0_PR1 => opcode_0_PR1_s1,
    opcode_1_PR1 => opcode_1_PR1_s1,   
    ra1_addr_PR1 => ra1_addr_PR1_s1,
    rb1_addr_PR1 => rb1_addr_PR1_s1,
    rc1_addr_PR1 => rc1_addr_PR1_s1,
    rt1_addr_PR1 => rt1_addr_PR1_s1,
    imm7_1_PR1   => imm7_1_PR1_s1,
    imm8_1_PR1   => imm8_1_PR1_s1,
    imm10_1_PR1  => imm10_1_PR1_s1,
    imm16_1_PR1  => imm16_1_PR1_s1,
    ra2_addr_PR1 => ra2_addr_PR1_s1,
    rb2_addr_PR1 => rb2_addr_PR1_s1,
    rc2_addr_PR1 => rc2_addr_PR1_s1,
    rt2_addr_PR1 => rt2_addr_PR1_s1,
    imm7_2_PR1   => imm7_2_PR1_s1,
    imm8_2_PR1   => imm8_2_PR1_s1,
    imm10_2_PR1  => imm10_2_PR1_s1,
    imm16_2_PR1  => imm16_2_PR1_s1,
    
    RegWr_ins0_PR1 => RegWr_ins0_PR1_s1,
    MemRd_ins0_PR1 => MemRd_ins0_PR1_s1,
    MemWr_ins0_PR1 => MemWr_ins0_PR1_s1,
    
    RegWr_ins1_PR1 => RegWr_ins1_PR1_s1,
    MemRd_ins1_PR1 => MemRd_ins1_PR1_s1,
    MemWr_ins1_PR1 => MemWr_ins1_PR1_s1,
    
    IMM_SEL1_PR1  => IMM_SEL1_PR1_s1,
    IMM_SEL2_PR1  => IMM_SEL2_PR1_s1,
    
    PC_PR1 => PC_PR1_s1,
    
    reset => reset_s1,
    clk   => clk_SPU
    );
    
    
--the depchk stage


dh2portmap: DH2 port map (
    opcode_0_DH  => opcode_0_PR1_s1,
    ra0_addr_DH  => ra1_addr_PR1_s1,
    rb0_addr_DH  => rb1_addr_PR1_s1,
    rc0_addr_DH  => rc1_addr_PR1_s1,
    rt0_addr_DH  => rt1_addr_PR1_s1,
    imm7_0_DH    => imm7_1_PR1_s1,
    imm8_0_DH    => imm8_1_PR1_s1,
    imm10_0_DH   => imm10_1_PR1_s1,
    imm16_0_DH   => imm16_1_PR1_s1,
    RegWr_ins0_DH  => RegWr_ins0_PR1_s1,
    MemRd_ins0_DH  => MemRd_ins0_PR1_s1,
    MemWr_ins0_DH  => MemWr_ins0_PR1_s1,
    
    opcode_1_DH  => opcode_1_PR1_s1,
    ra1_addr_DH  => ra2_addr_PR1_s1,
    rb1_addr_DH  => rb2_addr_PR1_s1,
    rc1_addr_DH  => rc2_addr_PR1_s1,
    rt1_addr_DH  => rt2_addr_PR1_s1,
    imm7_1_DH    => imm7_2_PR1_s1,
    imm8_1_DH    => imm8_2_PR1_s1,
    imm10_1_DH   => imm10_2_PR1_s1,
    imm16_1_DH   => imm16_2_PR1_s1,
    RegWr_ins1_DH  => RegWr_ins1_PR1_s1,
    MemRd_ins1_DH  => MemRd_ins1_PR1_s1,
    MemWr_ins1_DH  => MemWr_ins1_PR1_s1,
    
    PC_DH => PC_PR1_s1,
    
   
    opcode_0_ISS  =>  opcode_0_ISS_s2,
    ra0_addr_ISS  => ra0_addr_ISS_s2,
    rb0_addr_ISS  => rb0_addr_ISS_s2,
    rc0_addr_ISS  => rc0_addr_ISS_s2,
    rt0_addr_ISS  => rt0_addr_ISS_s2,
    imm7_0_ISS    => imm7_0_ISS_s2,
    imm8_0_ISS    => imm8_0_ISS_s2,
    imm10_0_ISS   => imm10_0_ISS_s2,
    imm16_0_ISS   => imm16_0_ISS_s2,
    RegWr_ins0_ISS  => RegWr_ins0_ISS_s2,
    MemRd_ins0_ISS  => MemRd_ins0_ISS_s2,
    MemWr_ins0_ISS  => MemWr_ins0_ISS_s2,
        
    opcode_1_ISS  => opcode_1_ISS_s2,
    ra1_addr_ISS  => ra1_addr_ISS_s2,
    rb1_addr_ISS  => rb1_addr_ISS_s2,
    rc1_addr_ISS  => rc1_addr_ISS_s2,
    rt1_addr_ISS  => rt1_addr_ISS_s2,
    imm7_1_ISS    => imm7_1_ISS_s2,
    imm8_1_ISS    => imm8_1_ISS_s2,
    imm10_1_ISS   => imm10_1_ISS_s2,
    imm16_1_ISS   => imm16_1_ISS_s2,
   
    RegWr_ins1_ISS  => RegWr_ins1_ISS_s2,
    MemRd_ins1_ISS  => MemRd_ins1_ISS_s2,
    MemWr_ins1_ISS  => MemWr_ins1_ISS_s2,
    
    PC_ISS => PC_ISS_s2,
    reset => reset_s2,
    firstInstr => firstInstr_s2,
    enable => enable_s2,
    clk => clk_SPU
  );


-- the 

Depchkregmap: DepChk_ISSreg PORT MAP(
    inst_0 => inst_0_s25,
    inst_1 => inst_1_s25,
    opcode_0 => opcode_0_ISS_s2,
    opcode_1 => opcode_1_ISS_s2,
    ra1_addr  => ra0_addr_ISS_s2,
    rb1_addr  => rb0_addr_ISS_s2,
    rc1_addr  => rc0_addr_ISS_s2,
    rt1_addr  => rt0_addr_ISS_s2,
    imm7_1    => imm7_0_ISS_s2,
    imm8_1    => imm8_0_ISS_s2,
    imm10_1   => imm10_0_ISS_s2,
    imm16_1   => imm16_0_ISS_s2,
    ra2_addr  => ra1_addr_ISS_s2,
    rb2_addr  => rb1_addr_ISS_s2,
    rc2_addr  => rc1_addr_ISS_s2,
    rt2_addr  => rt1_addr_ISS_s2,
    imm7_2    => imm7_1_ISS_s2,
    imm8_2    => imm8_1_ISS_s2,
    imm10_2   => imm10_1_ISS_s2,
    imm16_2   => imm16_1_ISS_s2,
    RegWr_ins0 => RegWr_ins0_ISS_s2,
    MemRd_ins0 => MemRd_ins0_ISS_s2,
    MemWr_ins0 => MemWr_ins0_ISS_s2,
    RegWr_ins1 => RegWr_ins1_ISS_s2,
    MemRd_ins1 => MemRd_ins1_ISS_s2,
    MemWr_ins1 => MemWr_ins1_ISS_s2,
    IMM_SEL1  =>   IMM_SEL1_PR1_s1,
    IMM_SEL2 =>  IMM_SEL2_PR1_s1,
    PC => PC_ISS_s2,
    opcode_0_PR5 => opcode_0_PR5_s25,
    opcode_1_PR5 => opcode_1_PR5_s25,
    ra1_addr_PR5 => ra1_addr_PR5_s25,
    rb1_addr_PR5 => rb1_addr_PR5_s25,
    rc1_addr_PR5 => rc1_addr_PR5_s25,
    rt1_addr_PR5 => rt1_addr_PR5_s25,
    imm7_1_PR5   => imm7_1_PR5_s25,
    imm8_1_PR5   => imm8_1_PR5_s25,
    imm10_1_PR5  => imm10_1_PR5_s25,
    imm16_1_PR5  => imm16_1_PR5_s25,
    ra2_addr_PR5 => ra2_addr_PR5_s25,
    rb2_addr_PR5 => rb2_addr_PR5_s25,
    rc2_addr_PR5 => rc2_addr_PR5_s25,
    rt2_addr_PR5 => rt2_addr_PR5_s25,
    imm7_2_PR5   => imm7_2_PR5_s25,
    imm8_2_PR5   => imm8_2_PR5_s25,
    imm10_2_PR5  => imm10_2_PR5_s25,
    imm16_2_PR5  => imm16_2_PR5_s25,
    RegWr_ins0_PR5 => RegWr_ins0_PR5_s25,
    MemRd_ins0_PR5 => MemRd_ins0_PR5_s25,
    MemWr_ins0_PR5 => MemWr_ins0_PR5_s25,
    RegWr_ins1_PR5 => RegWr_ins1_PR5_s25,
    MemRd_ins1_PR5 => MemRd_ins1_PR5_s25,
    MemWr_ins1_PR5 => MemWr_ins1_PR5_s25,
    IMM_SEL1_PR5 => IMM_SEL1_PR5_s25,
    IMM_SEL2_PR5 => IMM_SEL2_PR5_s25,
    PC_PR5 => PC_PR5_s25,
    reset => reset_s25,
    clk => clk_SPU);
  


-- the depchk_rf reg

--
registerfilemap : rf port map (
 ra1_addr => ra1_addr_PR5_s25,
 rb1_addr => rb1_addr_PR5_s25,
 rc1_addr => rc1_addr_PR5_s25,
 rt1_addr => rt1_addr_PR5_s25,
          
 ra2_addr => ra2_addr_PR5_s25,
 rb2_addr => rb2_addr_PR5_s25,
 rc2_addr => rc2_addr_PR5_s25,
 rt2_addr => rt2_addr_PR5_s25,
          
 ra1_data => ra1_data_s3,
 rb1_data => rb1_data_s3,
 rc1_data => rc1_data_s3,
          
 ra2_data => ra2_data_s3,
 rb2_data => rb2_data_s3,
 rc2_data => rc2_data_s3,
          
 rt1_data => rt1_data_s3,
 rt2_data => rt2_data_s3,
          
 rt1_enable => RegWrite1_PR4_s11,
 rt2_enable => RegWrite2_PR4_s11,
 clk => clk_SPU
 );
-- map the multiplexers for immediate values
rc1muxmap: dec_mux PORT map( 
    mux_in1 => rc1_data_s3,
    mux_in2 => imm10_1_PR5_s25, 
    mux_in3 => imm16_1_PR5_s25,
    mux_out => mux_out1, 
    sel => IMM_SEL1_PR5_s25,
    clk => clk_SPU
    );


rc2muxmap: dec_mux PORT map( 
    mux_in1 => rc2_data_s3,
    mux_in2 ( 0 to 9 ) => imm10_2_PR5_s25 ( 0 to 9 ), 
    mux_in3 ( 0 to 15) => imm16_2_PR5_s25 ( 0 to 15),
    mux_out => mux_out2, 
    sel => IMM_SEL2_PR5_s25,
    clk => clk_SPU
    );

--

RF_EXmap : RF_EX port map (
  ra1 => ra1_data_s3,
  rb1 => rb1_data_s3,
  rc1 => mux_out1,--rc1_data_s3, -- now input is from mux
  rt1 => rt1_data_s3,
  imm7_1 => rc1_data_s3( 0 to 6),
  imm8_1 => rc1_data_s3( 0 to 7),
  imm10_1 => rc1_data_s3( 0 to 9),
  imm16_1 => rc1_data_s3( 0 to 15),
  ra2 => ra2_data_s3,
  rb2 => rb2_data_s3,
  rc2 => mux_out2,--rc2_data_s3,
  rt2 => rt2_data_s3,
  imm7_2 => rc2_data_s3( 0 to 6),
  imm8_2 => rc2_data_s3( 0 to 7),
  imm10_2 => rc2_data_s3( 0 to 9),
  imm16_2 => rc2_data_s3( 0 to 15),
  opcode_0 =>opcode_0_PR5_s25, -- comes from decoder output as of now
  opcode_1 =>opcode_1_PR5_s25,
  
  RegWr_ins0  => RegWr_ins0_PR5_s25,
  MemRd_ins0  => MemRd_ins0_PR5_s25,
  MemWr_ins0  => MemWr_ins0_PR5_s25,
    
  RegWr_ins1 => RegWr_ins1_PR5_s25,
  MemRd_ins1 => MemRd_ins1_PR5_s25,
  MemWr_ins1 => MemWr_ins1_PR5_s25,
    
  IMM_SEL1 => IMM_SEL1_PR5_s25,
  IMM_SEL2 => IMM_SEL2_PR5_s25,
  
  PC => PC_PR5_s25,            -- no source in last stage of decoder. needs an additional 1 cycle delay
  
  ra1_PR3 => ra1_PR3_s4,
  rb1_PR3 => rb1_PR3_s4,
  rc1_PR3 => rc1_PR3_s4,
  rt1_PR3 => rt1_PR3_s4,
  imm7_1_PR3 => imm7_1_PR3_s4,
  imm8_1_PR3 => imm8_1_PR3_s4,  
  imm10_1_PR3 => imm10_1_PR3_s4,  
  imm16_1_PR3 => imm16_1_PR3_s4,
  ra2_PR3 => ra2_PR3_s4,
  rb2_PR3 => rb2_PR3_s4,
  rc2_PR3 => rc2_PR3_s4,
  rt2_PR3 => rt2_PR3_s4,
  imm7_2_PR3 => imm7_2_PR3_s4, 
  imm8_2_PR3 => imm8_2_PR3_s4, 
  imm10_2_PR3 => imm10_2_PR3_s4,
  imm16_2_PR3 => imm16_2_PR3_s4, 
  
  opcode_0_PR3=> opcode_0_PR3_s4,
  opcode_1_PR3 => opcode_1_PR3_s4,
  
  RegWr_ins0_PR3  => RegWr_ins0_PR3_s4, 
  MemRd_ins0_PR3  => MemRd_ins0_PR3_s4, 
  MemWr_ins0_PR3  => MemWr_ins0_PR3_s4, 
                               
  RegWr_ins1_PR3 =>  RegWr_ins1_PR3_s4,  
  MemRd_ins1_PR3 =>  MemRd_ins1_PR3_s4,  
  MemWr_ins1_PR3 =>  MemWr_ins1_PR3_s4,  
                               
  IMM_SEL1_PR3 =>    IMM_SEL1_PR3_s4,      
  IMM_SEL2_PR3 =>    IMM_SEL2_PR3_s4,      

  
  PC_PR3 => PC_PR3_s4,
    
  reset => reset_s4, 
  clk => clk_SPU

);

-- map the forwarding muxes to the input ports in the pipeline
  
--
---- forwarding in even pipe
frwdmux_ra1 : frwd_mux port map(
		fwe1  => fwe1_s5,
		fwe2	 => fwe2_s5, 
		fwe3  =>	fwe3_s5,
		fwo1 	=> fwo1_s5,
		fwo2  => fwo2_s5,
		fwo3  => fwo3_s5,
		rf	   => ra1_PR3_s4,    
		frwd_cntrl	=> frwd_cntrl_s5_a1,
		regout => regout_s5_ra1
	);
--
frwdmux_rb1 : frwd_mux  port map(
		fwe1  => fwe1_s5,
		fwe2	 => fwe2_s5, 
		fwe3  =>	fwe3_s5,
		fwo1 	=> fwo1_s5,
		fwo2  => fwo2_s5,
		fwo3  => fwo3_s5,
		rf	   => rb1_PR3_s4,    
		frwd_cntrl	=> frwd_cntrl_s5_b1,
		regout => regout_s5_rb1
	);

frwdmux_rc1 : frwd_mux  port map(
		fwe1  => fwe1_s5,
		fwe2	 => fwe2_s5, 
		fwe3  =>	fwe3_s5,
		fwo1 	=> fwo1_s5,
		fwo2  => fwo2_s5,
		fwo3  => fwo3_s5,
		rf	   => rc1_PR3_s4,    
		frwd_cntrl	=> frwd_cntrl_s5_c1,
		regout => regout_s5_rc1
	);--
--  
---- forwarding in odd pipe
frwdmux_ra2 : frwd_mux port map(
		fwe1  => fwe1_s5,
		fwe2	 => fwe2_s5, 
		fwe3  =>	fwe3_s5,
		fwo1 	=> fwo1_s5,
		fwo2  => fwo2_s5,
		fwo3  => fwo3_s5,
		rf	   => ra2_PR3_s4,    
		frwd_cntrl	=> frwd_cntrl_s5_a2,
		regout => regout_s5_ra2
	);
--
frwdmux_rb2 : frwd_mux  port map(
		fwe1  => fwe1_s5,
		fwe2	 => fwe2_s5, 
		fwe3  =>	fwe3_s5,
		fwo1 	=> fwo1_s5,
		fwo2  => fwo2_s5,
		fwo3  => fwo3_s5,
		rf	   => rb2_PR3_s4,    
		frwd_cntrl	=> frwd_cntrl_s5_b2,
		regout => regout_s5_rb2
	);
--
frwdmux_rc2 : frwd_mux  port map(
		fwe1  => fwe1_s5,
		fwe2	 => fwe2_s5, 
		fwe3  =>	fwe3_s5,
		fwo1 	=> fwo1_s5,
		fwo2  => fwo2_s5,
		fwo3  => fwo3_s5,
		rf	   => rc2_PR3_s4,    
		frwd_cntrl	=> frwd_cntrl_s5_c2,
		regout => regout_s5_rc2
	);
	--
evenpipemap: evenpipe PORT map(
    ra => regout_s5_ra1,
    rb => regout_s5_rb1,
    rc => regout_s5_rc1,
    rt => rt1_PR3_s4,
    sel => opcode_0_PR3_s4(8),
    opcode => opcode_0_PR3_s4(0 to 7),
    RegWr_ins0_ep =>   RegWr_ins0_PR3_s4,  
    MemRd_ins0_ep =>   MemRd_ins0_PR3_s4, 
    MemWr_ins0_ep =>   MemWr_ins0_PR3_s4,
    PC_ep => PC_ep_s7,
    RegWr_ins0_ep_o => RegWr_ins0_ep_o_s7,
    MemRd_ins0_ep_o => MemRd_ins0_ep_o_s7,
    MemWr_ins0_ep_o => MemWr_ins0_ep_o_s7,
    PC_ep_o => PC_ep_o_s7,
    result_data1 => result_data1_s7,
    result_data2 => result_data2_s7,
    result_data3 => result_data3_s7,
    clk => clk_SPU
    );
--	
--	   
      
     
oddpipemap: oddpipe PORT map(
    ra => regout_s5_ra2,
    rb => regout_s5_rb2,
    rc => regout_s5_rc2,
    rt => rt2_PR3_s4,
    sel => opcode_1_PR3_s4(8),
    opcode => opcode_1_PR3_s4(0 to 7),
    RegWr_ins1_ep =>  RegWr_ins1_PR3_s4,
    MemRd_ins1_ep =>  MemRd_ins1_PR3_s4,
    MemWr_ins1_ep =>  MemWr_ins1_PR3_s4,
    PC_ep => PC_ep_s7,
    RegWr_ins1_ep_o => RegWr_ins1_ep_o_s8,
    MemRd_ins1_ep_o => MemRd_ins1_ep_o_s8,
    MemWr_ins1_ep_o => MemWr_ins1_ep_o_s8,
    PC_ep_o =>     PC_ep_o_s7,
    flush => flush_s8,
    load_LS => load_LS_s8,
    pc_write => pc_write_s8,
    offset => offset_s8,
    result_address1 => result_address1_s8,
    result_address2 => result_address2_s8,
    result_data => result_data_s8,
    clk => clk_SPU
    );
    --
frwd_even_map : FWEunit PORT map( 
    fpu_result => result_data2_s7,
    fxu_result => result_data1_s7,
    byte_result => result_data3_s7,
    reset => reset_s9,
    fwe1_enable => fwe1_enable_s9,
    fwe2_enable => fwe2_enable_s9,
    fwe3_enable => fwe3_enable_s9,
    fwe1_dataout_bus => fwe1_dataout_bus_s9,
    fwe2_dataout_bus => fwe2_dataout_bus_s9,
    fwe3_dataout_bus => fwe3_dataout_bus_s9, 
    fwe_out_bus => fwe_out_bus_s9,
    clk => clk_SPU
    );
--
frwd_odd_map: FWOunit PORT map( 
    lsu_result => result_address1_s8,
    bru_result => result_address2_s8,
    perm_result => result_data_s8,
    reset => reset_s10,
    fwo1_enable => fwo1_enable_s10,
    fwo2_enable => fwo2_enable_s10,
    fwo3_enable => fwo3_enable_s10,
    fwo1_dataout_bus => fwo1_dataout_bus_s10,
    fwo2_dataout_bus => fwo2_dataout_bus_s10,
    fwo3_dataout_bus => fwo3_dataout_bus_s10,
    fwo_out_bus => fwo_out_bus_s10,
    clk => clk_SPU
    );
-- 
-- -- we need to generate lots of signals to get this moving further
--
    EX_LSxsmap : EX_LSxs PORT map(
    result1 => fwe_out_bus_s9,
    rt1 => rt1_s11,
    result2 => fwo_out_bus_s10,
    rt2 => rt2_s11,
    w_rt1 => w_rt1_s11,  
    w_rt2 => w_rt2_s11,
    MemRd1 => MemRd_ins1_ep_o_s8,
    MemWr1 => MemWr_ins1_ep_o_s8,
    PC => PC_s11,
    RegWrite1 => RegWr_ins0_ep_o_s7,
    RegWrite2 => RegWr_ins1_ep_o_s8,
  
    result1_PR4 => result1_PR4_s11,
    rt1_PR4 => rt1_PR4_s11,
    result2_PR4 => result2_PR4_s11,
    rt2_PR4 => rt2_PR4_s11,
    w_rt1_PR4 => w_rt1_PR4_s11,
    w_rt2_PR4 => w_rt2_PR4_s11,
    enable_ls_PR4 => enable_ls_PR4_s11,
    wr1_ls_PR4 => wr1_ls_PR4_s11,
    PC_PR4 => PC_PR4_s11,
    RegWrite1_PR4 => RegWrite1_PR4_s11,
    RegWrite2_PR4 => RegWrite2_PR4_s11,
  
    reset => reset_s11,
    clk => clk_SPU 
    );
---------------------------------------------------------------------------------------------
---LSA access
Locastoremap: localstore 	PORT MAP(
    clk	=> clk_SPU,
    enable			=> enable_ls_PR4_s11,
		datain	=> datain_s12,
		rw_address	=> result2_PR4_s11 ( 0 to 31),
  		wr1_e	=> wr1_ls_PR4_s11,
		wr2_e => wr2_e_s12,
		dataout1	=> dataout1_s12,
		dataout2 => dataout2_s12
	);
---------------------------------------------------------------------------------------------	
-- LS_WBregister
 LS_WBregmap : LSxs_WB PORT MAP(
    result1 => result1_s13,
    rt1 => rt1_s13,
    result2 => result2_s13,
    rt2 => rt2_s13,
    w_rt1 => w_rt1_s13,
    w_rt2 => w_rt2_s13,
    PC => PC_s13,
	  PC_wr => PC_wr_s13,
    result1_PR7 => result1_PR7_s13,
    rt1_PR7 => rt1_PR7_s13,
    result2_PR7 => result2_PR7_s13,
    rt2_PR7 => rt2_PR7_s13,
    w_rt1_PR7 => w_rt1_PR7_s13,
    w_rt2_PR7 => w_rt2_PR7_s13,
	  PC_wr_PR7 => PC_wr_PR7_s13,
    PC_PR7 => PC_PR7_s13,
    reset => reset_s13,
    clk => clk_SPU
  );
-----------------------------------------------------------------------
process (clk_SPU)
-- variable declarations
 begin   
   -- begin fetching data to ILB
   if clk_SPU'event and  clk_SPU ='1' 
  then 
    wr2_e_s12 <= '0'; ---burst load 32 instructions into ILB for Local Store
    ---------------------------------------
    
    enable_decunit_dec <= '1';
    -- need to set enable for LS high here 
    ---------------------------------------
  --  wr2_e_s12 <= '1' after 50 ns;
    ---------------------------------------
    
    end if; -- clk ends
  end process;
end behave;