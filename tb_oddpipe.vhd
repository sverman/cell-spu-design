LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE IEEE.std_logic_textio.ALL;
USE STD.textio.ALL;

ENTITY tb_oddpipe IS
END tb_oddpipe;

ARCHITECTURE testbench OF tb_oddpipe IS
  
  FILE logger : TEXT OPEN WRITE_MODE IS "transcript_evenpipe.log";
  
  COMPONENT oddpipe 
PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 7);
    sel : IN STD_ULOGIC;
    result_address1 : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_address2 : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_data : OUT STD_LOGIC_VECTOR ( 0 to 127);
    clk: IN STD_ULOGIC);
  END COMPONENT;
  
  SIGNAL ra : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rb : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rc : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rt : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL sel : STD_ULOGIC;
  SIGNAL opcode : STD_LOGIC_VECTOR(0 TO 7);
  SIGNAL result_address1 : STD_LOGIC_VECTOR(0 TO 31);
  SIGNAL result_address2 : STD_LOGIC_VECTOR(0 TO 31);
  SIGNAL result_data : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL clk :  STD_ULOGIC;
  
  PROCEDURE print(opcode : STD_LOGIC_VECTOR(0 TO 7);
                  ra : STD_LOGIC_VECTOR(0 TO 127);
                  rb : STD_LOGIC_VECTOR(0 TO 127);
                  rc : STD_LOGIC_VECTOR(0 TO 127);
                  rt : STD_LOGIC_VECTOR(0 TO 127);                
                  result_address1 : STD_LOGIC_VECTOR(0 TO 31);
                  result_address2 : STD_LOGIC_VECTOR(0 TO 31);
                  result_data : STD_LOGIC_VECTOR(0 TO 127)
                  ) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
    BEGIN
    swrite(line_out, "opcode = ");
    write(line_out, opcode);
    writeline(logger, line_out);
    swrite(line_out, "ra =     ");
    hwrite(line_out, ra);
    writeline(logger, line_out);
    swrite(line_out, "rb =     ");
    hwrite(line_out, rb);
    writeline(logger, line_out);
    swrite(line_out, "rc =     ");
    hwrite(line_out, rc);
    writeline(logger, line_out);
    swrite(line_out, "rt =     ");
    hwrite(line_out, rt);
    writeline(logger, line_out);
    swrite(line_out, "result_address1 = ");
    hwrite(line_out, result_address1);
    writeline(logger, line_out);
    swrite(line_out, "result_address2= ");
    write(line_out, result_address2);
    writeline(logger, line_out);
    swrite(line_out, "result_data= ");
    write(line_out, result_data);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;
  
  -- definition of a clock period
  CONSTANT clk_period : time := 10 ns;
  
BEGIN
  -- component instantiation
  DUT: oddpipe PORT MAP (
    ra => ra,
    rb => rb,
    rc => rc,
    rt => rt,
    clk => clk,
    sel => sel,
    opcode => opcode,
    result_address1 => result_address1,
    result_address2 => result_address2,
    result_data => result_data
    );
    
    CLK_PROCESS : PROCESS
    BEGIN
		  clk <= '1';
		  WAIT FOR clk_period/2;
		  
		  clk <= '0';
		  WAIT FOR clk_period/2;
    END PROCESS;
    
    SIMULATION : PROCESS
    BEGIN
    sel <='1';

  opcode <= "00001010";
  ra(0 TO 127) <= X"0000000F0012334F0004563F0000321F";
  rb(0 TO 127) <= X"0000000F7648000F0002378F0000123F";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result_address1,result_address2,result_data);

  opcode <= "00010010";
  ra(0 TO 127) <= X"0000000F7648000F002378F10864445E";
  rb(0 TO 127) <= X"0000000F7648000F0002378F0000123F";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result_address1,result_address2,result_data);
  
  opcode <= "00010100";
  ra(0 TO 127) <= X"11101021001120113050210430201022";
  rb(0 TO 127) <= X"44101021001120113030210130101022";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result_address1,result_address2,result_data);

  opcode <= "00010001";
  ra(0 TO 127) <= X"11101021001120113050210430201022";
  rb(0 TO 127) <= X"44101021001120113030210130101022";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result_address1,result_address2,result_data);
  
  REPORT "Test Completed";
 
  WAIT;
  
END PROCESS;
END;