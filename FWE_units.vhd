
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

Entity FWEunit is
  
PORT ( 
    fpu_result: IN STD_LOGIC_VECTOR(0 TO 127);
    fxu_result: IN STD_LOGIC_VECTOR(0 TO 127);
    byte_result: IN STD_LOGIC_VECTOR(0 TO 127);
    reset : IN STD_LOGIC;
    fwe1_enable : IN STD_LOGIC;
    fwe2_enable : IN STD_LOGIC;
    fwe3_enable : IN STD_LOGIC;
    fwe1_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe2_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe3_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwe_out_bus : OUT STD_LOGIC_VECTOR(0 TO 127);    
    clk: IN STD_LOGIC);
END FWEunit;

ARCHITECTURE behave OF FWEunit IS
component FWE01 is
  
port(
    
    fwd_datain : IN STD_LOGIC_VECTOR(0 TO 127);
    out_enable : IN STD_LOGIC;
    reset : in STD_LOGIC; 
    fwd_dataout : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwd_regout : out STD_LOGIC_VECTOR (0 to 127);
    clk: IN STD_LOGIC
     
     );
end component;

signal fwe1_output : STD_LOGIC_VECTOR ( 0 to 127);
signal fwe2_output : STD_LOGIC_VECTOR ( 0 to 127);
signal fwe3_output : STD_LOGIC_VECTOR ( 0 to 127);

signal fwe1_reg : STD_LOGIC_VECTOR ( 0 to 127);
signal fwe2_reg : STD_LOGIC_VECTOR ( 0 to 127);
signal fwe3_reg : STD_LOGIC_VECTOR ( 0 to 127);

signal fwe1_input : STD_LOGIC_VECTOR ( 0 to 127);
signal fwe2_input : STD_LOGIC_VECTOR ( 0 to 127);
signal fwe3_input : STD_LOGIC_VECTOR ( 0 to 127);


BEGIN
fwe1_input <= byte_result;
fwe2_input <= fxu_result;
fwe2_input <= fwe1_output;
fwe3_input <= fpu_result;
fwe3_input <= fwe2_output;

fwe_out_bus <= fwe3_output;
 
   fwd1 : FWE01 PORT MAP (
    fwd_datain  => fwe1_input,
    reset => reset,
    out_enable => fwe1_enable,
    fwd_dataout =>  fwe1_dataout_bus,
    fwd_regout => fwe1_output,
    
    clk   => clk
        );
     
   fwd2 : FWE01 PORT MAP (
  fwd_datain  => fwe2_input,
    reset => reset,
    out_enable => fwe2_enable,
    fwd_dataout =>  fwe2_dataout_bus,
    fwd_regout => fwe2_output,
    clk   => clk
         
        );
        
   fwd3 : FWE01 PORT MAP (
   fwd_datain  => fwe3_input,
    reset => reset,
    out_enable => fwe3_enable,
     fwd_dataout =>  fwe3_dataout_bus,
    fwd_regout => fwe3_output,
    clk   => clk
        
        );

end behave;
  