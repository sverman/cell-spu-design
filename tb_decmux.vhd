--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   01:04:09 03/29/2013
-- Design Name:   
-- Module Name:   C:/Users/SAMBHAV/xilinx_work/ILB_SPU/test_ILB.vhd
-- Project Name:  ILB_SPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ILB_SPU
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_decmux IS
END test_decmux;

 ARCHITECTURE behavior OF test_decmux IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT dec_mux PORT 
    (
    mux_in1 : IN STD_LOGIC_VECTOR(0 TO 127);
    mux_in2 : IN STD_LOGIC_VECTOR(0 TO 9);
    mux_in3 : IN STD_LOGIC_VECTOR(0 TO 15);
    mux_out : OUT STD_LOGIC_VECTOR(0 TO 127);
    
    sel : IN BIT_Vector( 0 to 1);
    
    clk: in STD_LOGIC
    );
    END COMPONENT;
    

 	  signal mux_in1	: 	std_logic_vector ( 0 to 127);
 	  signal mux_in2	: 	std_logic_vector ( 0 to 9);
 	  signal mux_in3	: 	std_logic_vector ( 0 to 15);
 	  
 	  signal mux_out : STD_LOGIC_VECTOR(0 TO 127);
    
    signal sel: BIT_vector( 0 to 1);
    
    signal clk : STD_LOGIC;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: dec_mux PORT MAP
    (
 mux_in1 => mux_in1,
 mux_in2 => mux_in2,
 mux_in3 => mux_in3,
 mux_out => mux_out,
 sel => sel,
 clk =>clk
 );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
   sel <="11";
    wait for clk_period/2;
		  sel <= "10";
      mux_in1 <= X"12ff345F6789F10212ff345F6789F102";
           mux_in2 <= "1010101010";
           mux_in3 <= "1010101010101101";
    wait for clk_period;
		  sel <= "00";
		      wait for clk_period;
		  sel <= "11"; 
 
    
      wait;
   end process;

END;
