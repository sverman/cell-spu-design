LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE IEEE.std_logic_textio.ALL;
USE STD.textio.ALL;

ENTITY tb_decoder IS
END tb_decoder;

ARCHITECTURE decoder OF tb_decoder IS
  
  FILE logger : TEXT OPEN WRITE_MODE IS "transcript_decoder.log";

  COMPONENT decoder3 IS
  PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    opcode_0, opcode_1 : OUT STD_LOGIC_VECTOR(0 TO 8);
    
    ra1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_2    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    -- Register R/W & Memory R/W Signals
    RegWr_ins0  : OUT BIT;
    MemRd_ins0  : OUT BIT;
    MemWr_ins0  : OUT BIT;
    
    RegWr_ins1  : OUT BIT;
    MemRd_ins1  : OUT BIT;
    MemWr_ins1  : OUT BIT;
        
    delayILB : OUT STD_ULOGIC;
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC;
    IMM_SEL1  : OUT BIT_VECTOR (0 to 1) ; -- 00 for rc -- 01 immv10 -- 02 immv16
    IMM_SEL2  : OUT BIT_VECTOR  (0 to 1) ;
      
    i_inst : IN STD_LOGIC_VECTOR(0 TO 31);
    o_inst : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    i_inst_sel : IN STD_ULOGIC;
    o_inst_sel : OUT STD_ULOGIC
    );
  END COMPONENT;
  
  SIGNAL inst_0, inst_1 : STD_LOGIC_VECTOR(0 TO 31);
  SIGNAL opcode_0, opcode_1 : STD_LOGIC_VECTOR(0 TO 8);
  SIGNAL ra1_addr  : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL rb1_addr  : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL rc1_addr  : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL rt1_addr  : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL imm7_1    : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL imm8_1    : STD_LOGIC_VECTOR(0 TO 7);
  SIGNAL imm10_1   : STD_LOGIC_VECTOR(0 TO 9);
  SIGNAL imm16_1   : STD_LOGIC_VECTOR(0 TO 15);
  
  SIGNAL ra2_addr  : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL rb2_addr  : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL rc2_addr  : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL rt2_addr  : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL imm7_2    : STD_LOGIC_VECTOR(0 TO 6);
  SIGNAL imm8_2    : STD_LOGIC_VECTOR(0 TO 7);
  SIGNAL imm10_2   : STD_LOGIC_VECTOR(0 TO 9);
  SIGNAL imm16_2   : STD_LOGIC_VECTOR(0 TO 15);
  
  SIGNAL RegWr_ins0  : BIT;
  SIGNAL MemRd_ins0  : BIT;
  SIGNAL MemWr_ins0  : BIT;
    
  SIGNAL RegWr_ins1  : BIT;
  SIGNAL MemRd_ins1  : BIT;
  SIGNAL MemWr_ins1  : BIT;
  SIGNAL IMM_SEL1 :  BIT_vector( 0 TO 1);
  SIGNAL IMM_SEL2 :  BIT_vector( 0 TO 1);
  
  
  SIGNAL enable : STD_ULOGIC;
  SIGNAL clk: STD_ULOGIC;
  
  SIGNAL i_inst : STD_LOGIC_VECTOR(0 TO 31);
  SIGNAL o_inst : STD_LOGIC_VECTOR(0 TO 31);
    
  SIGNAL i_inst_sel : STD_ULOGIC;
  SIGNAL o_inst_sel : STD_ULOGIC;
  
  PROCEDURE print(inst_0 : STD_LOGIC_VECTOR(0 TO 31);
                  inst_1 : STD_LOGIC_VECTOR(0 TO 31);
                  opcode_0 : STD_LOGIC_VECTOR(0 TO 8);
                  opcode_1 : STD_LOGIC_VECTOR(0 TO 8)
                  ) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
  BEGIN
    swrite(line_out, "inst_0 = ");
    write(line_out, inst_0);
    writeline(logger, line_out);
    swrite(line_out, "inst_1 = ");
    write(line_out, inst_1);
    writeline(logger, line_out);
    swrite(line_out, "opcode_0 = ");
    write(line_out, opcode_0);
    writeline(logger, line_out);
    swrite(line_out, "opcode_1 = ");
    write(line_out, opcode_1);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;
  
  -- definition of a clock period
  CONSTANT clk_period : time := 10 ns;
  
BEGIN
  
  -- component instantiation
  DUT: decoder3 PORT MAP (
    inst_0 => inst_0,
    inst_1 => inst_1,
    opcode_0 => opcode_0,
    opcode_1 => opcode_1,
    ra1_addr => ra1_addr,
    rb1_addr => rb1_addr,
    rc1_addr => rc1_addr,
    rt1_addr => rt1_addr,
    imm7_1   => imm7_1,
    imm8_1   => imm8_1,
    imm10_1  => imm10_1,
    imm16_1  => imm16_1,
    ra2_addr => ra2_addr,
    rb2_addr => rb2_addr,
    rc2_addr => rc2_addr,
    rt2_addr => rt2_addr,
    imm7_2   => imm7_2,
    imm8_2   => imm8_2,
    imm10_2  => imm10_2,
    imm16_2  => imm16_2,
    
    RegWr_ins0 => RegWr_ins0,
    
    MemRd_ins0 => MemRd_ins0,
    
    MemWr_ins0 => MemWr_ins0,
    
    RegWr_ins1 => RegWr_ins1,
    
    MemRd_ins1 => MemRd_ins1,
    
    MemWr_ins1 => MemWr_ins1,
    IMM_SEL1 => IMM_SEL1,
    IMM_SEL2 => IMM_SEL2,

    i_inst => i_inst,
    i_inst_sel => i_inst_sel,
    
    o_inst => o_inst,
    o_inst_sel => o_inst_sel,
    
    clk => clk,
    enable => enable);
    
    CLK_PROCESS : PROCESS
    BEGIN
		  clk <= '0';
		  WAIT FOR clk_period/2;
		  
		  clk <= '1';
		  WAIT FOR clk_period/2;
    END PROCESS;
              
    SIMULATION : PROCESS
    BEGIN

    WAIT FOR 10 ns;
    enable <= '1';
--00110110010000100100000100000111
--00110110001000011100000100000011
    
    inst_0 <= "00110110010000100100000100000111";
    inst_1 <= "00110110010000100100000100000111";
    i_inst <= (OTHERS => 'X');
    i_inst_sel <= '0';
    print(inst_0, inst_1, opcode_0, opcode_1);
    WAIT FOR 10 ns;
    
    inst_0 <= "00100011100000000000000110101001";
    inst_1 <= "00011000000000000000110011101111";
    i_inst <= (OTHERS => 'X');
    i_inst_sel <= '0';
    print(inst_0, inst_1, opcode_0, opcode_1);
    WAIT FOR 10 ns;
    
    inst_0 <= "00001101100000000000000110101001";
    inst_1 <= "00011000001000000000000110101001"; --and
    i_inst <= (OTHERS => 'X');
    i_inst_sel <= '0';
    print(inst_0, inst_1, opcode_0, opcode_1);
    WAIT FOR 10 ns;
    
    inst_0 <= (OTHERS => 'X');
    inst_1 <= (OTHERS => 'X');
    i_inst <= "00100011100000000000000110101001";
    i_inst_sel <= '1';
    print(inst_0, inst_1, opcode_0, opcode_1);
    WAIT FOR 10 ns;
    
    inst_0 <= "00011001000000000000010111001110";
    inst_1 <= "00111000100000000001110011010101";
    i_inst <= (OTHERS => 'X');
    i_inst_sel <= '0';
    print(inst_0, inst_1, opcode_0, opcode_1);
    WAIT FOR 10 ns;
    
    REPORT "Test Completed";
    --enable <= '0';
    WAIT;
    
END PROCESS;
END;
    
    