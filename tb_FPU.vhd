library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.textio.all;  
use IEEE.std_logic_textio.all;

Entity test_FPU is
end test_FPU;
architecture test of test_FPU is
FILE logger : TEXT OPEN WRITE_MODE IS "transcript_FPU.log";  
component floatingunit
port(ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_ULOGIC;
    enable: IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
end component;

    signal ra, rb, rc, rt : STD_LOGIC_VECTOR(0 TO 127);
    signal opcode :  STD_LOGIC_VECTOR(0 TO 4);
    signal result :  STD_LOGIC_VECTOR(0 TO 127);
    signal zero :  STD_ULOGIC;
    signal clk:  STD_ULOGIC := '0';
    signal enable:  STD_ULOGIC;
   
    PROCEDURE print(opcode : STD_LOGIC_VECTOR(0 TO 4);
                  ra : STD_LOGIC_VECTOR(0 TO 127);
                  rb : STD_LOGIC_VECTOR(0 TO 127);
                  rc : STD_LOGIC_VECTOR(0 TO 127);
                  rt : STD_LOGIC_VECTOR(0 TO 127);                
                  result : STD_LOGIC_VECTOR(0 TO 127);
                  zero : STD_ULOGIC) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
  BEGIN
    swrite(line_out, "opcode = ");
    write(line_out, opcode);
    writeline(logger, line_out);
    swrite(line_out, "ra =     ");
    hwrite(line_out, ra);
    writeline(logger, line_out);
    swrite(line_out, "rb =     ");
    hwrite(line_out, rb);
    writeline(logger, line_out);
    swrite(line_out, "rc =     ");
    hwrite(line_out, rc);
    writeline(logger, line_out);
    swrite(line_out, "rt =     ");
    hwrite(line_out, rt);
    writeline(logger, line_out);
    swrite(line_out, "result = ");
    hwrite(line_out, result);
    writeline(logger, line_out);
    swrite(line_out, "zero =   ");
    write(line_out, zero);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;
    
    constant clk_period : time := 10 ns;
    begin
    DUT: floatingunit PORT MAP (
     ra  => ra,
     rb  => rb,
     rc  => rc,
     rt  => rt,
     opcode  => opcode,
     enable => enable,
     result  => result,
     zero  => zero,
     clk => clk
                     );
     
  clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   
   stim_proc: process is
   variable my_line : line;  -- type 'line' comes from textio
   begin		
     
     enable <= '1';

      opcode <= "00001";
      ra <= X"11101021001120113050210430201022";
      rb <= X"44101021001120113030210130101022";
      rc <= (OTHERS => 'X');
      rt <= (OTHERS => 'X');
      
      wait for 10 ns;
      print(opcode, ra, rb, rc, rt, result, zero);
 
      opcode <= "00010";
      wait for 10 ns;
      print(opcode, ra, rb, rc, rt, result, zero);
 
      opcode <= "00011";
      wait for 10 ns;
      print(opcode, ra, rb, rc, rt, result, zero);
 
      opcode <= "00100";
      print(opcode, ra, rb, rc, rt, result, zero);
 
  --    enable <= '0';
  
      wait ;
    REPORT "Test Completed";

  end process;
end;
    
        
