LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY ID_DepChk IS
  PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    
    opcode_0, opcode_1 : IN STD_LOGIC_VECTOR(0 TO 8);   
    ra1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_1    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_2    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    RegWr_ins0  : IN BIT;
    MemRd_ins0  : IN BIT;
    MemWr_ins0  : IN BIT;
    
    RegWr_ins1  : IN BIT;
    MemRd_ins1  : IN BIT;
    MemWr_ins1  : IN BIT;
    
    IMM_SEL1  : in BIT_VECTOR (0 to 1);
    IMM_SEL2  : in BIT_VECTOR (0 to 1);
    
    PC : IN STD_LOGIC_VECTOR(0 TO 31);
    
    opcode_0_PR1, opcode_1_PR1 : OUT STD_LOGIC_VECTOR(0 TO 8);    
    ra1_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_PR1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_PR1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_PR1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_PR1   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr_PR1  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_2_PR1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2_PR1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2_PR1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2_PR1   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    RegWr_ins0_PR1  : OUT BIT;
    MemRd_ins0_PR1  : OUT BIT;
    MemWr_ins0_PR1  : OUT BIT;
    
    RegWr_ins1_PR1  : OUT BIT;
    MemRd_ins1_PR1  : OUT BIT;
    MemWr_ins1_PR1  : OUT BIT;
    
    IMM_SEL1_PR1  : OUT BIT_VECTOR (0 to 1);
    IMM_SEL2_PR1  : OUT BIT_VECTOR (0 to 1);
    
    PC_PR1 : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    reset : IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
END ID_DepChk;

ARCHITECTURE behave OF ID_DepChk IS
BEGIN
  PROCESS(clk)
  BEGIN
  IF clk'event AND clk = '1' THEN -- clk
    IF reset = '1' THEN
      
      opcode_0_PR1 <= (OTHERS => 'Z');  
      ra1_addr_PR1 <= (OTHERS => 'Z');
      rb1_addr_PR1 <= (OTHERS => 'Z');
      rc1_addr_PR1 <= (OTHERS => 'Z');
      rt1_addr_PR1 <= (OTHERS => 'Z');
      imm7_1_PR1   <= (OTHERS => 'Z');
      imm8_1_PR1   <= (OTHERS => 'Z');
      imm10_1_PR1  <= (OTHERS => 'Z');
      imm16_1_PR1  <= (OTHERS => 'Z');
      
      opcode_1_PR1 <= (OTHERS => 'Z');
      ra2_addr_PR1 <= (OTHERS => 'Z');
      rb2_addr_PR1 <= (OTHERS => 'Z');
      rc2_addr_PR1 <= (OTHERS => 'Z');
      rt2_addr_PR1 <= (OTHERS => 'Z');
      imm7_2_PR1   <= (OTHERS => 'Z');
      imm8_2_PR1   <= (OTHERS => 'Z');
      imm10_2_PR1  <= (OTHERS => 'Z');
      imm16_2_PR1  <= (OTHERS => 'Z');
      
      RegWr_ins0_PR1  <= '0'; 
      MemRd_ins0_PR1  <= '0';
      MemWr_ins0_PR1  <= '0';
                              
      RegWr_ins1_PR1  <= '0';
      MemRd_ins1_PR1  <= '0'; 
      MemWr_ins1_PR1  <= '0'; 

      IMM_SEL1_PR1  <= "00"; 
      IMM_SEL2_PR1  <= "00";                        
      
      PC_PR1 <= (OTHERS => 'Z');
    
    ELSE
      
      opcode_0_PR1 <= opcode_0; 
      ra1_addr_PR1 <= ra1_addr;
      rb1_addr_PR1 <= rb1_addr;
      rc1_addr_PR1 <= rc1_addr;
      rt1_addr_PR1 <= rt1_addr;
      imm7_1_PR1   <= imm7_1;
      imm8_1_PR1   <= imm8_1;
      imm10_1_PR1  <= imm10_1;
      imm16_1_PR1  <= imm16_1;
      
      opcode_1_PR1 <= opcode_1;
      ra2_addr_PR1 <= ra2_addr;
      rb2_addr_PR1 <= rb2_addr;
      rc2_addr_PR1 <= rc2_addr;
      rt2_addr_PR1 <= rt2_addr; 
      imm7_2_PR1   <= imm7_2;
      imm8_2_PR1   <= imm8_2;
      imm10_2_PR1  <= imm10_2;
      imm16_2_PR1  <= imm16_2;
      
      RegWr_ins0_PR1  <= RegWr_ins0;
      MemRd_ins0_PR1  <= MemRd_ins0;
  	   MemWr_ins0_PR1  <= MemWr_ins0;
                    
      RegWr_ins1_PR1  <= RegWr_ins1;
      MemRd_ins1_PR1  <= MemRd_ins1;
      MemWr_ins1_PR1  <= MemWr_ins1;
      
      IMM_SEL1_PR1  <= IMM_SEL1;
      IMM_SEL2_PR1  <= IMM_SEL2;
          
      PC_PR1 <= PC;
      
    END IF;
  END IF;
END PROCESS;
END behave;

