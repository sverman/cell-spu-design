LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE IEEE.std_logic_textio.ALL;
USE STD.textio.ALL;

ENTITY tb_dualissuepipe IS
END tb_dualissuepipe;

ARCHITECTURE testbench OF tb_dualissuepipe IS
  
  FILE logger : TEXT OPEN WRITE_MODE IS "transcript_dualissuepipe.log";
 
COMPONENT dualissue_pipe is  
PORT (
     ra_1, rb_1, rc_1, rt_1 : IN STD_LOGIC_VECTOR(0 TO 127);
     opcode_1 : IN STD_LOGIC_VECTOR(0 TO 8);
     result_data1_1 : OUT STD_LOGIC_VECTOR (0 to 127);
     result_data2_1 : OUT STD_LOGIC_VECTOR (0 to 127);
     result_data3_1 : OUT STD_LOGIC_VECTOR (0 to 127);
     
     ra_2, rb_2, rc_2, rt_2 : IN STD_LOGIC_VECTOR(0 TO 127);
     opcode_2 : IN STD_LOGIC_VECTOR(0 TO 8); 
     result_address1_2 : OUT STD_LOGIC_VECTOR(0 TO 31);
     result_address2_2 : OUT STD_LOGIC_VECTOR(0 TO 31);
     result_data_2 : OUT STD_LOGIC_VECTOR (0 to 127);
     clk : IN STD_ULOGIC);
end COMPONENT;

  SIGNAL ra_1, rb_1, rc_1, rt_1 : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL ra_2, rb_2, rc_2, rt_2 : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL opcode_1 : STD_LOGIC_VECTOR(0 TO 8);
  SIGNAL opcode_2 : STD_LOGIC_VECTOR(0 TO 8);
  SIGNAL result_data1_1 :  STD_LOGIC_VECTOR (0 to 127);
  SIGNAL result_data2_1 :  STD_LOGIC_VECTOR (0 to 127);
  SIGNAL result_data3_1 :  STD_LOGIC_VECTOR (0 to 127);
  SIGNAL result_address1_2 : STD_LOGIC_VECTOR(0 TO 31);
  SIGNAL result_address2_2 : STD_LOGIC_VECTOR(0 TO 31);
  SIGNAL result_data_2 : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL clk :  STD_ULOGIC;
  
    PROCEDURE print(opcode_1 : STD_LOGIC_VECTOR(0 TO 8);
                  ra_1 : STD_LOGIC_VECTOR(0 TO 127);
                  rb_1 : STD_LOGIC_VECTOR(0 TO 127);
                  rc_1 : STD_LOGIC_VECTOR(0 TO 127);
                  rt_1 : STD_LOGIC_VECTOR(0 TO 127);                
                  result_address1_2 : STD_LOGIC_VECTOR(0 TO 31);
                  result_address2_2 : STD_LOGIC_VECTOR(0 TO 31);
                  result_data_2 : STD_LOGIC_VECTOR(0 TO 127)
                  ) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
    BEGIN
    swrite(line_out, "opcode_1 = ");
    write(line_out, opcode_1);
    writeline(logger, line_out);
    swrite(line_out, "ra_1 =     ");
    hwrite(line_out, ra_1);
    writeline(logger, line_out);
    swrite(line_out, "rb_1 =     ");
    hwrite(line_out, rb_1);
    writeline(logger, line_out);
    swrite(line_out, "rc_1 =     ");
    hwrite(line_out, rc_1);
    writeline(logger, line_out);
    swrite(line_out, "rt_1 =     ");
    hwrite(line_out, rt_1);
    writeline(logger, line_out);
    swrite(line_out, "result_address1_2 = ");
    hwrite(line_out, result_address1_2);
    writeline(logger, line_out);
    swrite(line_out, "result_address2_2= ");
    write(line_out, result_address2_2);
    writeline(logger, line_out);
    swrite(line_out, "result_data2= ");
    write(line_out, result_data_2);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;
  
  -- definition of a clock period
  CONSTANT clk_period : time := 10 ns;
  
BEGIN
  -- component instantiation
  DUT: dualissue_pipe PORT MAP (
    ra_1 => ra_1,
    rb_1 => rb_1,
    rc_1 => rc_1,
    rt_1 => rt_1,
    opcode_1 => opcode_1,
    result_data1_1 => result_data1_1,
    result_data2_1 => result_data2_1,
    result_data3_1 => result_data3_1,
    
    ra_2 => ra_2,
    rb_2 => rb_2,
    rc_2 => rc_2,
    rt_2 => rt_2,
    opcode_2 => opcode_2,
    result_address1_2 => result_address1_2,
    result_address2_2 => result_address2_2,
    result_data_2 => result_data_2,
    clk => clk
);
    
    CLK_PROCESS : PROCESS
    BEGIN
		  clk <= '1';
		  WAIT FOR clk_period/2;
		  
		  clk <= '0';
		  WAIT FOR clk_period/2;
    END PROCESS;
    
    SIMULATION : PROCESS
    BEGIN
    
      opcode_2 <= "000111110"; -- byte unit -- compare equal bytes   
      ra_2 <= X"00000F000000000000000F0000000000";
      rb_2 <= X"00000000000000000000000000000000";
      rc_2 <= (OTHERS => 'X');
      rt_2 <= (OTHERS => 'X');
      WAIT FOR 20 ns;
  
      opcode_1 <= "000010011"; -- odd pipe
      ra_1(0 TO 127) <= X"1230000F0012334F0004563F0000321F";
      rb_1(0 TO 127) <= X"3210000F7648000F0002378F0000123F";
      rc_1 <= (OTHERS => 'X');
      rt_1 <= (OTHERS => 'X');
      WAIT FOR 20 ns;
  
  REPORT "Test Completed";
 
  WAIT;
  
END PROCESS;
END;
