library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.textio.all;  
use IEEE.std_logic_textio.all;

Entity test_localstore is
Generic(W : natural := 32);
end test_localstore;
architecture tb_regfile of test_localstore is
component localstore is	PORT
	( clk		     	: IN  std_logic;
	 enable      : IN std_logic;
		datain	  		: IN  std_logic_vector(0 TO 127);
		rw_address	: IN  std_logic_vector( 0 TO W-1);
   -- read_address	: IN  std_logic_vector(0 TO 31);
		wr1_e	      : IN  std_logic;
		wr2_e       : IN std_logic;
		dataout1		 : OUT std_logic_vector(0 TO 127);
		dataout2   : OUT std_logic_vector(0 TO 1023)
	);
end component;
     signal clk  : std_logic;
     signal datain  : std_logic_vector(0 TO 127);
     signal rw_address  :  std_logic_vector (0 to W-1);
     signal wr1_e  :  std_logic;
     signal wr2_e  :  std_logic;
     signal dataout1   :  std_logic_vector(0 to 127);
     signal dataout2  :  std_logic_vector(0 to 1023);
     signal enable : std_logic;
     constant clk_period : time := 10 ns;
     begin
    uut: localstore PORT MAP (
   datain   => datain,
   enable => enable,
   wr1_e => wr1_e,
   rw_address => rw_address,
   wr2_e => wr2_e,
   dataout1 => dataout1,
   dataout2 => dataout2,
   clk => clk
   );
   clk_process :process
    begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;   
   stim_proc: process is
   variable my_line : line;  -- type 'line' comes from textio
   begin		
--     print($time,, " A1=%d, A2=%d, A3=%d, WD3=%d, WD4=%d, RD1=%d, RD2=%d We3= %d , We4= %d ",A1,A2,A3,WD3,WD4,RD1,RD2,We3,We4,);
      -- hold reset state for 100 ns.
     -- wait for 100 ns;	
     enable <='1';
      wait for clk_period;
  --    wr1_e <= '1';
    wr2_e <='1';
  -- wait for 10 ns;
    -- rw_address <= X"12401114";
      --datain <= X"1234567891987654321000000000000f";
    -- wait for 10 ns;
   --  rw_address <= X"12401114";
      datain <= X"22225678919876543210ffffff0f0fff";
    -- wr1_e <= '0';
    --  wait for 10 ns;
    -- wr2_e <= '1';
    -- rw_address <= X"12401114";
   --   datain <= X"1234567891987654321000000000000f";
    --  wait for 10 ns;
    wr2_e <= '0';
    --  rw_address <= X"12401110";
      --datain <= X"1234567891987654321000052524747f";

    wait ;
  end process;
end;
    
        