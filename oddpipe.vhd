library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
Entity oddpipe is
   generic (clk_period :INTEGER :=  10);
PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 7);
    sel : IN STD_ULOGIC;
    
    RegWr_ins1_ep  : IN BIT;
    MemRd_ins1_ep  : IN BIT;
    MemWr_ins1_ep  : IN BIT;
    PC_ep : IN STD_LOGIC_VECTOR(0 TO 31);
    RegWr_ins1_ep_o  : OUT BIT;
    MemRd_ins1_ep_o  : OUT BIT;
    MemWr_ins1_ep_o  : OUT BIT;
    PC_ep_o : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    --tag_address : IN STD_LOGIC_VECTOR(0 TO 31); -- program counter
    flush : OUT BIT;
    load_LS : OUT BIT;
    pc_write : OUT BIT;
    offset : OUT STD_LOGIC_VECTOR(0 TO 4);

    result_address1 : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_address2 : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_data : OUT STD_LOGIC_VECTOR (0 to 127);
    clk: IN STD_ULOGIC);
  end oddpipe;
  
  ARCHITECTURE behave of oddpipe is
        
signal    CU_OPCODE : STD_LOGIC_VECTOR(0 TO 7);
signal    EU_OPCODE : STD_LOGIC_VECTOR(0 TO 2);
signal    enable_lsu : STD_ULOGIC;
signal    enable_bru : STD_ULOGIC;
signal    enable_perm : STD_ULOGIC;

--   bits 5:7 in CU_OPCODE decides the execution unit 
signal    tag_address_s :  STD_LOGIC_VECTOR(0 TO 31);
signal    flush_s :  BIT;
signal    load_LS_s :  BIT;
signal    pc_write_s :  BIT;
signal    offset_s :  STD_LOGIC_VECTOR(0 TO 4);
  
signal    ra_s, rb_s, rc_s, rt_s :  STD_LOGIC_VECTOR(0 TO 127);
signal    opcode_s :  STD_LOGIC_VECTOR(0 TO 7);
signal    result_address_s1 :  STD_LOGIC_VECTOR(0 TO 31);
signal    result_address_s2 :  STD_LOGIC_VECTOR(0 TO 31);
signal    result_data_s :  STD_LOGIC_VECTOR ( 0 to 127);
signal    clk_s:  STD_ULOGIC;

component LoadStoreUnit is
  PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    result_address : OUT STD_LOGIC_VECTOR(0 TO 31);
    clk: IN STD_ULOGIC);
end component;    
    
component branchunit is
  
PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    tag_address : IN STD_LOGIC_VECTOR(0 TO 31);
    flush : OUT BIT;
    load_LS : OUT BIT;
    pc_write : OUT BIT;
    offset : OUT STD_LOGIC_VECTOR(0 TO 4);
    result_address : OUT STD_LOGIC_VECTOR(0 TO 31);
    clk: IN STD_ULOGIC);
END component;


component permute IS
  PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    clk: IN STD_ULOGIC;
    enable : IN STD_ULOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_ULOGIC);
END component;
begin 

-- begin execution on even pipe depending on the instruction and opcode
    
 LSU : loadstoreunit port map (
          ra => ra_s,
          rb => rb_s,
          rc => rc_s,
          rt => rt_s,
          opcode(0 to 4) => CU_OPCODE(0 to 4),
          result_address => result_address_s1,
          enable => enable_lsu,
          clk => clk
        );

 PERMU : permute port map (
          ra => ra_s,
          rb => rb_s,
          rc => rc_s,
          rt => rt_s,
          opcode(0 to 4) => CU_OPCODE(0 to 4),
          result => result_data_s,
          enable => enable_bru,
          clk => clk
        );


 BRU : branchunit port map (
          ra => ra_s,
          rb => rb_s,
          rc => rc_s,
          rt => rt_s,
          tag_address => tag_address_s,
          flush =>flush_s,
          load_LS =>load_LS_s,
          pc_write =>pc_write_s,
          offset => offset_s,
          opcode(0 to 4) => CU_OPCODE(0 to 4),
          result_address => result_address_s2,
          enable => enable_perm,
          clk => clk
        ); 
        
  ra_s <= ra;
  rb_s <= rb; 
  rc_s <= rc;
  rt_s <= rt;
  opcode_s <= opcode;
  result_data <= result_data_s;
  result_address1 <= result_address_s1;
  result_address2 <= result_address_s2;
  
  tag_address_s <= PC_ep;
  
   flush <= flush_s;
   load_LS <= load_LS_s;
   pc_write <= pc_write_s;
   offset <= offset_s;
  
  
  clk_s <= clk;
process (clk)
-- variable declarations
  
begin        
  
  
  EU_OPCODE(0 to 2) <= opcode_s ( 5 to 7);
 CU_OPCODE(0 to 4) <= opcode_s ( 0 to 4);

--  if clk'event and  clk ='1' 
 -- then 
    if sel ='1'
    then
  
      if EU_OPCODE = "010" --LSU
      then 
        enable_lsu <='1';
         RegWr_ins1_ep_o <= transport RegWr_ins1_ep after clk_period*4 ns;
         MemRd_ins1_ep_o <= transport MemRd_ins1_ep after clk_period*4 ns;
         MemWr_ins1_ep_o <= transport MemWr_ins1_ep after clk_period*4 ns;
    
      else enable_lsu <= '0';
      end if;

      if EU_OPCODE = "100" --BRU
      then 
        enable_bru <='1';
         RegWr_ins1_ep_o <= transport RegWr_ins1_ep after clk_period*2 ns;
         MemRd_ins1_ep_o <= transport MemRd_ins1_ep after clk_period*2 ns;
         MemWr_ins1_ep_o <= transport MemWr_ins1_ep after clk_period*2 ns;
    
      else enable_bru <='0';
      end if;

      if EU_OPCODE = "001" --PERMU
      then 
        enable_perm <='1';
         RegWr_ins1_ep_o <= transport RegWr_ins1_ep after clk_period*1 ns;
         MemRd_ins1_ep_o <= transport MemRd_ins1_ep after clk_period*1 ns;
         MemWr_ins1_ep_o <= transport MemWr_ins1_ep after clk_period*1 ns;
    
      else enable_perm <='0';
      end if;

      else
   --   enable_lsu <= '0';
    --  enable_bru <= '0';
     -- enable_perm <= '0';
      end if;
        
--    end if;
  end process;
end behave;