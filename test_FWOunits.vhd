
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.textio.all;  
use IEEE.std_logic_textio.all;

Entity test_FWOunits is

end test_FWOunits;

architecture tb_FWOunits of test_FWOunits is

component FWOunit is
  
port(
    
    lsu_result: IN STD_LOGIC_VECTOR(0 TO 31);
    bru_result: IN STD_LOGIC_VECTOR(0 TO 31);
    perm_result: IN STD_LOGIC_VECTOR(0 TO 127);
    reset : IN STD_LOGIC;
    fwo1_enable : IN STD_LOGIC;
    fwo2_enable : IN STD_LOGIC;
    fwo3_enable : IN STD_LOGIC;
    fwo1_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo2_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo3_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo_out_bus : OUT STD_LOGIC_VECTOR(0 TO 127); 
    clk: IN STD_LOGIC
     
     );
     
end component;

    signal lsu_result: STD_LOGIC_VECTOR(0 TO 31);
    signal bru_result: STD_LOGIC_VECTOR(0 TO 31);
    signal perm_result: STD_LOGIC_VECTOR(0 TO 127);
    signal reset : STD_LOGIC;
    signal fwo1_enable : STD_LOGIC;
    signal fwo2_enable : STD_LOGIC;
    signal fwo3_enable : STD_LOGIC;
    
    signal fwo1_dataout_bus : STD_LOGIC_VECTOR(0 TO 127);
    signal fwo2_dataout_bus : STD_LOGIC_VECTOR(0 TO 127);
    signal fwo3_dataout_bus : STD_LOGIC_VECTOR(0 TO 127);
    signal fwo_out_bus :  STD_LOGIC_VECTOR(0 TO 127); 
    signal clk: STD_LOGIC;
    constant clk_period : time := 10 ns;
    begin
    uut: FWOunit PORT MAP (
     lsu_result   => lsu_result,
     reset => reset,
     bru_result => bru_result,
     perm_result => perm_result,
     fwo1_enable => fwo1_enable,
     fwo2_enable => fwo2_enable,
     fwo3_enable => fwo3_enable,
      fwo1_dataout_bus  => fwo1_dataout_bus,
     fwo2_dataout_bus  => fwo2_dataout_bus,
     fwo3_dataout_bus  => fwo3_dataout_bus,
      fwo_out_bus => fwo_out_bus,
     clk => clk
        );
     
    clk_process :process
    begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   
   stim_proc: process is
   variable my_line : line;  -- type 'line' comes from textio

   begin		
--     print($time,, " A1=%d, A2=%d, A3=%d, WD3=%d, WD4=%d, RD1=%d, RD2=%d We3= %d , We4= %d ",A1,A2,A3,WD3,WD4,RD1,RD2,We3,We4,);
      -- hold reset state for 100 ns.
     -- wait for 100 ns;	
     
    perm_result  <= (others => 'Z');
        bru_result  <= (others => 'Z');
         lsu_result  <= (others => 'Z');
    wait for clk_period;
     reset <= '0';
   --  fwo1_enable <= '1';
     --   wait for clk_period*10;
     lsu_result  <= X"54321678";
         --perm_result  <= X"543216789198765432100005252fefef";
 ----         wait for clk_period;
    -- fwo2_enable <= '1';
 --      perm_result  <= (others => 'Z');
 --       bru_result  <= (others => 'Z');
  --       lsu_result  <= (others => 'Z');
       
    wait for clk_period;
  --  bru_result  <= X"12345678";
--    wait for clk_period;
  --   bru_result  <= (others => 'Z');
-- fwo3_enable <= '1';
    wait for clk_period;
  --  lsu_result  <= X"2450789f";
    --  reset <= '1';
    wait ;
  end process;
end;
    
        

