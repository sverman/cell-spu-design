
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

Entity FWOunit is
  
PORT ( 
    lsu_result: IN STD_LOGIC_VECTOR(0 TO 31);
    bru_result: IN STD_LOGIC_VECTOR(0 TO 31);
    perm_result: IN STD_LOGIC_VECTOR(0 TO 127);
    reset : IN STD_LOGIC;
    fwo1_enable : IN STD_LOGIC;
    fwo2_enable : IN STD_LOGIC;
    fwo3_enable : IN STD_LOGIC;
    fwo1_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo2_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo3_dataout_bus : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwo_out_bus : OUT STD_LOGIC_VECTOR(0 TO 127); 
    clk: IN STD_LOGIC);
END FWOunit;

ARCHITECTURE behave OF FWOunit IS
component FWE01 is
  
port(
    
   fwd_datain : IN STD_LOGIC_VECTOR(0 TO 127);
    out_enable : IN STD_LOGIC;
    reset : in STD_LOGIC; 
    fwd_dataout : OUT STD_LOGIC_VECTOR(0 TO 127);
    fwd_regout : out STD_LOGIC_VECTOR (0 to 127);
    clk: IN STD_LOGIC
     
     );
end component;


signal fwo1_output : STD_LOGIC_VECTOR ( 0 to 127);
signal fwo2_output : STD_LOGIC_VECTOR ( 0 to 127);
signal fwo3_output : STD_LOGIC_VECTOR ( 0 to 127);

signal fwo1_input : STD_LOGIC_VECTOR ( 0 to 127);
signal fwo2_input : STD_LOGIC_VECTOR ( 0 to 127);
signal fwo3_input : STD_LOGIC_VECTOR ( 0 to 127);

BEGIN
fwo1_input <= perm_result;
fwo2_input (0 to 31) <= bru_result;
fwo2_input <= fwo1_output;
fwo3_input(0 to 31) <= lsu_result;
fwo3_input <= fwo2_output;
fwo_out_bus <= fwo3_output;
  
   fwd1 : FWE01 PORT MAP (
    fwd_datain  => fwo1_input,
    reset => reset,
    out_enable => fwo1_enable,
    fwd_dataout => fwo1_dataout_bus,
    fwd_regout => fwo1_output,
    clk   => clk
        );
     
   fwd2 : FWE01 PORT MAP (
  fwd_datain  => fwo2_input,
    reset => reset,
    out_enable => fwo2_enable,
    fwd_dataout => fwo2_dataout_bus,
    fwd_regout => fwo2_output,
    clk   => clk
        );
        
   fwd3 : FWE01 PORT MAP (
   fwd_datain  => fwo3_input,
    reset => reset,
    out_enable => fwo3_enable,
    fwd_dataout => fwo3_dataout_bus,
    fwd_regout => fwo3_output,
    clk   => clk
        );
  

end behave;
  