LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE IEEE.std_logic_textio.ALL;
USE STD.textio.ALL;

ENTITY tb_evenpipe IS
END tb_evenpipe;

  ARCHITECTURE testbench OF tb_evenpipe IS
  
  FILE logger : TEXT OPEN WRITE_MODE IS "transcript_evenpipe.log";
  
  COMPONENT evenpipe 
  PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    sel : IN STD_ULOGIC;
    opcode : IN STD_LOGIC_VECTOR(0 TO 7);
    RegWr_ins0_ep  : IN BIT;
    MemRd_ins0_ep  : IN BIT;
    MemWr_ins0_ep  : IN BIT;
   
    PC_ep : IN STD_LOGIC_VECTOR(0 TO 31);
 
    RegWr_ins0_ep_o  : OUT BIT;
    MemRd_ins0_ep_o  : OUT BIT;
    MemWr_ins0_ep_o  : OUT BIT;
  
    PC_ep_o : OUT STD_LOGIC_VECTOR(0 TO 31);
   
  --  result_address : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_data1 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data2 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data3 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    clk: IN STD_ULOGIC);
  END COMPONENT;
  
  SIGNAL ra : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rb : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rc : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL rt : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL sel : STD_ULOGIC;
  SIGNAL opcode : STD_LOGIC_VECTOR(0 TO 7);
  signal RegWr_ins0_ep  :  BIT;
  signal MemRd_ins0_ep  :  BIT;
  signal MemWr_ins0_ep  :  BIT;

  signal PC_ep :  STD_LOGIC_VECTOR(0 TO 31);
  signal RegWr_ins0_ep_o  :  BIT;
  signal MemRd_ins0_ep_o  :  BIT;
  signal MemWr_ins0_ep_o  :  BIT;

  signal PC_ep_o :  STD_LOGIC_VECTOR(0 TO 31);
  
--  SIGNAL result_address : STD_LOGIC_VECTOR(0 TO 31);
  SIGNAL result_data1 : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL result_data2 : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL result_data3 : STD_LOGIC_VECTOR(0 TO 127);
  SIGNAL clk :  STD_ULOGIC;
  
  PROCEDURE print(opcode : STD_LOGIC_VECTOR(0 TO 7);
                  ra : STD_LOGIC_VECTOR(0 TO 127);
                  rb : STD_LOGIC_VECTOR(0 TO 127); 
                  rc : STD_LOGIC_VECTOR(0 TO 127);
                  rt : STD_LOGIC_VECTOR(0 TO 127);                
               --   result_address : STD_LOGIC_VECTOR(0 TO 31);
                  result_data1 : STD_LOGIC_VECTOR(0 TO 127);
                  result_data2 : STD_LOGIC_VECTOR(0 TO 127);
                  result_data3 : STD_LOGIC_VECTOR(0 TO 127)
                  ) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
    BEGIN
    swrite(line_out, "opcode = ");
    write(line_out, opcode);
    writeline(logger, line_out);
    swrite(line_out, "ra =     ");
    hwrite(line_out, ra);
    writeline(logger, line_out);
    swrite(line_out, "rb =     ");
    hwrite(line_out, rb);
    writeline(logger, line_out);
    swrite(line_out, "rc =     ");
    hwrite(line_out, rc);
    writeline(logger, line_out);
    swrite(line_out, "rt =     ");
    hwrite(line_out, rt);
    writeline(logger, line_out);
  --  swrite(line_out, "result_address = ");
  --  hwrite(line_out, result_address);
  --  writeline(logger, line_out);
    swrite(line_out, "result_data1= ");
    write(line_out, result_data1);
    writeline(logger, line_out);
    swrite(line_out, "result_data2= ");
    write(line_out, result_data2);
    writeline(logger, line_out);
    swrite(line_out, "result_data3= ");
    write(line_out, result_data3);
 --   writeline(logger, line_out);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;
  
  -- definition of a clock period
  CONSTANT clk_period : time := 10 ns;
  
BEGIN
  -- component instantiation
  DUT: evenpipe PORT MAP (
    ra => ra,
    rb => rb,
    rc => rc,
    rt => rt,
    clk => clk,
    sel => sel,
    opcode => opcode,
    RegWr_ins0_ep =>    RegWr_ins0_ep,
    MemRd_ins0_ep =>    MemRd_ins0_ep,
    MemWr_ins0_ep =>    MemWr_ins0_ep,
  
    PC_ep =>     PC_ep,
    RegWr_ins0_ep_o =>    RegWr_ins0_ep_o,
    MemRd_ins0_ep_o =>    MemRd_ins0_ep_o,
    MemWr_ins0_ep_o =>    MemWr_ins0_ep_o,
  
    PC_ep_o =>     PC_ep_o,
    
  --  result_address => result_address,
    result_data1 => result_data1,
    result_data2 => result_data2,
    result_data3 => result_data3
    );
    
    CLK_PROCESS : PROCESS
    BEGIN
		  clk <= '0';
		  WAIT FOR clk_period/2;
		  
		  clk <= '1';
		  WAIT FOR clk_period/2;
    END PROCESS;
    
    SIMULATION : PROCESS
    BEGIN
    sel <='0';

  -- (1) a (add word)
  opcode <= "00001101";
  RegWr_ins0_ep <= '1';
  MemRd_ins0_ep <= '1';
  MemWr_ins0_ep <= '1';

  PC_ep <= X"23451235";

  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rb(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt,result_data1,result_data2,result_data3);

  -- (2) ah (add half word)
  opcode <= "00010101";
  opcode <= "00001101";
  RegWr_ins0_ep <= '0';
  MemRd_ins0_ep <= '1';
  MemWr_ins0_ep <= '0';

  ra(0 TO 127) <= X"000F000F000F000F000F000F000F000F";
  rb(0 TO 127) <= X"000F000F000F000F000F000F000F000F";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result_data1,result_data2,result_data3);

  -- (3) and (and word)
  opcode <= "00100011";
  opcode <= "00001101";
  RegWr_ins0_ep <= '1';
  MemRd_ins0_ep <= '0';
  MemWr_ins0_ep <= '1';
 
  ra(0 TO 127) <= X"11101021001120113050210430201022";
  rb(0 TO 127) <= X"44101021001120113030210130101022";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result_data1,result_data2,result_data3);

  -- (4) or (or word)
  opcode <= "00010011";
   RegWr_ins0_ep <= '0';
  MemRd_ins0_ep <= '1';
  MemWr_ins0_ep <= '0';
  ra(0 TO 127) <= X"11101021001120113050210430201022";
  rb(0 TO 127) <= X"44101021001120113030210130101022";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt,result_data1,result_data2,result_data3);

  -- (5) nand (nand word)
  opcode <= "00100011";
   RegWr_ins0_ep <= '1';
  MemRd_ins0_ep <= '0';
  MemWr_ins0_ep <= '1';
  ra(0 TO 127) <= X"11101021001120113050210430201022";
  rb(0 TO 127) <= X"44101021001120113030210130101022";
  rc(0 TO 127) <= (OTHERS => 'X');
  rt(0 TO 127) <= (OTHERS => 'X');
  WAIT FOR 10 ns;
  print(opcode, ra, rb, rc, rt, result_data1,result_data2,result_data3);

--  -- (6) nor (nor word)
--  -- opcode <= "00110";
--  --ra(0 TO 127) <= X"FFFFFFF0FFFFFFF0FFFFFFF0FFFFFFF0";
--  rb(0 TO 127) <= X"FFFFFFF0FFFFFFF0FFFFFFF0FFFFFFF0";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--
--  -- (7) xor (xor word)
--  --opcode <= "00111";
--  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
--  rb(0 TO 127) <= X"00000000000000000000000000000000";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--    print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--
--  -- (8) sfh (subtract from half word)
--  --opcode <= "01000";    
--  ra <= X"000F000F000F000F000F000F000F000F";
--  rb <= X"000F000F000F000F000F000F000F000F";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--    print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--  
--  -- (9) sf (subtract from word)
--  --opcode <= "01001";
--  ra <= X"0000000F0000000F0000000F0000000F";
--  rb <= X"0000000F0000000F0000000F0000000F";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--   print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--  
--  -- (10) bg (Borrow Generate)
--  --opcode <= "01010";   
--  ra <= X"00000F000000000000000F0000000000";
--  rb <= X"00000000000000000000000000000000";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--  
--  -- (11) eqv (equivalent)
--  --opcode <= "01011";
--  ra <= X"FFFFFFFF000000000000000000000000";
--  rb <= X"00000000000000000000000000000000";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--  
--  -- (12) ceq (Compare Equal Word)
--  --opcode <= "01100"; 
--  ra <= X"00000000000000000000000000000000";
--  rb <= X"FFFFFFFF00000000FFFFFFFF00000000";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--  
--  -- (13) shl (Shift Left Word) -- FIX      
--  --opcode <= "01101";        
--  ra <= X"000000000000000000000000FFFFFFFF";
--  rb(0 TO 23) <= X"000000";
--  rb(24 TO 31) <= "00000001";
--  rb(32 TO 127) <= X"000000000000000000000000";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--    
--  -- (14) rot (Rotate Word)
--  --opcode <= "01110";    
--  ra <= X"000000000000000000000000FFFFFFFF";
--  rb(0 TO 23) <= X"000000";
--  rb(24 TO 31) <= "00000001";
--  rb(32 TO 127) <= X"000000000000000000000000";
--  rc(0 TO 127) <= (OTHERS => 'X');
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--    print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--  
--  -- IMMEDIATE TYPE INSTRUCTIONS
--  -- (15) ahi (Add Half Word Immediate)
--  --opcode <= "01111";
--  ra(0 TO 127) <= X"000F000F000F000F000F000F000F000F";
--  rb(0 TO 127) <= (OTHERS => 'X');
--  rc(0 TO 127) <= X"000F000F000F000F000F000F000F000F";
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--      
--  -- (16) ai (Add Word Immediate)
--  --opcode <= "10000";
--  ra(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
--  rb(0 TO 127) <= (OTHERS => 'X');
--  rc(0 TO 127) <= X"0000000F0000000F0000000F0000000F";
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
--  
--  -- (17) sfhi (Subtract From Halfword Immediate)
--  --opcode <= "10001";
--  ra <= X"000F000F000F000F000F000F000F000F";
--  rb(0 TO 127) <= (OTHERS => 'X');
--  rc <= X"000F000F000F000F000F000F000F000F";
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
-- 
--  -- (18) sfi (Subtract From Word Immediate)
--  --opcode <= "10010";
--  sel<= '0';
--  ra <= X"0000000F0000000F0000000F0000000F";
--  rb(0 TO 127) <= (OTHERS => 'X');
--  rc <= X"0000000F0000000F0000000F0000000F";
--  rt(0 TO 127) <= (OTHERS => 'X');
--  WAIT FOR 10 ns;
--  print(opcode, ra, rb, rc, rt, result_address,result_data1,result_data2,result_data3);
  
  REPORT "Test Completed";
 
  WAIT;
  
END PROCESS;
END;
