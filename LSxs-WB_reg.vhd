LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY LSxs_WB IS
  PORT(
    result1, rt1 : IN STD_LOGIC_VECTOR(0 TO 127); 
    result2, rt2 : IN STD_LOGIC_VECTOR(0 TO 127);
    
    w_rt1 : IN STD_ULOGIC;
    w_rt2 : IN STD_ULOGIC;
    
    PC : IN STD_LOGIC_VECTOR(0 TO 31);
	  PC_wr : IN STD_ULOGIC; 
    
    result1_PR7, rt1_PR7 : OUT STD_LOGIC_VECTOR(0 TO 127);
    result2_PR7, rt2_PR7 : OUT STD_LOGIC_VECTOR(0 TO 127);
    
    w_rt1_PR7 : OUT STD_ULOGIC;
    w_rt2_PR7 : OUT STD_ULOGIC;
   	PC_wr_PR7 : OUT STD_ULOGIC;
    
    PC_PR7 : OUT STD_LOGIC_VECTOR(0 TO 31);
    
    reset : IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
END LSxs_WB;

ARCHITECTURE behave OF LSxs_WB IS
BEGIN
  PROCESS(clk)
  BEGIN
  IF clk'event AND clk = '1' THEN -- clk
    IF reset = '1' THEN
       
      result1_PR7 <= (OTHERS => 'Z');
      rt1_PR7 <= (OTHERS => 'Z');
      w_rt1_PR7 <= 'Z';
      
      result2_PR7 <= (OTHERS => 'Z');
      rt2_PR7 <= (OTHERS => 'Z');
      w_rt2_PR7 <= 'Z';
      PC_wr_PR7 <= 'Z';
      PC_PR7 <= (OTHERS => 'Z');
      
    ELSE
    
      result1_PR7 <= result1;
      rt1_PR7 <= rt1;
      w_rt1_PR7 <= w_rt1;
      
      result2_PR7 <= result2;
      rt2_PR7 <= rt2;
      w_rt2_PR7 <= w_rt2;
      PC_wr_PR7 <= PC_wr;
      PC_PR7 <= PC;
      
    END IF;
  END IF;
END PROCESS;
END behave;