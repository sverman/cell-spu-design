LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY EX_LSxs IS
  PORT(
    result1 : IN STD_LOGIC_VECTOR(0 TO 127); -- even pipe
    result2 : IN STD_LOGIC_VECTOR(0 TO 127); -- odd pipe
    RegWrite1 : IN BIT;
    RegWrite2 : IN BIT;
    rt1 : IN STD_LOGIC_VECTOR(0 TO 6); -- even pipe rt address to write to the register
    rt2 : IN STD_LOGIC_VECTOR(0 TO 6); -- odd pipe rt address to write to the register
    
    w_rt1 : IN STD_LOGIC; -- write signal to register file for rt1
    w_rt2 : IN STD_LOGIC; -- write signal to register file for rt2
 
 ----------------------------------
 
    MemRd1 : IN BIT; --only from odd pipe
    MemWr1 : IN BIT; --only from odd pipe

    ----------------------------------   
    PC : IN STD_LOGIC_VECTOR(0 TO 31); 
    
    result1_PR4 : OUT STD_LOGIC_VECTOR(0 TO 127); 
    result2_PR4 : OUT STD_LOGIC_VECTOR(0 TO 127); 
    rt1_PR4 : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_PR4 : OUT STD_LOGIC_VECTOR(0 TO 6);
    w_rt1_PR4  : OUT STD_LOGIC;
    w_rt2_PR4  : OUT STD_LOGIC;
   
    wr1_ls_PR4 : OUT STD_LOGIC;-- output register derived from Memrd/Memwr coming from previous pipeline stages
  --  wr2_ls_PR4 : OUT STD_LOGIC;
  --  MemRd1_PR4 : OUT STD_LOGIC; 
  --  MemWr1_PR4 : OUT STD_LOGIC;   
    
    enable_ls_PR4 :  OUT STD_LOGIC;
    RegWrite1_PR4 : OUT BIT;
    RegWrite2_PR4 : OUT BIT;
    PC_PR4 : OUT STD_LOGIC_VECTOR(0 TO 31); 
    
    reset : IN STD_LOGIC;
    clk: IN STD_LOGIC);
    END EX_LSxs;

ARCHITECTURE behave OF EX_LSxs IS
BEGIN
  PROCESS(clk)
  BEGIN
  IF clk'event AND clk = '1' THEN -- clk
    
    IF reset = '1' THEN
       
      result1_PR4 <= (OTHERS => 'Z');
      rt1_PR4 <= (OTHERS => 'Z');
      w_rt1_PR4 <= 'Z';
      
      wr1_ls_PR4 <= 'Z';
      
      result2_PR4 <= (OTHERS => 'Z');
      rt2_PR4 <= (OTHERS => 'Z');
      w_rt2_PR4 <= 'Z';
      RegWrite1_PR4 <= '0';
      RegWrite2_PR4 <= '0';
      enable_ls_PR4 <= '0';
      
      PC_PR4 <= (OTHERS => 'Z');
      
      ELSE
        
   -------------------     
      if MemRd1 = '1' or MemWr1 ='1'
      then
      enable_ls_PR4 <='1';
      else 
      enable_ls_PR4 <='0';
      end if;
      
       if MemRd1 = '1' 
       then
       wr1_ls_PR4 <= '0';
       elsif MemWr1 ='1'
      then
       wr1_ls_PR4 <= '1';
      end if;
  --------------------     
      result1_PR4 <= result1;
      rt1_PR4 <= rt1;
      w_rt1_PR4 <= w_rt1;
      result2_PR4 <= result2;
      rt2_PR4 <= rt2;
      w_rt2_PR4 <= w_rt2;
  --------------------   
      RegWrite1_PR4 <= RegWrite1;
      RegWrite2_PR4 <= RegWrite2;

           
      PC_PR4 <= PC;
      
    END IF;
  END IF;
END PROCESS;
END behave;
