library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity evenpipe is  
  generic (clk_period :INTEGER :=  10);
PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    sel : IN STD_ULOGIC;
    opcode : IN STD_LOGIC_VECTOR(0 TO 7);
    RegWr_ins0_ep  : IN BIT;
    MemRd_ins0_ep  : IN BIT;
    MemWr_ins0_ep  : IN BIT;
    PC_ep : IN STD_LOGIC_VECTOR(0 TO 31);
 
    result_data1 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data2 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data3 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    RegWr_ins0_ep_o  : OUT BIT;
    MemRd_ins0_ep_o  : OUT BIT;
    MemWr_ins0_ep_o  : OUT BIT;
    PC_ep_o : OUT STD_LOGIC_VECTOR(0 TO 31);
      
    clk: IN STD_ULOGIC);
end evenpipe;
  
ARCHITECTURE behave of evenpipe is
    
signal    CU_OPCODE : STD_LOGIC_VECTOR(0 TO 7);
signal    EU_OPCODE : STD_LOGIC_VECTOR(0 TO 2);
signal    enable_fxu : STD_ULOGIC;
signal    enable_fpu : STD_ULOGIC;
signal    enable_byte : STD_ULOGIC;

-- bits 5:7 in CU_OPCODE decides the execution unit 

signal    ra_s, rb_s, rc_s, rt_s :  STD_LOGIC_VECTOR(0 TO 127);
signal    opcode_s :  STD_LOGIC_VECTOR(0 TO 7);
--signal    result_address_s :  STD_LOGIC_VECTOR(0 TO 31);
signal    result_data_s1 :  STD_LOGIC_VECTOR ( 0 to 127);
signal    result_data_s2 :  STD_LOGIC_VECTOR ( 0 to 127);
signal    result_data_s3 :  STD_LOGIC_VECTOR ( 0 to 127);
signal    clk_s:  STD_ULOGIC;

component fxu IS
PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_ULOGIC);
END component;

    
component floatingunit is
  
PORT ( 
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    enable : IN STD_ULOGIC;
    zero : OUT STD_ULOGIC;
    clk: IN STD_ULOGIC);
END component;

component byte IS
  PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_ULOGIC);
END component;
begin 
  
  
FXUnit : fxu port map (
          ra => ra_s,
          rb => rb_s,
          rc => rc_s,
          rt => rt_s,
          opcode(0 to 4) => opcode_s( 0 to 4),
          result => result_data_s1,
          enable => enable_fxu,
          clk => clk_s
        );

FPU : floatingunit port map (
          ra => ra_s,
          rb => rb_s,
          rc => rc_s,
          rt => rt_s,
          opcode(0 to 4) => opcode_s(0 to 4),
          result => result_data_s2,
          enable => enable_fpu,
          clk => clk_s
        );


Byteunit : byte port map (
          ra => ra_s,
          rb => rb_s,
          rc => rc_s,
          rt => rt_s,
          opcode(0 to 4) => opcode_s(0 to 4),
          result => result_data_s3,
          enable => enable_byte,
          clk => clk_s
        ); 
  
    ra_s <= ra;
  rb_s <= rb;
  rc_s <= rc;
  opcode_s <= opcode;
  result_data1 <= result_data_s1;
  result_data2 <= result_data_s2;
  result_data3 <= result_data_s3;
  
  --result_address <= result_address_s;
  clk_s <= clk;
  EU_OPCODE(0 to 2) <= opcode_s ( 5 to 7);
  CU_OPCODE(0 to 4) <= opcode_s ( 0 to 4);
process (clk)
-- variable declarations
  
begin   
   

    -- begin execution on even pipe depending on the instruction and opcode


--  if clk'event and  clk ='1' 
--  then 
    if sel ='0'
    then
  
      if EU_OPCODE = "101" --FXU
      then 
        enable_fxu <='1';
         RegWr_ins0_ep_o <= transport RegWr_ins0_ep after clk_period*2 ns;
         MemRd_ins0_ep_o <= transport MemRd_ins0_ep after clk_period*2 ns;
         MemWr_ins0_ep_o <= transport MemWr_ins0_ep after clk_period*2 ns;
    
      else
      enable_fxu <='0';
      end if;

      if EU_OPCODE = "011" --FPU
      then 
        enable_fpu <='1';
          RegWr_ins0_ep_o <= transport RegWr_ins0_ep after clk_period*4 ns;
         MemRd_ins0_ep_o <= transport MemRd_ins0_ep after clk_period*4 ns;
         MemWr_ins0_ep_o <= transport MemWr_ins0_ep after clk_period*4 ns;
           
      else enable_fpu <='0';
      end if;

      if EU_OPCODE = "111" --BYTEU
      then 
        enable_byte <='1';
          RegWr_ins0_ep_o <= transport RegWr_ins0_ep after clk_period*1 ns;
         MemRd_ins0_ep_o <= transport MemRd_ins0_ep after clk_period*1 ns;
         MemWr_ins0_ep_o <= transport MemWr_ins0_ep after clk_period*1 ns;
          
      else enable_byte <='0';
      end if;
      
      else
     -- enable_fxu <= '0';
     -- enable_fpu <= '0';
     -- enable_byte <= '0';
      end if;
  
   -- end if; -- clk ends
  end process;
end behave;