library IEEE;

use IEEE.std_logic_1164.all;


entity frwd_mux is 

 port(
		fwe1	: in	std_logic_vector ( 0 to 127);
		fwe2	: in	std_logic_vector ( 0 to 127);
		fwe3	: in	std_logic_vector ( 0 to 127);
		fwo1	: in	std_logic_vector ( 0 to 127);
		fwo2	: in	std_logic_vector ( 0 to 127);
		fwo3	: in	std_logic_vector ( 0 to 127);
		rf	  : in	std_logic_vector ( 0 to 127);
		frwd_cntrl	: in	bit_vector(0 to 2);
		regout : out	std_logic_vector (0 to 127)
	);

end frwd_mux;


architecture behave of frwd_mux is

begin

  with frwd_cntrl select
  regout <= fwe1 when "000",
            fwe2 when "001",
            fwe3 when "010",
            fwo1 when "011",
            fwo2 when "100",
            fwo3 when "101",
            rf   when "110",
            (regout'range => 'Z' ) when "111";
end behave;