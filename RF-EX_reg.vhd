LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY RF_EX IS
  PORT(
    ra1, rb1, rc1, rt1 : IN STD_LOGIC_VECTOR(0 TO 127);
    imm7_1    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    ra2, rb2, rc2, rt2 : IN STD_LOGIC_VECTOR(0 TO 127);
    imm7_2    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : IN STD_LOGIC_VECTOR(0 TO 15);
    
    opcode_0 : IN STD_LOGIC_VECTOR(0 TO 8);
    opcode_1 : IN STD_LOGIC_VECTOR(0 TO 8);
    
    RegWr_ins0  : IN BIT;
    MemRd_ins0  : IN BIT;
    MemWr_ins0  : IN BIT;
    
    RegWr_ins1  : IN BIT;
    MemRd_ins1  : IN BIT;
    MemWr_ins1  : IN BIT;
    
    IMM_SEL1  : in BIT_VECTOR ( 0 to 1);
    IMM_SEL2  : in BIT_VECTOR( 0 to 1);
    
    PC : IN STD_LOGIC_VECTOR(0 TO 31);
    
    ra1_PR3, rb1_PR3, rc1_PR3, rt1_PR3 : OUT STD_LOGIC_VECTOR(0 TO 127);
    imm7_1_PR3    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_PR3    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_PR3   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_PR3   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_PR3, rb2_PR3, rc2_PR3, rt2_PR3 : OUT STD_LOGIC_VECTOR(0 TO 127);
    imm7_2_PR3    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2_PR3    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2_PR3   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2_PR3   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    opcode_0_PR3 : OUT STD_LOGIC_VECTOR(0 TO 8);
    opcode_1_PR3 : OUT STD_LOGIC_VECTOR(0 TO 8);
    
    RegWr_ins0_PR3  : OUT BIT;
    MemRd_ins0_PR3  : OUT BIT;
    MemWr_ins0_PR3  : OUT BIT;
    
    RegWr_ins1_PR3  : OUT BIT;
    MemRd_ins1_PR3  : OUT BIT;
    MemWr_ins1_PR3  : OUT BIT;
    
    IMM_SEL1_PR3  : OUT BIT_VECTOR ( 0 to 1);
    IMM_SEL2_PR3  : OUT BIT_VECTOR ( 0 to 1);
    
    PC_PR3 : OUT STD_LOGIC_VECTOR(0 TO 31);    
    
    reset : IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
END RF_EX;

ARCHITECTURE behave OF RF_EX IS
BEGIN
  PROCESS(clk)
  BEGIN
  IF clk'event AND clk = '1' THEN -- clk
    IF reset = '1' THEN
      
      opcode_0_PR3 <= (OTHERS => 'Z');  
      ra1_PR3 <= (OTHERS => 'Z');
      rb1_PR3 <= (OTHERS => 'Z');
      rc1_PR3 <= (OTHERS => 'Z');
      rt1_PR3 <= (OTHERS => 'Z');
      imm7_1_PR3   <= (OTHERS => 'Z');
      imm8_1_PR3   <= (OTHERS => 'Z');
      imm10_1_PR3  <= (OTHERS => 'Z');
      imm16_1_PR3  <= (OTHERS => 'Z');
      
      opcode_1_PR3 <= (OTHERS => 'Z');  
      ra2_PR3 <= (OTHERS => 'Z');
      rb2_PR3 <= (OTHERS => 'Z');
      rc2_PR3 <= (OTHERS => 'Z');
      rt2_PR3 <= (OTHERS => 'Z');
      imm7_2_PR3   <= (OTHERS => 'Z');
      imm8_2_PR3   <= (OTHERS => 'Z');
      imm10_2_PR3  <= (OTHERS => 'Z');
      imm16_2_PR3  <= (OTHERS => 'Z');
      
      RegWr_ins0_PR3  <= '0';
      MemRd_ins0_PR3  <= '0';
      MemWr_ins0_PR3  <= '0';
                   
      RegWr_ins1_PR3  <= '0';
      MemRd_ins1_PR3  <= '0';
      MemWr_ins1_PR3  <= '0';
                   
      IMM_SEL1_PR3  <= "00";
      IMM_SEL2_PR3  <= "00";

      
      PC_PR3 <= (OTHERS => 'Z');
    
      ELSE
    
      opcode_0_PR3 <= opcode_0;  
      ra1_PR3 <= ra1;
      rb1_PR3 <= rb1;
      rc1_PR3 <= rc1;
      rt1_PR3 <= rt1;
      
      imm7_1_PR3   <= imm7_1;
      imm8_1_PR3   <= imm8_1;
      imm10_1_PR3  <= imm10_1;
      imm16_1_PR3  <= imm16_1;
      
      opcode_1_PR3 <= opcode_1;  
      ra2_PR3 <= ra2;
      rb2_PR3 <= rb2;
      rc2_PR3 <= rc2;
      rt2_PR3 <= rt2;
      
      imm7_2_PR3   <= imm7_2;
      imm8_2_PR3   <= imm8_2;
      imm10_2_PR3  <= imm10_2;
      imm16_2_PR3  <= imm16_2;    
      
      RegWr_ins0_PR3  <= RegWr_ins0;
      MemRd_ins0_PR3  <= MemRd_ins0;
  	   MemWr_ins0_PR3  <= MemWr_ins0;
                    
      RegWr_ins1_PR3  <= RegWr_ins1;
      MemRd_ins1_PR3  <= MemRd_ins1;
      MemWr_ins1_PR3  <= MemWr_ins1;
      
      IMM_SEL1_PR3  <= IMM_SEL1;
      IMM_SEL2_PR3  <= IMM_SEL2;
      
      PC_PR3 <= PC;
      
    END IF;
  END IF;
END PROCESS;
END behave;