LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE IEEE.std_logic_textio.ALL;
USE STD.textio.ALL;

ENTITY tb_decoder IS
END tb_decoder;

ARCHITECTURE decoder_tb OF tb_decoder IS
  
  FILE logger : TEXT OPEN WRITE_MODE IS "transcript_decoder.log";

  COMPONENT decoder2
    PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    opcode_0, opcode_1 : OUT STD_LOGIC_VECTOR(0 TO 8);
    
    ra1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_2    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    --new signals added
  RegWr_ins0  : OUT BIT;  
  MemRd_ins0  : OUT BIT;  
  MemWr_ins0  : OUT BIT;  
                        
  RegWr_ins1  : OUT BIT;  
  MemRd_ins1  : OUT BIT;  
  MemWr_ins1  : OUT BIT;  
                        
  IMM_SEL1  : OUT BIT_vector ( 0 to 1);
  IMM_SEL2  : OUT BIT_vector ( 0 to 1);
  
  delayILB :  OUT STD_LOGIC;
    
    enable : IN STD_ULOGIC :='1';
    clk: IN STD_ULOGIC :='0');
  END COMPONENT;
  
  SIGNAL inst_0, inst_1 : STD_LOGIC_VECTOR(0 TO 31) := (others => 'Z');
  SIGNAL opcode_0, opcode_1 : STD_LOGIC_VECTOR(0 TO 8) := (others => 'Z');
  SIGNAL ra1_addr  : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL rb1_addr  : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL rc1_addr  : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL rt1_addr  : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL imm7_1    : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL imm8_1    : STD_LOGIC_VECTOR(0 TO 7):= (others => 'Z');
  SIGNAL imm10_1   : STD_LOGIC_VECTOR(0 TO 9):= (others => 'Z');
  SIGNAL imm16_1   : STD_LOGIC_VECTOR(0 TO 15):= (others => 'Z');
  
  SIGNAL ra2_addr  : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL rb2_addr  : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL rc2_addr  : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL rt2_addr  : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL imm7_2    : STD_LOGIC_VECTOR(0 TO 6):= (others => 'Z');
  SIGNAL imm8_2    : STD_LOGIC_VECTOR(0 TO 7):= (others => 'Z');
  SIGNAL imm10_2   : STD_LOGIC_VECTOR(0 TO 9):= (others => 'Z');
  SIGNAL imm16_2   : STD_LOGIC_VECTOR(0 TO 15):= (others => 'Z');
  
  signal RegWr_ins0  :  BIT;
  signal MemRd_ins0  :  BIT;
  signal MemWr_ins0  :  BIT;
                     
  signal RegWr_ins1  :  BIT;
  signal MemRd_ins1  :  BIT;
  signal MemWr_ins1  :  BIT;
  signal IMM_SEL1   :  BIT_vector ( 0 to 1);
  signal IMM_SEL2   :  BIT_vector ( 0 to 1);
  SIGNAL enable : STD_ULOGIC := 'Z';
  SIGNAL clk: STD_ULOGIC := '0';
  
  PROCEDURE print(inst_0 : STD_LOGIC_VECTOR(0 TO 31);
                  inst_1 : STD_LOGIC_VECTOR(0 TO 31);
                  opcode_0 : STD_LOGIC_VECTOR(0 TO 8);
                  opcode_1 : STD_LOGIC_VECTOR(0 TO 8)
                  ) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
  BEGIN
    swrite(line_out, "inst_0 = ");
    write(line_out, inst_0);
    writeline(logger, line_out);
    swrite(line_out, "inst_1 = ");
    write(line_out, inst_1);
    writeline(logger, line_out);
    swrite(line_out, "opcode_0 = ");
    write(line_out, opcode_0);
    writeline(logger, line_out);
    swrite(line_out, "opcode_1 = ");
    write(line_out, opcode_1);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;
  
  -- definition of a clock period
  CONSTANT clk_period : time := 10 ns;
  
BEGIN
  
  -- component instantiation
  DUT: decoder2 PORT MAP (
    inst_0 => inst_0,
    inst_1 => inst_1,
    opcode_0 => opcode_0,
    opcode_1 => opcode_1,
    ra1_addr => ra1_addr,
    rb1_addr => rb1_addr,
    rc1_addr => rc1_addr,
    rt1_addr => rt1_addr,
    imm7_1   => imm7_1,
    imm8_1   => imm8_1,
    imm10_1  => imm10_1,
    imm16_1  => imm16_1,
    ra2_addr => ra2_addr,
    rb2_addr => rb2_addr,
    rc2_addr => rc2_addr,
    rt2_addr => rt2_addr,
    imm7_2   => imm7_2,
    imm8_2   => imm8_2,
    imm10_2  => imm10_2,
    imm16_2  => imm16_2,
    RegWr_ins0 => RegWr_ins0,
    MemRd_ins0 => MemRd_ins0,
    MemWr_ins0 => MemWr_ins0,   
    RegWr_ins1 => RegWr_ins1,
    MemRd_ins1 => MemRd_ins1,
    MemWr_ins1 => MemWr_ins1,
    IMM_SEL1 => IMM_SEL1,
    IMM_SEL2 => IMM_SEL2,
    clk => clk,
    enable => enable);
    
    CLK_PROCESS : PROCESS
    BEGIN
		  clk <= '0';
		  WAIT FOR clk_period/2;
		  
		  clk <= '1';
		  WAIT FOR clk_period/2;
    END PROCESS;
              
    SIMULATION : PROCESS
    BEGIN
      
 --   WAIT FOR clk_period;
    
    enable <= '1';
  --  WAIT FOR clk_period;
  --  inst_0 <= "00011001000001110010010111001110";
  --  inst_1 <= "00111000100011001111110011010101";
    print(inst_0, inst_1, opcode_0, opcode_1);
 -- WAIT FOR 2 ns;
 --   enable <= '0';
    --WAIT FOR clk_period;
  --  inst_0 <= "00100011100010101010000110101001"; --stqr "010000101"
  --  inst_1 <= "00100000100011110000110011101111"; --stqa "001110101"
    --print(inst_0, inst_1, opcode_0, opcode_1);
    --WAIT FOR 10 ns;
     WAIT FOR 12 ns;
    
    inst_0 <= "01001001001000000000000110101001";--"010111010"
    inst_1 <= "00011000000000000000110011101111";--"000011010"
     WAIT FOR 10 ns;
 inst_0 <= (others => 'Z');--"010111010"
 inst_1 <= (others => 'Z');--"000011010"
    
   inst_0 <= "00110010000000000000000110101001";-- "000010011"
       inst_1 <= "00110000000101111000000110101001";-- "010000101" --bra
  WAIT FOR 10 ns;
    inst_0 <= "00011001000001110010010111001110";
    inst_1 <= "00111000100011001111110011010101";
    WAIT FOR 10 ns;
    inst_0 <= "01001001001000000000000110101001";--"010111010"
   inst_1 <= "00011000000000000000110011101111";--"000011010"
    WAIT FOR 10 ns;
--     inst_0 <= "00100011100010101010000110101001"; --stqr "010000101"
--    inst_1 <= "00100000100011110000110011101111"; --stqa "001110101"
--    --print(inst_0, inst_1, opcode_0, opcode_1);
    --WAIT FOR 10 ns;
     WAIT FOR 10 ns;
  
    REPORT "Test Completed";
    --enable <= '0';
    WAIT;
    
END PROCESS;
END;
    
    