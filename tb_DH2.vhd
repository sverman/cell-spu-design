library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity test_DH2 is
end test_DH2;
architecture test_datahzd of test_DH2 is
component DH2 is
port (
    opcode_0_DH  : IN STD_LOGIC_VECTOR(0 TO 8);   
    ra0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_0_DH    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_0_DH    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_0_DH   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_0_DH   : IN STD_LOGIC_VECTOR(0 TO 15);
    RegWr_ins0_DH  : IN BIT;
    MemRd_ins0_DH  : IN BIT;
    MemWr_ins0_DH  : IN BIT;
    
    opcode_1_DH  : IN STD_LOGIC_VECTOR(0 TO 8);
    ra1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_DH    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_DH    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_DH   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_DH   : IN STD_LOGIC_VECTOR(0 TO 15);    
    RegWr_ins1_DH  : IN BIT;
    MemRd_ins1_DH  : IN BIT;
    MemWr_ins1_DH  : IN BIT;
    
    PC_DH : IN STD_LOGIC_VECTOR(0 TO 31);
    
   
    opcode_0_ISS  : OUT STD_LOGIC_VECTOR(0 TO 8);    
    ra0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_0_ISS    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_0_ISS    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_0_ISS   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_0_ISS   : OUT STD_LOGIC_VECTOR(0 TO 15);
    RegWr_ins0_ISS  : OUT BIT;
    MemRd_ins0_ISS  : OUT BIT;
    MemWr_ins0_ISS  : OUT BIT;
    
    
    opcode_1_ISS  : OUT STD_LOGIC_VECTOR(0 TO 8);
    ra1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_ISS    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_ISS    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_ISS   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_ISS   : OUT STD_LOGIC_VECTOR(0 TO 15);
   
    RegWr_ins1_ISS  : OUT BIT;
    MemRd_ins1_ISS  : OUT BIT;
    MemWr_ins1_ISS  : OUT BIT;
    
    PC_ISS : OUT STD_LOGIC_VECTOR(0 TO 31);
    reset : in std_logic;
    firstInstr : IN BIT;
    enable : IN STD_LOGIC;
    clk: IN STD_LOGIC
  );

end component;

  
    signal opcode_0_DH  : STD_LOGIC_VECTOR(0 TO 8);   
    signal ra0_addr_DH  : STD_LOGIC_VECTOR(0 TO 6);
    signal rb0_addr_DH  : STD_LOGIC_VECTOR(0 TO 6);
    signal rc0_addr_DH  : STD_LOGIC_VECTOR(0 TO 6);
    signal rt0_addr_DH  : STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_0_DH    : STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_0_DH    : STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_0_DH   : STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_0_DH   : STD_LOGIC_VECTOR(0 TO 15);
    signal RegWr_ins0_DH  : BIT;
    signal MemRd_ins0_DH  : BIT;
    signal MemWr_ins0_DH  : BIT;
    
    signal opcode_1_DH  : STD_LOGIC_VECTOR(0 TO 8);
    signal ra1_addr_DH  : STD_LOGIC_VECTOR(0 TO 6);
    signal rb1_addr_DH  : STD_LOGIC_VECTOR(0 TO 6);
    signal rc1_addr_DH  : STD_LOGIC_VECTOR(0 TO 6);
    signal rt1_addr_DH  : STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_1_DH    : STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_DH    : STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_DH   : STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_DH   : STD_LOGIC_VECTOR(0 TO 15);    
    signal RegWr_ins1_DH  : BIT;
    signal MemRd_ins1_DH  : BIT;
    signal MemWr_ins1_DH  : BIT;
    
    signal PC_DH : STD_LOGIC_VECTOR(0 TO 31);
    
   
    signal opcode_0_ISS  :  STD_LOGIC_VECTOR(0 TO 8);    
    signal ra0_addr_ISS  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rb0_addr_ISS  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rc0_addr_ISS  :  STD_LOGIC_VECTOR(0 TO 6);
    signal rt0_addr_ISS  :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_0_ISS    :  STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_0_ISS    :  STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_0_ISS   :  STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_0_ISS   :  STD_LOGIC_VECTOR(0 TO 15);
    signal RegWr_ins0_ISS  :  BIT;
    signal MemRd_ins0_ISS  :  BIT;
    signal MemWr_ins0_ISS  :  BIT;
    
    
    signal opcode_1_ISS  : STD_LOGIC_VECTOR(0 TO 8);
    signal ra1_addr_ISS  : STD_LOGIC_VECTOR(0 TO 6);
    signal rb1_addr_ISS  : STD_LOGIC_VECTOR(0 TO 6);
    signal rc1_addr_ISS  : STD_LOGIC_VECTOR(0 TO 6);
    signal rt1_addr_ISS  : STD_LOGIC_VECTOR(0 TO 6);
    signal imm7_1_ISS    : STD_LOGIC_VECTOR(0 TO 6);
    signal imm8_1_ISS    : STD_LOGIC_VECTOR(0 TO 7);
    signal imm10_1_ISS   : STD_LOGIC_VECTOR(0 TO 9);
    signal imm16_1_ISS   : STD_LOGIC_VECTOR(0 TO 15);
   
    signal RegWr_ins1_ISS  : BIT;
    signal MemRd_ins1_ISS  : BIT;
    signal MemWr_ins1_ISS  :  BIT;
    
    signal PC_ISS :  STD_LOGIC_VECTOR(0 TO 31);
    signal reset :  std_logic;
    signal firstInstr :  BIT;
    signal enable :  STD_LOGIC;
    signal clk:  STD_LOGIC;


   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DH2 PORT MAP
(
 
 opcode_0_DH => opcode_0_DH,
 ra0_addr_DH => ra0_addr_DH,
 rb0_addr_DH => rb0_addr_DH,
 rc0_addr_DH =>  rc0_addr_DH,
 rt0_addr_DH => rt0_addr_DH,
 imm7_0_DH   => imm7_0_DH,
 imm8_0_DH   => imm8_0_DH,
 imm10_0_DH  =>  imm10_0_DH,
 imm16_0_DH  => imm16_0_DH,
 RegWr_ins0_DH => RegWr_ins0_DH,
 MemRd_ins0_DH => MemRd_ins0_DH,
 MemWr_ins0_DH => MemWr_ins0_DH,
             
 opcode_1_DH => opcode_1_DH,
 ra1_addr_DH => ra1_addr_DH,
 rb1_addr_DH => rb1_addr_DH,
 rc1_addr_DH => rc1_addr_DH,
 rt1_addr_DH => rt1_addr_DH,
 imm7_1_DH   => imm7_1_DH,
 imm8_1_DH   => imm8_1_DH,
 imm10_1_DH  => imm10_1_DH,
 imm16_1_DH  => imm16_1_DH,
 RegWr_ins1_DH => RegWr_ins1_DH,
 MemRd_ins1_DH => MemRd_ins1_DH,
 MemWr_ins1_DH => MemWr_ins1_DH,
 PC_DH => PC_DH,
 
              
 opcode_0_ISS => opcode_0_ISS,
 ra0_addr_ISS => ra0_addr_ISS,
 rb0_addr_ISS => rb0_addr_ISS,
 rc0_addr_ISS => rc0_addr_ISS,
 rt0_addr_ISS => rt0_addr_ISS,
 imm7_0_ISS => imm7_0_ISS,
 imm8_0_ISS => imm8_0_ISS,
 imm10_0_ISS =>  imm10_0_ISS, 
 imm16_0_ISS => imm16_0_ISS,
 RegWr_ins0_ISS => RegWr_ins0_ISS,
 MemRd_ins0_ISS => MemRd_ins0_ISS,
 MemWr_ins0_ISS => MemWr_ins0_ISS,
             
 opcode_1_ISS => opcode_1_ISS,
 ra1_addr_ISS =>  ra1_addr_ISS, 
 rb1_addr_ISS => rb1_addr_ISS,
 rc1_addr_ISS => rc1_addr_ISS,
 rt1_addr_ISS => rt1_addr_ISS, 
 imm7_1_ISS => imm7_1_ISS,
 imm8_1_ISS => imm8_1_ISS,
 imm10_1_ISS => imm10_1_ISS,
 imm16_1_ISS => imm16_1_ISS,      
 RegWr_ins1_ISS => RegWr_ins1_ISS,
 MemRd_ins1_ISS => MemRd_ins1_ISS,
 MemWr_ins1_ISS => MemWr_ins1_ISS,

  PC_ISS => PC_ISS,
  reset => reset,
  firstInstr => firstInstr,
  enable => enable,
  clk => clk

 
 );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
   end process;
  stim_proc: process
 begin		
   
 --  wait for 3 ns;
 opcode_0_DH <= "000101010";
 ra0_addr_DH <= "1010101";
 rb0_addr_DH <= "0001110";
 rc0_addr_DH <= "0010010";
 rt0_addr_DH <= "0001011";
 imm7_0_DH <= "0001010";
 imm8_0_DH  <= "00010101";
 imm10_0_DH <= "0001010111";
 imm16_0_DH <= "0001010111010101";
 RegWr_ins0_DH <= '1';
 MemRd_ins0_DH <= '0';
 MemWr_ins0_DH <= '0';
             
 opcode_1_DH <= "000101001";
 ra1_addr_DH <= "0001111";
 rb1_addr_DH <= "0001010";
 rc1_addr_DH <= "0001000";
 rt1_addr_DH <= "0011000";
 imm7_1_DH <= "0001010";
 imm8_1_DH  <= "00010101";
 imm10_1_DH <= "0001010101";
 imm16_1_DH <= "0001010100010101";
 RegWr_ins1_DH <= '1';
 MemRd_ins1_DH <= '0';
 MemWr_ins1_DH <= '1';
 PC_DH <= "00010101000101010001010100010101";
 reset <= '0';
 enable <= '1';
 firstInstr <= '1';
    wait for clk_period;
    
    opcode_0_DH <= "000101010";
 ra0_addr_DH <= "1010101";
 rb0_addr_DH <= "0001110";
 rc0_addr_DH <= "0010010";
 rt0_addr_DH <= "0001011";
 imm7_0_DH <= "0001010";
 imm8_0_DH  <= "00010101";
 imm10_0_DH <= "0001010111";
 imm16_0_DH <= "0001010111010101";
 RegWr_ins0_DH <= '1';
 MemRd_ins0_DH <= '0';
 MemWr_ins0_DH <= '0';
             
 opcode_1_DH <= "000101001";
 ra1_addr_DH <= "0001111";
 rb1_addr_DH <= "0001010";
 rc1_addr_DH <= "0001000";
 rt1_addr_DH <= "0011000";
 imm7_1_DH <= "0001010";
 imm8_1_DH  <= "00010101";
 imm10_1_DH <= "0001010101";
 imm16_1_DH <= "0001010100010101";
 RegWr_ins1_DH <= '1';
 MemRd_ins1_DH <= '0';
 MemWr_ins1_DH <= '1';
 PC_DH <= "00010101000101010001010100010101";
 reset <= '0';
 enable <= '1';
 firstInstr <= '1';
   wait for clk_period;
 opcode_0_DH <= "000101010";
 ra0_addr_DH <= "1010101";
 rb0_addr_DH <= "0001110";
 rc0_addr_DH <= "0010010";
 rt0_addr_DH <= "0001011";
 imm7_0_DH <= "0001010";
 imm8_0_DH  <= "00010101";
 imm10_0_DH <= "0001010111";
 imm16_0_DH <= "0001010111010101";
 RegWr_ins0_DH <= '1';
 MemRd_ins0_DH <= '0';
 MemWr_ins0_DH <= '0';
             
 opcode_1_DH <= "000101001";
 ra1_addr_DH <= "0001111";
 rb1_addr_DH <= "0001010";
 rc1_addr_DH <= "0001000";
 rt1_addr_DH <= "0011000";
 imm7_1_DH <= "0001010";
 imm8_1_DH  <= "00010101";
 imm10_1_DH <= "0001010101";
 imm16_1_DH <= "0001010100010101";
 RegWr_ins1_DH <= '1';
 MemRd_ins1_DH <= '0';
 MemWr_ins1_DH <= '1';
 PC_DH <= "00010101000101010001010100010101";
 reset <= '0';
 enable <= '1';
 firstInstr <= '1';
   
--opcode_1_DH <= (others => 'Z');
--opcode_0_DH <= (others => 'Z');  
      wait;
   end process;
END;
