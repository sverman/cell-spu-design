LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY dec_mux IS
  PORT 
  (
    mux_in1 : IN STD_LOGIC_VECTOR(0 TO 127);
    mux_in2 : IN STD_LOGIC_VECTOR(0 TO 9);
    mux_in3 : IN STD_LOGIC_VECTOR(0 TO 15);
    mux_out : OUT STD_LOGIC_VECTOR(0 TO 127);
    
    sel: IN BIT_VECTOR ( 0 to 1):="00";
    clk: IN STD_LOGIC
    );
END dec_mux;

ARCHITECTURE mux_dec OF dec_mux IS
BEGIN
 process(mux_in1,mux_in2,mux_in3,sel )
 begin
 --if clk'event then
CASE sel is
when "00" =>
    mux_out <= mux_in1;--rc
  when "01" =>
    mux_out ( 0 to 9 ) <=   mux_in2 (0 to 9);
    mux_out (10 to 127) <=  (others => 'Z');--immv10
  when "10" =>
    mux_out ( 0 to 15 ) <=   mux_in3 (0 to 15);
    mux_out (16 to 127) <=  (others => 'Z');--immv16
 when others =>
    mux_out (0 to 127) <=  (others => 'Z');
end case;
-- END if;
end process;
end;
