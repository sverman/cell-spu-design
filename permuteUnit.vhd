LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY permute IS
  PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_ULOGIC);
END permute;

ARCHITECTURE behave OF permute IS
  signal result_s : STD_LOGIC_VECTOR(0 TO 127);
BEGIN

 PROCESS(clk)
  -- declaration of variables
  VARIABLE ra_uns : UNSIGNED(0 TO 127);
  VARIABLE rb_uns : UNSIGNED(0 TO 127);
  VARIABLE rc_uns : UNSIGNED(0 TO 127);
  VARIABLE rt_uns : UNSIGNED(0 TO 127);
  VARIABLE z_uns : UNSIGNED(0 TO 0);
  
  VARIABLE p : UNSIGNED(0 TO 127);
  VARIABLE t : UNSIGNED(0 TO 3);
  VARIABLE q : UNSIGNED(0 TO 7);
  VARIABLE r : UNSIGNED(0 TO 15);
  VARIABLE b : INTEGER;
  VARIABLE s : INTEGER;
  
  BEGIN
  IF clk'event AND clk = '1' THEN -- clk
   
  -- initialize
  ra_uns := UNSIGNED(ra);
  rb_uns := UNSIGNED(rb);
  rc_uns := UNSIGNED(rc);
  rt_uns := UNSIGNED(rt);
  z_uns(0) := '0';

  -- select desired operation
  CASE opcode IS

   -- (1) shlqbi (Shift Left Quadword by bits)
  WHEN "00001" =>
  s := TO_INTEGER(rb_uns(29 TO 31));       
      
  FOR b IN 0 TO 127 LOOP
    IF (b + s) < 128 THEN
      p(b) := ra_uns(b + s);
    ELSE
      p(b) := '0';
    END IF;
  END LOOP;
  rt_uns(0 TO 127) := p(0 TO 127);        
          
  -- (2) rotqbi (Rotate Quadword by bits)
  WHEN "00010" =>
  s := TO_INTEGER(rb_uns(29 TO 31));        
      
  FOR b IN 0 TO 127 LOOP
    IF (b + s) < 128 THEN
      p(b) := ra_uns(b + s);
    ELSE
      p(b) := ra_uns(b + (s - 128));
    END IF;
  END LOOP;
  rt_uns(0 TO 127) := p(0 TO 127);
  
  -- (3) gb (Gather Bits from Words)
  WHEN "00011" =>
  t := X"0";
  FOR b IN 0 TO 3 LOOP
    t(b) := ra_uns(((b+1) * 32) - 1);
  END LOOP;
  rt_uns(0 TO 127) := X"00000000000000000000000000000000";
  rt_uns(28 TO 31) := t(0 TO 3);

  -- (4) gbh (Gather Bits from HalfWords)
  WHEN "00100" =>
  q := X"00";
  FOR b IN 0 TO 7 LOOP
    q(b) := ra_uns(((b+1) * 16) - 1);
  END LOOP;
  rt_uns(0 TO 127) := X"00000000000000000000000000000000";
  rt_uns(24 TO 31) := q(0 TO 7);

  -- (5) gbb (Gather Bits from Bytes)
  WHEN "00101" =>
  r := X"0000";
  FOR b IN 0 TO 15 LOOP
    r(b) := ra_uns(((b+1) * 8) - 1);
  END LOOP;
  rt_uns(0 TO 127) := X"00000000000000000000000000000000";
  rt_uns(16 TO 31) := r(0 TO 15);
    
  -- others 
  WHEN OTHERS => rt_uns := (OTHERS => 'X');
  END CASE;

  -- set zero bit if result equals zero 
  IF TO_INTEGER(rt_uns) = 0 THEN
    z_uns(0) := '1';
  ELSE
    z_uns(0) := '0';
  END IF; 

  -- assign variables to output signals
   IF enable = '1' THEN
  result_s <= STD_LOGIC_VECTOR(rt_uns);
  zero <= z_uns(0); 
  
  ELSE
    result_s <= (OTHERS => 'Z');
  END IF; -- enable
  result <=  result_s ;
  END IF;
  END PROCESS; 

END behave;