LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY decoder2 IS
  PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    opcode_0, opcode_1 : OUT STD_LOGIC_VECTOR(0 TO 8):=(OTHERS => 'Z');
    
    ra1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    rb1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    rc1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    rt1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    imm7_1    : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    imm8_1    : OUT STD_LOGIC_VECTOR(0 TO 7):=(OTHERS => 'Z');
    imm10_1   : OUT STD_LOGIC_VECTOR(0 TO 9):=(OTHERS => 'Z');
    imm16_1   : OUT STD_LOGIC_VECTOR(0 TO 15):=(OTHERS => 'Z');
    
    ra2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    rb2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    rc2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    rt2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    imm7_2    : OUT STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    imm8_2    : OUT STD_LOGIC_VECTOR(0 TO 7):=(OTHERS => 'Z');
    imm10_2   : OUT STD_LOGIC_VECTOR(0 TO 9):=(OTHERS => 'Z');
    imm16_2   : OUT STD_LOGIC_VECTOR(0 TO 15):=(OTHERS => 'Z');
    
   -- IMMV_DEC1  : OUT STD_LOGIC_VECTOR(0 TO 127); -- signal for multiplexed immediate output    
   -- IMMV_DEC2  : OUT STD_LOGIC_VECTOR(0 TO 127);
    -- Register R/W & Memory R/W Signals
    
    RegWr_ins0  : OUT BIT;
    MemRd_ins0  : OUT BIT;
    MemWr_ins0  : OUT BIT;
    
    RegWr_ins1  : OUT BIT;
    MemRd_ins1  : OUT BIT;
    MemWr_ins1  : OUT BIT;
    
    IMM_SEL1  : OUT BIT_VECTOR (0 to 1) ; -- 00 for rc -- 01 immv10 -- 02 immv16
    IMM_SEL2  : OUT BIT_VECTOR  (0 to 1) ;
    
    delayILB : OUT STD_LOGIC :='1'; -- active low
    enable : IN STD_ULOGIC;
     clk: IN STD_ULOGIC);
    END decoder2;

ARCHITECTURE behave OF decoder2 IS
 SIGNAL in_0 : STD_LOGIC_VECTOR(0 TO 31) := (others => 'Z');
 SIGNAL in_1 : STD_LOGIC_VECTOR(0 TO 31) := (others => 'Z');
  signal issue0, issue1 : integer :=0;
type state_type is (s0,s1,s2,s3);  --type of state machine.
signal current_s, next_s: state_type := s0;  --current and next state declaration.

---BEGIN
 --PROCESS(inst_0,inst_1,clk)
    -- Declaration of Variables

  --  VARIABLE op_0 : STD_LOGIC_VECTOR(0 TO 8);
    signal op_0 : STD_LOGIC_VECTOR(0 TO 8):=(OTHERS => 'Z');
    signal op_1 : STD_LOGIC_VECTOR(0 TO 8):=(OTHERS => 'Z');
    signal buff_op_0 : STD_LOGIC_VECTOR(0 TO 8):=(OTHERS => 'Z');
    signal buff_op_1 : STD_LOGIC_VECTOR(0 TO 8):=(OTHERS => 'Z');    
    
    
    signal ra_0  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal rb_0  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal rc_0  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal rt_0  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal i10_0 : STD_LOGIC_VECTOR(0 TO 9):=(OTHERS => 'Z');
    signal i16_0 : STD_LOGIC_VECTOR(0 TO 15):=(OTHERS => 'Z');
    
    signal ra_1  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal rb_1  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal rc_1  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal rt_1  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z'); 
    signal i10_1 : STD_LOGIC_VECTOR(0 TO 9):=(OTHERS => 'Z');
    signal i16_1 : STD_LOGIC_VECTOR(0 TO 15):=(OTHERS => 'Z');
    
    
    signal buff_ra_0  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal buff_rb_0  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal buff_rc_0  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal buff_rt_0  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal buff_i10_0 : STD_LOGIC_VECTOR(0 TO 9):=(OTHERS => 'Z');
    signal buff_i16_0 : STD_LOGIC_VECTOR(0 TO 15):=(OTHERS => 'Z');
    
    signal buff_ra_1  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal buff_rb_1  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal buff_rc_1  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z');
    signal buff_rt_1  : STD_LOGIC_VECTOR(0 TO 6):=(OTHERS => 'Z'); 
    signal buff_i10_1 : STD_LOGIC_VECTOR(0 TO 9):=(OTHERS => 'Z');
    signal buff_i16_1 : STD_LOGIC_VECTOR(0 TO 15):=(OTHERS => 'Z');
    
    signal buff_vRegWr_ins0 : BIT;
    signal buff_vMemRd_ins0 : BIT;
    signal buff_vMemWr_ins0 : BIT;
    
    signal buff_vRegWr_ins1 : BIT;
    signal buff_vMemRd_ins1 : BIT;
    signal buff_vMemWr_ins1 : BIT;
   
    signal buff_imm_selv1 : BIT_VECTOR (0 to 1);
    signal buff_imm_selv2 : BIT_VECTOR (0 to 1);
    --signal IMMV_DECv : STD_LOGIC_VECTOR(0 TO 127);
    
    -- Flags
    signal vRegWr_ins0 : BIT;
    signal vMemRd_ins0 : BIT;
    signal vMemWr_ins0 : BIT;
    
    signal vRegWr_ins1 : BIT;
    signal vMemRd_ins1 : BIT;
    signal vMemWr_ins1 : BIT;
   
    signal imm_selv1 : BIT_VECTOR (0 to 1);
    signal imm_selv2 : BIT_VECTOR (0 to 1);
    
    begin
      in_0 <= inst_0;
      in_1 <= inst_1;
      
    PROCESS(clk,inst_0,inst_1,in_0,in_1,current_s,next_s)
        
  BEGIN
             
        
       
             
  IF (rising_edge(clk)) THEN -- clk
    
        
           
--      
      -- Check Instruction 1
      --noop
      IF in_0(0 TO 10) = "00000000000" THEN -- noop
        op_0 <= "000000000";
        ra_0 <= "0000000";
        rb_0 <= "0000000";
        rc_0 <= "0000000";
        rt_0 <= "0000000";
        i10_0 <= (OTHERS => '0');
        i16_0 ( 0 to 15) <= (OTHERS => '0');
        
      --IMMV_DECv <= (OTHERS => 'X');
      
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Check for FXU
      ELSIF in_0(0 TO 10) = "00011000000" THEN -- a
        op_0 <= "000011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 ( 0 to 15) <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
        
      ELSIF in_0(0 TO 10) = "00011001000" THEN  -- ah
        op_0 <= "000101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00011000001" THEN -- and
        op_0 <= "000111010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001000001" THEN -- or
        op_0 <= "001001010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00011001001" THEN -- nand
        op_0 <= "001011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001001001" THEN -- nor
        op_0 <= "001101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
    --    --IMMV_DECv <= (OTHERS => 'X');
    
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01001000001" THEN -- xor
        op_0 <= "001111010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        
     --   --IMMV_DECv <= (OTHERS => 'X');
     
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001001000" THEN -- sfh
        op_0 <= "010001010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
    --    --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001000000" THEN -- sf
        op_0 <= "010011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
     --   --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
    
      ELSIF in_0(0 TO 10) = "00001000010" THEN -- bg
        op_0 <= "010101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
    --    --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01001001001" THEN -- eqv
        op_0 <= "010111010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01111000000" THEN -- ceq
        op_0 <= "011001010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001011011" THEN -- shl
        op_0 <= "011011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00001011000" THEN -- rot
        op_0 <= "011101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Check for Permute Instructions
      ELSIF in_0(0 TO 10) = "00111011011" THEN -- shlqbi
        op_0 <= "000011001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00111011000" THEN -- rotqbi
        op_0 <= "000101001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00110110000" THEN -- gb
        op_0 <= "000111001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00110110001" THEN -- gbh
        op_0 <= "001001001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00110110010" THEN -- gbb
        op_0 <= "001011001";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Check for Byte Instructions
      ELSIF in_0(0 TO 10) = "01010110100" THEN  -- cntb
        op_0 <= "000011110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
        
      ELSIF in_0(0 TO 10) = "01001010011" THEN -- sumb
        op_0 <= "000101110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01111010000" THEN -- ceqb
        op_0 <= "000111110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00111011100" THEN -- rotqby
        op_0 <= "001001110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "00111011111" THEN -- shlqby
        op_0 <= "001011110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Check for FPU Instructions
      ELSIF in_0(0 TO 10) = "01011000100" THEN -- fa
        op_0 <= "000010110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01011000101" THEN  -- fs
        op_0 <= "000100110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01011000110" THEN -- fm
        op_0 <= "000110110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      ELSIF in_0(0 TO 10) = "01111000010" THEN -- fceq
        op_0 <= "001010110";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
      
      -- Load Instruction x-form
      ELSIF in_0(0 TO 10) = "00111000100" THEN -- lqx
        op_0 <= "000100101";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        --IMMV_DECv <= (OTHERS => 'X');
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '1';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "11";
        
      -- Store Instruction x-form
      ELSIF in_0(0 TO 10) = "00101000100" THEN -- stqx
        op_0 <= "001100101";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(11 TO 17);
        rc_0 <= in_0(25 TO 31); -- rt goes in rc
        rt_0 <= (OTHERS => 'X');--in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= (OTHERS => 'X');
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '1';
         imm_selv1 <= "00";
      
      -- Load Instruction a-form and  r-form
      ELSIF in_0(0 TO 8) = "001100001" THEN -- lqa
        op_0 <= "000110101";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '1';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "10";
      
      ELSIF in_0(0 TO 8) = "001100111" THEN -- lqr
        op_0 <= "001000101";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '1';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "10";
      
      -- Store Instruction a-form and  r-form
      ELSIF in_0(0 TO 8) = "001000001" THEN -- stqa
        op_0 <= "001110101";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= in_0(25 TO 31);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= (OTHERS => 'X');--in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '1';
        imm_selv1 <= "10";
      
      ELSIF in_0(0 TO 8) = "001000111" THEN -- stqr
        op_0 <= "010000101";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= in_0(25 TO 31);
        rc_0 <= (OTHERS => 'X');
        rt_0 <=  (OTHERS => 'X');--in_0(25 TO 31);
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '1';
        imm_selv1 <= "10";
        
      -- BRANCH UNIT Instructions
      ELSIF in_0(0 TO 8) = "001100100" THEN -- br
        op_0 <= "000010011";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= (OTHERS => 'X');
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "10";
      
      ELSIF in_0(0 TO 8) = "001100000" THEN -- bra
        op_0 <= "000100011";
        ra_0 <= (OTHERS => 'X');
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= (OTHERS => 'X');
        i10_0 <= (OTHERS => 'X');
        i16_0 <= in_0(9 TO 24);
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      -- FXU Immediate Type Instructions
      ELSIF in_0(0 TO 7) = "00011101" THEN -- ahi
        op_0 <= "011111010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      ELSIF in_0(0 TO 7) = "00011100" THEN -- ai
        op_0 <= "100001010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      ELSIF in_0(0 TO 7) = "00001101" THEN -- sfhi
        op_0 <= "100011010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
        
      ELSIF in_0(0 TO 7) = "00001100" THEN -- sfi
        op_0 <= "100101010";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      -- Load Instruction d-form
      ELSIF in_0(0 TO 7) = "00110100" THEN -- lqd
        op_0 <= "000010101";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= (OTHERS => 'X');
        rc_0 <= (OTHERS => 'X');
        rt_0 <= in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '1';
        vMemRd_ins0 <= '1';
        vMemWr_ins0 <= '0';
        imm_selv1 <= "01";
      
      -- Store Instruction d-form      
      ELSIF in_0(0 TO 7) = "00100100" THEN -- stqd
        op_0 <= "001010101";
        ra_0 <= in_0(18 TO 24);
        rb_0 <= in_0(25 TO 31);
        rc_0 <= (OTHERS => 'X');
        rt_0 <= (OTHERS => 'X');--in_0(25 TO 31);
        i10_0 <= in_0(8 TO 17);
        i16_0 <= (OTHERS => 'X');
        
        vRegWr_ins0 <= '0';
        vMemRd_ins0 <= '0';
        vMemWr_ins0 <= '1';
        imm_selv1 <= "01";
      
      ELSE
        op_0 <= (OTHERS => 'Z');
       ra_0 <= (OTHERS => 'Z');
       rb_0 <= (OTHERS => 'Z');
       rc_0 <= (OTHERS => 'Z');
       rt_0 <= (OTHERS => 'Z');
       i10_0 <= (OTHERS => 'Z');
       i16_0 <= (OTHERS => 'Z');
   
      vRegWr_ins0 <= '0';
      vMemRd_ins0 <= '0';
      vMemWr_ins0 <= '0';
      imm_selv1 <= "11";
        
      END IF;
 ---------------------------------------------------------------------------------------------------------------------------------------------------------     
      -- Check for Instruction 2
      -- Check for FXU
      
        IF in_1(0 TO 10) = "00000000000" THEN -- noop
        op_1 <= "000000000";
        ra_1 <= "0000000";
        rb_1 <= "0000000";
        rc_1 <= "0000000";
        rt_1 <= "0000000";
        i10_1 <= (OTHERS => '0');
        i16_1 ( 0 to 15) <= (OTHERS => '0');
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "11";
      -----------------
      IF in_1(0 TO 10) = "00011000000" THEN -- a
        op_1 <= "000011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00011001000" THEN  -- ah
        op_1 <= "000101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00011000001" THEN -- and
        op_1 <= "000111010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00001000001" THEN -- or
        op_1 <= "001001010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00011001001" THEN -- nand
        op_1 <= "001011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
                
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00001001001" THEN -- nor
        op_1 <= "001101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "01001000001" THEN -- xor
        op_1 <= "001111010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00001001000" THEN -- sfh
        op_1 <= "010001010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00001000000" THEN -- sf
        op_1 <= "010011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
    
      ELSIF in_1(0 TO 10) = "00001000010" THEN -- bg
        op_1 <= "010101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "01001001001" THEN -- eqv
        op_1 <= "010111010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "01111000000" THEN -- ceq
        op_1 <= "011001010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00001011011" THEN -- shl
        op_1 <= "011011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00001011000" THEN -- rot
        op_1 <= "011101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      -- Check for Permute Instructions
      ELSIF in_1(0 TO 10) = "00111011011" THEN -- shlqbi
        op_1 <= "000011001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00111011000" THEN -- rotqbi
        op_1 <= "000101001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
                
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00110110000" THEN -- gb
        op_1 <= "000111001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00110110001" THEN -- gbh
        op_1 <= "001001001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00110110010" THEN -- gbb
        op_1 <= "001011001";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      -- Check for Byte Instructions
      ELSIF in_1(0 TO 10) = "01010110100" THEN  -- cntb
        op_1 <= "000011110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "01001010011" THEN -- sumb
        op_1 <= "000101110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "01111010000" THEN -- ceqb
        op_1 <= "000111110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00111011100" THEN -- rotqby
        op_1 <= "001001110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "00111011111" THEN -- shlqby
        op_1 <= "001011110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      -- Check for FPU Instructions
      ELSIF in_1(0 TO 10) = "01011000100" THEN -- fa
        op_1 <= "000010110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "01011000101" THEN  -- fs
        op_1 <= "000100110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "01011000110" THEN -- fm
        op_1 <= "000110110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      ELSIF in_1(0 TO 10) = "01111000010" THEN -- fceq
        op_1 <= "001010110";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      -- Load Instruction x-form
      ELSIF in_1(0 TO 10) = "00111000100" THEN -- lqx
        op_1 <= "000100101";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '1';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "00";
      
      -- Store Instruction x-form
      ELSIF in_1(0 TO 10) = "00101000100" THEN -- stqx
        op_1 <= "001100101";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(11 TO 17);
        rc_1 <= in_1(25 TO 31);
        rt_1 <= (OTHERS => 'X');--in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '1';
        imm_selv2 <= "00";
      
      -- Load Instruction a-form and  r-form
      ELSIF in_1(0 TO 8) = "001100001" THEN -- lqa
        op_1 <= "000110101";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '1';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "10";
      
      ELSIF in_1(0 TO 8) = "001100111" THEN -- lqr
        op_1 <= "001000101";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '1';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "10";
      
      -- Store Instruction a-form and  r-form
      ELSIF in_1(0 TO 8) = "001000001" THEN -- stqa
        op_1 <= "001110101";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= in_1(25 TO 31);--(OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= (OTHERS => 'X');
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '1';
        imm_selv2 <= "10";
              
      ELSIF in_1(0 TO 8) = "001000111" THEN -- stqr
        op_1 <= "010000101";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= in_1(25 TO 31);
        rc_1 <= (OTHERS => 'X');
        rt_1 <= (OTHERS => 'X');--in_1(25 TO 31);
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '1';
        imm_selv2 <= "10";
      
      -- BRANCH UNIT Instructions
      ELSIF in_1(0 TO 8) = "001100100" THEN -- br
        op_1 <= "000010011";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= (OTHERS => 'X');
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "10";
        
      ELSIF in_1(0 TO 8) = "001100000" THEN -- bra
        op_1 <= "000100011";
        ra_1 <= (OTHERS => 'X');
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= (OTHERS => 'X');
        i10_1 <= (OTHERS => 'X');
        i16_1 <= in_1(9 TO 24);
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "10";
      
      -- FXU Immediate Type Instructions
      ELSIF in_1(0 TO 7) = "00011101" THEN -- ahi
        op_1 <= "011111010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      ELSIF in_1(0 TO 7) = "00011100" THEN -- ai
        op_1 <= "100001010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      ELSIF in_1(0 TO 7) = "00001101" THEN -- sfhi
        op_1 <= "100011010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      ELSIF in_1(0 TO 7) = "00001100" THEN -- sfi
        op_1 <= "100101010";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      -- Load Instruction d-form
      ELSIF in_1(0 TO 7) = "00110100" THEN -- lqd
        op_1 <= "000010101";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= (OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= in_1(25 TO 31);
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '1';
        vMemRd_ins1 <= '1';
        vMemWr_ins1 <= '0';
        imm_selv2 <= "01";
      
      -- Store Instruction d-form      
      ELSIF in_1(0 TO 7) = "00100100" THEN -- stqd
        op_1 <= "001010101";
        ra_1 <= in_1(18 TO 24);
        rb_1 <= in_1(25 TO 31);--(OTHERS => 'X');
        rc_1 <= (OTHERS => 'X');
        rt_1 <= (OTHERS => 'X');
        i10_1 <= in_1(8 TO 17);
        i16_1 <= (OTHERS => 'X');
        
        vRegWr_ins1 <= '0';
        vMemRd_ins1 <= '0';
        vMemWr_ins1 <= '1';
        imm_selv2 <= "01";
      
      ELSE
    op_1 <= (OTHERS => 'Z');
    ra_1 <= (OTHERS => 'Z');
    rb_1 <= (OTHERS => 'Z');
    rc_1 <= (OTHERS => 'Z');
    rt_1 <= (OTHERS => 'Z');
    i10_1 <= (OTHERS => 'Z');
    i16_1 <= (OTHERS => 'Z');
    imm_selv2 <= "00";
    imm_selv2 <= "00";
       vRegWr_ins1 <= '0';
       vMemRd_ins1 <= '0';
       vMemWr_ins1 <= '0';
       END IF;
---------------------------------------------------------------------------------------------------------------------------------------------------------      
-- Check for Structural Hazards and assign signals to output signals
--      
--if enable ='0' then
--current_s <= s0;  --default state on reset.
--elsif (rising_edge(clk)) then
--current_s <= next_s;   --state change.
--end if;
----end if;
----state machine process.
--  case current_s is
--   ---------------------------------------------------------------------------------------------------------
--
--      when s0 =>       --when current state is "s0"
--      if op_0(8) = '0' and  op_1(8) ='0' then
--      next_s <= s1;
--      elsif op_0(8) = '0' and  op_1(8) ='1' then
--     next_s <= s3;
--      elsif op_0(8) = '1' and  op_1(8) ='0' then
--      next_s <= s3;
--      elsif op_0(8) = '1' and  op_1(8) ='1' then
--      next_s <= s1;
--      end if;
--  -----------------------------------------------------------------------------------------------------         
--     when s1 =>
--       next_s <= s2;
--
--    ----------------------------------------------------------------------------------------------------- 
--      when s2 =>       --when current state is "s2"
--
--      next_s <= s0;
--    ----------------------------------------------------------------------------------------------------
--      when s3 =>        --when current state is "s0"
--        if inst_0'event or inst_1'event then
--       next_s <= s0;
--     end if;
--    -------------------------------------------------------------------
--      end case;
     
          if enable = '1' then
          case current_s is
          when  s0 =>
          if op_0(8) = '0' and  op_1(8) ='0'  then
                        
          opcode_0 <= op_0;   
          ra1_addr <= ra_0;   
          rb1_addr <= rb_0;   
          rc1_addr <= rc_0;   
          rt1_addr <= rt_0;   
          imm7_1   <= (OTHERS => 'Z');          
          imm8_1   <= (OTHERS => 'Z');          
          imm10_1  <= i10_0;  
          imm16_1  <= i16_0;  
          RegWr_ins0 <= vRegWr_ins0; 
          MemRd_ins0 <= vMemRd_ins0; 
          MemWr_ins0 <= vMemWr_ins0; 
           
          opcode_1 <= (OTHERS => '0');    
          ra2_addr <= (OTHERS => 'Z');    
          rb2_addr <= (OTHERS => 'Z');    
          rc2_addr <= (OTHERS => 'Z');    
          rt2_addr <= (OTHERS => 'Z');    ---no op
          imm7_2   <= (OTHERS => 'Z');    
          imm8_2   <= (OTHERS => 'Z');    
          imm10_2  <= (OTHERS => 'Z');    
          imm16_2  <= (OTHERS => 'Z');    
          RegWr_ins1 <= '0';            
          MemRd_ins1 <= '0';            
          MemWr_ins1 <= '0';          
          delayILB <= '1';
          next_s <= s1;
          
          buff_op_0 <= op_1;
          buff_ra_0 <= ra_1;
          buff_rb_0 <= rb_1;
          buff_rc_0 <= rc_1;
          buff_rt_0 <= rt_1;
          buff_i10_0 <= i10_1;
          buff_i16_0 <= i16_1;
          buff_vRegWr_ins0 <= vRegWr_ins1;
          buff_vMemRd_ins0 <= vMemRd_ins1;
          buff_vMemWr_ins0 <= vMemWr_ins1;

          
          
          elsif  op_0(8) = '1' and  op_1(8) = '1' then
          
          
          opcode_1 <= op_0 ;   --issue instruction 0 to odd pipe 
          ra2_addr <= ra_0 ;   
          rb2_addr <= rb_0 ;   
          rc2_addr <= rc_0 ;   
          rt2_addr <= rt_0 ;
          imm7_2   <= (OTHERS => 'Z');    
          imm8_2   <= (OTHERS => 'Z');    
          imm10_2  <= i10_0 ;   
          imm16_2  <= i16_0 ;
          RegWr_ins1 <= vRegWr_ins0 ;
          MemRd_ins1 <= vMemRd_ins0 ;           
          MemWr_ins1 <= vMemWr_ins0 ;           
          delayILB <= '1';       
          
          opcode_0 <= (OTHERS => 'Z');    
          ra1_addr <= (OTHERS => 'Z');    
          rb1_addr <= (OTHERS => 'Z');    
          rc1_addr <= (OTHERS => 'Z');    
          rt1_addr <= (OTHERS => 'Z');    ---no op
          imm7_1   <= (OTHERS => 'Z');    
          imm8_1   <= (OTHERS => 'Z');    
          imm10_1  <= (OTHERS => 'Z');    
          imm16_1  <= (OTHERS => 'Z');    
          RegWr_ins0 <= '0';            
          MemRd_ins0 <= '0';            
          MemWr_ins0 <= '0';          
          delayILB <= '1';
          buff_op_1 <= op_1;
          buff_ra_1 <= ra_1;
          buff_rb_1 <= rb_1;
          buff_rc_1 <= rc_1;
          buff_rt_1 <= rt_1;
          buff_i10_1 <= i10_1;
          buff_i16_1 <= i16_1;
          buff_vRegWr_ins1 <= vRegWr_ins1;
          buff_vMemRd_ins1 <= vMemRd_ins1;
          buff_vMemWr_ins1 <= vMemWr_ins1;
                    
          next_s <= s2;
             elsif op_0(8) = '0' and  op_1(8) ='1' then
          opcode_0 <= op_0;   
          ra1_addr <= ra_0;   
          rb1_addr <= rb_0;   
          rc1_addr <= rc_0;   
          rt1_addr <= rt_0;   
          imm7_1   <= (OTHERS => 'Z');          
          imm8_1   <= (OTHERS => 'Z');          
          imm10_1  <= i10_0;  
          imm16_1  <= i16_0;  
          RegWr_ins0 <= vRegWr_ins0; 
          MemRd_ins0 <= vMemRd_ins0; 
          MemWr_ins0 <= vMemWr_ins0; 
        
          opcode_1 <= op_1 ;
          ra2_addr <= ra_1 ;
          rb2_addr <= rb_1 ;
          rc2_addr <= rc_1 ;   
          rt2_addr <= rt_1 ;
          imm7_2   <= (OTHERS => 'Z');    
          imm8_2   <= (OTHERS => 'Z');    
          imm10_2  <= i10_1;   
          imm16_2  <= i16_1;
          RegWr_ins1 <= vRegWr_ins1 ;
          MemRd_ins1 <= vMemRd_ins1 ;           
          MemWr_ins1 <= vMemWr_ins1 ; 
          
          elsif op_0(8) = '1' and  op_1(8) ='0' then
             
             
          opcode_1 <= op_0;   
          ra2_addr <= ra_0;   
          rb2_addr <= rb_0;   
          rc2_addr <= rc_0;   
          rt2_addr <= rt_0;   
          imm7_2   <= (OTHERS => 'Z');          
          imm8_2   <= (OTHERS => 'Z');          
          imm10_2  <= i10_0;  
          imm16_2  <= i16_0;  
          RegWr_ins1 <= vRegWr_ins0; 
          MemRd_ins1 <= vMemRd_ins0; 
          MemWr_ins1 <= vMemWr_ins0; 
          
          opcode_0 <= op_1;   
          ra1_addr <= ra_1;   
          rb1_addr <= rb_1;   
          rc1_addr <= rc_1;   
          rt1_addr <= rt_1;   
          imm7_1   <= (OTHERS => 'Z');          
          imm8_1   <= (OTHERS => 'Z');          
          imm10_1  <= i10_1;  
          imm16_1  <= i16_1;  
          RegWr_ins0 <= vRegWr_ins1; 
          MemRd_ins0 <= vMemRd_ins1; 
          MemWr_ins0 <= vMemWr_ins1; 
          
          next_s <= s3;
          end if;
 
          
        when s1 =>
          
          
          opcode_0 <= buff_op_0;-- after 10 ns;   --issue instruction 1 to even pipe again
          ra1_addr <= buff_ra_0;-- after 10 ns;   
          rb1_addr <= buff_rb_0;-- after 10 ns;   
          rc1_addr <= buff_rc_0;-- after 10 ns;   
          rt1_addr <= buff_rt_0;-- after 10 ns;
          imm7_1   <= (OTHERS => 'Z');--after 10 ns;    
          imm8_1   <= (OTHERS => 'Z');--after 10 ns;    
          imm10_1  <= buff_i10_0;-- after 10 ns;   
          imm16_1  <= buff_i16_0;-- after 10 ns;
          RegWr_ins0 <= buff_vRegWr_ins0;-- after 10 ns;
          MemRd_ins0 <= buff_vMemRd_ins0;-- after 10 ns;           
          MemWr_ins0 <= buff_vMemWr_ins0;-- after 10 ns;           
          delayILB <= '0';-- after 10 ns;       
          
          opcode_1 <=  (OTHERS => '0');-- after 10 ns;    
          ra2_addr <= (OTHERS => 'Z');--after 10 ns;    
          rb2_addr <= (OTHERS => 'Z');--after 10 ns;    
          rc2_addr <= (OTHERS => 'Z');--after 10 ns;    
          rt2_addr <= (OTHERS => 'Z');--after 10 ns;    ---no op
          imm7_2   <= (OTHERS => 'Z');--after 10 ns;    
          imm8_2   <= (OTHERS => 'Z');--after 10 ns;    
          imm10_2  <= (OTHERS => 'Z');--after 10 ns;    
          imm16_2  <= (OTHERS => 'Z');--after 10 ns;    
          RegWr_ins1 <= '0';-- after 10 ns;            
          MemRd_ins1 <= '0';-- after 10 ns;      
          MemWr_ins1 <= '0';-- after 10 ns;          
          --delayILB <= '1' after 10 ns;
         next_s <= s3;
         
       when s2 =>
          
          
          
          opcode_1 <= buff_op_1;-- after 10 ns;   --issue instruction 0 to odd pipe again
          ra2_addr <= buff_ra_1;-- after 10 ns ;   
          rb2_addr <= buff_rb_1;-- after 10 ns ;    
          rc2_addr <= buff_rc_1;-- after 10 ns ;    
          rt2_addr <= buff_rt_1;-- after 10 ns ; 
          imm7_2   <= (OTHERS => 'Z');--after 10 ns ;    
          imm8_2   <= (OTHERS => 'Z');--after 10 ns ; 
          imm10_2  <= buff_i10_1;-- after 10 ns ; 
          imm16_2  <= buff_i16_1 ;--after 10 ns ; 
          RegWr_ins1 <= buff_vRegWr_ins1;-- after 10 ns ; 
          MemRd_ins1 <= buff_vMemRd_ins1;-- after 10 ns ;   
          MemWr_ins1 <= buff_vMemWr_ins1;-- after 10 ns ; 
          delayILB <= '0';-- after 10 ns; 
          
          opcode_0 <= (OTHERS => 'Z');    
          ra1_addr <= (OTHERS => 'Z');    
          rb1_addr <= (OTHERS => 'Z');    
          rc1_addr <= (OTHERS => 'Z');    
          rt1_addr <= (OTHERS => 'Z');    ---no op
          imm7_1   <= (OTHERS => 'Z');    
          imm8_1   <= (OTHERS => 'Z');    
          imm10_1  <= (OTHERS => 'Z');    
          imm16_1  <= (OTHERS => 'Z');    
          RegWr_ins0 <= '0';            
          MemRd_ins0 <= '0';            
          MemWr_ins0 <= '0';          
        --  delayILB <= '1';
          
          next_s <= s3;
          
          when s3 =>
          
          next_s <= s0;
        end case;
          
            end if;
             
          end if; --enable
          end if; --clk 
END PROCESS; 

--type2 : process (current_s,next_s)
--begin
----if (rising_edge(clk))
----then  
--case current_s is
--
--when S0 => issue0 <= 0; issue1 <= 0; delayILB <= '1';
--when S1 => issue0 <= 1; issue1 <= 0; delayILB <= '1';
--when S2 => issue0 <= 0; issue1 <= 1; delayILB <= '1';
--when S3 => issue0 <= 1; issue1 <= 1; delayILB <= '0';
--end case;
----end  if;
--end process;

END behave;