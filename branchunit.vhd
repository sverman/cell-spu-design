library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity branchunit is
  
PORT (
    ra, rb, rc, rt: IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    
    tag_address : IN STD_LOGIC_VECTOR(0 TO 31);
    flush : OUT BIT;
    load_LS : OUT BIT;
    pc_write : OUT BIT;
    offset : OUT STD_LOGIC_VECTOR(0 TO 4);
    
    result_address : OUT STD_LOGIC_VECTOR(0 TO 31);
    clk: IN STD_ULOGIC);
END branchunit;

ARCHITECTURE behave OF branchunit IS

component FU_register is port 
        (FUR_indata   : in std_logic_vector ( 0 to 127);
               reset  : in std_logic :='0';
               clk    : in std_logic;
          FUR_outdata : out std_logic_vector ( 0 to 127));
end component;

signal  result_s1 : STD_LOGIC_VECTOR(0 TO 31);
signal dummy1 : STD_LOGIC_VECTOR ( 32 to 127) := (others => 'Z');
signal reset_s : std_logic := '0';
signal result_addr : UNSIGNED(0 TO 31);
signal result_addr2 : STD_LOGIC_VECTOR(0 TO 31);
BEGIN
  
   reg1map : FU_register PORT MAP
   (
    FUR_indata (0 TO 31) => result_s1,
    FUR_indata ( 32 to 127) =>  dummy1 ( 32 to 127),
    reset => reset_s,
    clk   => clk,
    FUR_outdata ( 32 to 127) => dummy1 ( 32 to 127),
    FUR_outdata (0 TO 31) => result_addr2 --t_uns_sig2
  );
  
  PROCESS(clk)

    variable immv10 : UNSIGNED(0 TO 9); 
    variable immv8 : UNSIGNED(0 TO 7);
    variable immv16 : UNSIGNED(0 TO 15);
    variable ext : UNSIGNED(0 TO 13);
    variable ext2 : UNSIGNED(0 TO 17);
    variable zeroext : UNSIGNED(0 TO 3) := "0000";

    variable in_ra : UNSIGNED(0 TO 31);
    variable value1 : UNSIGNED(0 TO 31);
    variable value2 : UNSIGNED(0  TO 31);
    variable lsa : UNSIGNED(0 TO 31);

    
    VARIABLE tag_addr : UNSIGNED(0 TO 31);
    VARIABLE offset_l : UNSIGNED(0 TO 4);
    VARIABLE load_LS_l : BIT;
    VARIABLE flush_l : BIT;
    

    begin -- process

      if clk'event and clk = '1' 
      then -- clk
      
   --   IF enable = '1' THEN

        in_ra(0 to 31) := UNSIGNED(ra (0 to 31));
        immv16 ( 0 to 15) := UNSIGNED(rc(0 to 15)); 

        if opcode = "00001"   -- branch absolute form
        then
          ext2(0 to 17) := immv16 & b"00";
          value1(0 to 31):= ((18 to 31 => ext2(17)) & ext2) and X"0003FFFF";
       --   result_address <= STD_LOGIC_VECTOR(value1);
        
        elsif opcode = "00010"   -- branch relative form
        then
          ext2(0 to 17) := immv16 & b"00";  
          value1(0 to 31):= (((18 to 31 => ext2(17)) & ext2)+in_ra) and X"0003FFFF";    
        --  result_address <= STD_LOGIC_VECTOR(value1);
        end if;
        if enable ='1' then
        result_s1 <= STD_LOGIC_VECTOR(value1);
        result_address <= STD_LOGIC_VECTOR(result_addr);
        else
        result_s1 <= (OTHERS => 'Z');
        end if;
    
        result_addr2 <= STD_LOGIC_VECTOR(result_addr);
     
        if enable = '1' then
        
        IF (result_addr2(0 TO 25) = tag_address(0 to 25) ) and (not (result_addr2 (0 to 25) <= ( result_addr2'range => 'Z')  ) )  THEN
        -- Target addr is in ILB
        offset_l(0 TO 4) := result_addr(26 TO 30); -- target address is in ILB
        flush_l := '0';
        load_LS_l := '0';
        ELSE
        -- Target addr is in LS
        flush_l := '1';
        load_LS_l := '1';
        END IF;        
        
        flush <= flush_l;
        load_LS <= load_LS_l;
        pc_write <= '1';
        
        IF flush_l = '1' THEN
          offset <= (OTHERS => 'Z');
        ELSE
        offset <= STD_LOGIC_VECTOR(offset_l);
        END IF;
        
        ELSE
        result_address <= (OTHERS => 'Z');
        offset <= (OTHERS => 'Z');
        END IF; -- enable
    
      end if; -- end clk
    end process;
end behave;  