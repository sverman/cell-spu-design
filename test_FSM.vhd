library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity test_mealy is
end test_mealy;
architecture test_mly of test_mealy is
component mealy is 
port (clk : in std_logic;
      reset : in std_logic;
      input : in std_logic;
      output : out std_logic
  ); 
end component;

  
    signal clk :  std_logic;
   signal   reset :  std_logic;
     signal input :  std_logic;
      signal output :  std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: mealy PORT MAP
(
 output => output,
 input => input,
 reset => reset,
 clk =>clk
 );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
  stim_proc: process
   begin		
   input <= '1';
    reset <= '0';
    wait for clk_period;
		  input <= '0';
     wait for clk_period;
		  input <= '1';
		--   wait for clk_period;
		 
		   wait for clk_period;
		  input <= '1';
		  wait for clk_period;
		  input <= '0';
  
      wait;
   end process;
END;
