library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
USE IEEE.std_logic_textio.ALL;
USE STD.textio.ALL;

Entity tb_branchunit is
end;
architecture test of tb_branchunit is
   FILE logger : TEXT OPEN WRITE_MODE IS "transcript_BranchUnit.log";

component branchunit
port(ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    result_address : OUT STD_LOGIC_VECTOR(0 TO 31);
    clk: IN STD_ULOGIC);
end component;

signal ra, rb, rc, rt : STD_LOGIC_VECTOR(0 TO 127);
signal opcode :  STD_LOGIC_VECTOR(0 TO 4);
signal result_address :  STD_LOGIC_VECTOR(0 TO 31);
signal clk:  STD_ULOGIC;
signal enable:  STD_ULOGIC;

PROCEDURE print(opcode : STD_LOGIC_VECTOR(0 TO 4);
                  ra : STD_LOGIC_VECTOR(0 TO 127);
                  rb : STD_LOGIC_VECTOR(0 TO 127);
                  rc : STD_LOGIC_VECTOR(0 TO 127);
                  rt : STD_LOGIC_VECTOR(0 TO 127);                
                  result_address : STD_LOGIC_VECTOR(0 TO 31)
                  ) IS
    VARIABLE line_out : line;
    ALIAS swrite IS write [line, string, side, width] ;
  BEGIN
    swrite(line_out, "opcode = ");
    write(line_out, opcode);
    writeline(logger, line_out);
    swrite(line_out, "ra =     ");
    hwrite(line_out, ra);
    writeline(logger, line_out);
    swrite(line_out, "rb =     ");
    hwrite(line_out, rb);
    writeline(logger, line_out);
    swrite(line_out, "rc =     ");
    hwrite(line_out, rc);
    writeline(logger, line_out);
    swrite(line_out, "rt =     ");
    hwrite(line_out, rt);
    writeline(logger, line_out);
    swrite(line_out, "result = ");
    hwrite(line_out, result_address);
    writeline(logger, line_out);
    swrite(line_out, " ");
    writeline(logger, line_out);
  END print;

 constant clk_period : time := 10 ns;
    begin
    uut: branchunit PORT MAP (
     ra  => ra,
     rb  => rb,
     rc  => rc,
     rt  => rt,
     opcode  => opcode,
     enable => enable,
     result_address  => result_address,
     clk => clk
                     );
clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;

stim_proc: process is
 
   begin   
      enable <= '1';

      opcode <= "00001";
      ra <= X"11101021001120113050210430201022";
      rc <= X"44101021001120113030210130101022";
      rb(0 TO 127) <= (OTHERS => 'X');
      rt(0 TO 127) <= (OTHERS => 'X');
      wait for 10 ns;
       print(opcode, ra, rb, rc, rt, result_address);
      
      opcode <= "00010";
       wait for 10 ns;
       print(opcode, ra, rb, rc, rt, result_address);
      
      opcode <= "00001";
       wait for 10 ns;
       print(opcode, ra, rb, rc, rt, result_address);
      
      opcode <= "00010";
      wait for 10 ns;
       print(opcode, ra, rb, rc, rt, result_address);
      
      opcode <= "00001";
      wait for 10 ns;
       print(opcode, ra, rb, rc, rt, result_address);
      
      opcode <= "00010";
      wait for 10 ns;
       print(opcode, ra, rb, rc, rt, result_address);
      
      opcode <= "00001";
      wait for 10 ns;
       print(opcode, ra, rb, rc, rt, result_address);
      
      enable <= '0';
      wait ;
       
  end process;
end;                                  