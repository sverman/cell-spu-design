LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY fxu IS
  PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_ULOGIC);
END fxu;

ARCHITECTURE behave OF fxu IS
  component FU_register is port (FUR_indata   : in std_logic_vector ( 0 to 127);
               reset  : in std_logic :='0';
               clk    : in std_logic;
          FUR_outdata : out std_logic_vector ( 0 to 127));
end component;

signal rt_uns_sig1 : std_logic_vector(0 TO 127);-- : = (OTHERS => '0');
signal rt_uns_sig2 : std_logic_vector(0 TO 127);
signal reset_s : std_logic := '0';
BEGIN
  
   reg1map : FU_register PORT MAP
   (
    FUR_indata  => rt_uns_sig1,
    reset => reset_s,
    clk   => clk,
    FUR_outdata => result--t_uns_sig2
  );
--   reg2map : FU_register PORT MAP
--   (
--    FUR_indata  => rt_uns_sig2,
--    reset => reset_s,
--    clk   => clk,
--    FUR_outdata => result
--  );
 PROCESS(clk)
  -- declaration of variables
  VARIABLE ra_uns : UNSIGNED(0 TO 127);
  VARIABLE rb_uns : UNSIGNED(0 TO 127);
  VARIABLE rc_uns : UNSIGNED(0 TO 127);
  VARIABLE rt_uns : UNSIGNED(0 TO 127); 
  VARIABLE z_uns : UNSIGNED(0 TO 0);
  
  VARIABLE t : UNSIGNED(0 TO 31);
  VARIABLE r : UNSIGNED(0 TO 31);
  VARIABLE p : UNSIGNED(0 TO 127);
  VARIABLE b : INTEGER;
  VARIABLE s : INTEGER;
  VARIABLE i : INTEGER;
  VARIABLE x : INTEGER;
  
  VARIABLE imm16 : UNSIGNED(0 TO 15);
  VARIABLE imm32 : UNSIGNED(0 TO 31);

  BEGIN
  IF clk'event AND clk = '1'  THEN -- clk
 
    
  -- initialize
  ra_uns := UNSIGNED(ra);
  rb_uns := UNSIGNED(rb);
  rc_uns := UNSIGNED(rc);
  rt_uns := UNSIGNED(rt);
  z_uns(0) := '0';

  -- select desired operation
  CASE opcode IS

  -- (1) a (add word)
  WHEN "00001" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) + rb_uns(x TO (x + 31));
  END LOOP;

  -- (2) ah (add half word)
  WHEN "00010" =>
  FOR i IN 0 TO 7 LOOP
    x := (i * 16);
    rt_uns(x TO (x + 15)) := ra_uns(x TO (x + 15)) + rb_uns(x TO (x + 15));
  END LOOP;
  
  -- (3) and (and word)
  WHEN "00011" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) AND rb_uns(x TO (x + 31));
  END LOOP;

  -- (4) or (or word)
  WHEN "00100" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) OR rb_uns(x TO (x + 31));
  END LOOP;

  -- (5) nand (nand word)
  WHEN "00101" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) NAND rb_uns(x TO (x + 31));
  END LOOP;

  -- (6) nor (nor word)
  WHEN "00110" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) NOR rb_uns(x TO (x + 31));
  END LOOP;

  -- (7) xor (xor word)
  WHEN "00111" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) XOR rb_uns(x TO (x + 31));
  END LOOP; 

  -- (8) sfh (subtract from half word)
  WHEN "01000" =>    
  FOR i IN 0 TO 7 LOOP
    x := (i * 16);
    rt_uns(x TO (x + 15)) := ra_uns(x TO (x + 15)) + (NOT(rb_uns(x TO (x + 15)))) + 1;
  END LOOP;
  
  -- (9) sf (subtract from word)
  WHEN "01001" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) + (NOT(rb_uns(x TO (x + 31)))) + 1;
  END LOOP; 
  
  -- (10) bg (Borrow Generate)
  WHEN "01010" =>   
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    IF rb_uns(x TO (x + 31)) >= ra_uns(x TO (x + 31)) THEN
      rt_uns((x + 1) TO (x + 31)) := (OTHERS => '0');
      rt_uns(x) := '1';
    ELSE
      rt_uns(x TO (x + 31)) := (OTHERS => '0');
    END IF;
  END LOOP;
  
  -- (11) eqv (equivalent)
  WHEN "01011" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) XOR (NOT(rb_uns(x TO (x + 31))));
  END LOOP;  
  
  -- (12) ceq (Compare Equal Word)
  WHEN "01100" =>
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    IF rb_uns(x TO (x + 31)) = ra_uns(x TO (x + 31)) THEN
      rt_uns(x TO (x + 31)) := X"FFFFFFFF";
    ELSE
      rt_uns(x TO (x + 31)) := X"00000000";
    END IF;
  END LOOP;  
  
  -- (13) shl (Shift Left Word)      
  WHEN "01101" =>        
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    
    s := TO_INTEGER(rb_uns(x TO (x + 31)) AND X"0000003F");
    t(0 TO 31) := ra_uns(x TO (x + 31));
    FOR b IN 0 TO 31 LOOP
      IF (b + s) < 32 THEN
        r(b) := t(b + s);
      ELSE
        r(b) := '0';
      END IF;
    END LOOP;
    rt_uns(x TO (x + 31)) := r(0 TO 31);
  END LOOP; 
  
  -- (14) rot (Rotate Word)
  WHEN "01110" =>    
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    
    s := TO_INTEGER(rb_uns(x TO (x + 31)) AND X"0000001F");
    t(0 TO 31) := ra_uns(x TO (x + 31));
    FOR b IN 0 TO 31 LOOP
      IF (b + s) < 32 THEN
        r(b) := t(b + s);
      ELSE
        r(b) := t(b + (s - 32));
      END IF;
    END LOOP;
    rt_uns(x TO (x + 31)) := r(0 TO 31);
  END LOOP;   
  
  -- IMMEDIATE TYPE INSTRUCTIONS
  -- (15) ahi (Add Half Word Immediate)
  WHEN "01111" =>
  imm16(0 TO 15) := (0 TO 5 => rc_uns(118)) & rc_uns(118 TO 127);
  FOR i IN 0 TO 7 LOOP
    x := (i * 16);
    rt_uns(x TO (x + 15)) := ra_uns(x TO (x + 15)) + imm16(0 TO 15);
  END LOOP;
    
  -- (16) ai (Add Word Immediate)
  WHEN "10000" =>
  imm32(0 TO 31) := (0 TO 21 => rc_uns(118)) & rc_uns(118 TO 127);
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := ra_uns(x TO (x + 31)) + imm32(0 TO 31);
  END LOOP;  
  
  -- (17) sfhi (Subtract From Halfword Immediate)
  WHEN "10001" =>
  imm16(0 TO 15) := (0 TO 5 => rc_uns(118)) & rc_uns(118 TO 127);
  FOR i IN 0 TO 7 LOOP
    x := (i * 16);
    rt_uns(x TO (x + 15)) := imm16(0 TO 15) + (NOT(ra_uns(x TO (x + 15)))) + 1;
  END LOOP;
  
  -- (18) sfi (Subtract From Word Immediate)
  WHEN "10010" =>
  imm32(0 TO 31) := (0 TO 21 => rc_uns(118)) & rc_uns(118 TO 127);
  FOR i IN 0 TO 3 LOOP
    x := (i * 32);
    rt_uns(x TO (x + 31)) := imm32(0 TO 31) + (NOT(ra_uns(x TO (x + 31)))) + 1;
  END LOOP;
  
  -- others 
  WHEN OTHERS => rt_uns := (OTHERS => 'X');
  END CASE;

  -- set zero bit if result equals zero 
  IF TO_INTEGER(rt_uns) = 0 THEN
    z_uns(0) := '1';
  ELSE
    z_uns(0) := '0';
  END IF; 

  -- assign variables to output signals
--  result <= STD_LOGIC_VECTOR(rt_uns);
----------------------------------------------------   
  IF enable = '1' THEN
    rt_uns_sig1 <= STD_LOGIC_VECTOR(rt_uns);
  zero <= z_uns(0);
else
  rt_uns_sig1 <= ( others => 'Z');
end if;
-------------------------------------------------------
  ELSE
   -- result <= (OTHERS => 'Z');
 -- END IF; -- enable
  
  END IF; -- clk ends
  END PROCESS; 

END behave;