library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity dualissue_pipe is  
PORT (
    ra_1, rb_1, rc_1, rt_1 : IN STD_LOGIC_VECTOR(0 TO 127);
    -- sel_1 : IN STD_ULOGIC;
    opcode_1 : IN STD_LOGIC_VECTOR(0 TO 8);
    -- result_address1_e : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_data1_1 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data2_1 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data3_1 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    
    ra_2, rb_2, rc_2, rt_2 : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode_2 : IN STD_LOGIC_VECTOR(0 TO 8);
    -- sel_2 : IN STD_ULOGIC;
    result_address1_2 : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_address2_2 : OUT STD_LOGIC_VECTOR(0 TO 31);
    result_data_2 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    clk : IN STD_ULOGIC);
end dualissue_pipe;
  
ARCHITECTURE behave of dualissue_pipe is
    
    
signal    sel_evenpipe : STD_ULOGIC;
signal    sel_oddpipe : STD_ULOGIC;


  signal    opcode_inpe :  STD_LOGIC_VECTOR(0 TO 8);
  signal    ra_inpe, rb_inpe, rc_inpe, rt_inpe :  STD_LOGIC_VECTOR(0 TO 127);
  signal    result_data_inpe1 :  STD_LOGIC_VECTOR ( 0 to 127);
  signal    result_data_inpe2 :  STD_LOGIC_VECTOR ( 0 to 127);
  signal    result_data_inpe3 :  STD_LOGIC_VECTOR ( 0 to 127);

  signal    opcode_inpo :  STD_LOGIC_VECTOR(0 TO 8);
  signal    ra_inpo, rb_inpo, rc_inpo, rt_inpo :  STD_LOGIC_VECTOR(0 TO 127);
  signal    result_data_inpo1 :  STD_LOGIC_VECTOR ( 0 to 127);
  signal    result_address_inpo1 :  STD_LOGIC_VECTOR(0 TO 31);
  signal    result_address_inpo2 :  STD_LOGIC_VECTOR(0 TO 31);

  signal    clk_s:  STD_ULOGIC;

  component evenpipe IS
  PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    sel : IN STD_ULOGIC;
    opcode : IN STD_LOGIC_VECTOR(0 TO 7);
    result_data1 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data2 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    result_data3 : OUT STD_LOGIC_VECTOR ( 0 to 127);
    clk : IN STD_ULOGIC);
  END component;
    
  component oddpipe is 
  PORT ( 
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 7);
    sel : IN STD_ULOGIC;
    result_address1 : OUT STD_LOGIC_VECTOR( 0 TO 31);
    result_address2 : OUT STD_LOGIC_VECTOR( 0 TO 31);
    result_data : OUT STD_LOGIC_VECTOR ( 0 to 127);
    clk : IN STD_ULOGIC);
  END component;

begin 
  
        evenpipeline : evenpipe port map (
          ra => ra_inpe,
          rb => rb_inpe,
          rc => rc_inpe,
          rt => rt_inpe,
          opcode( 0 to 7) => opcode_inpe( 0 to 7),
          sel => sel_evenpipe,
          result_data1 => result_data_inpe1,
          result_data2 => result_data_inpe2,
          result_data3 => result_data_inpe3,
          clk => clk_s
        );

      oddpipeline : oddpipe port map (
          ra => ra_inpo,
          rb => rb_inpo,
          rc => rc_inpo,
          rt => rt_inpo,
          opcode( 0 to 7) => opcode_inpo(0 to 7),
          sel => sel_oddpipe,
          result_data => result_data_inpo1,
          result_address1 => result_address_inpo1,
          result_address2 => result_address_inpo2,
          clk => clk_s
        );

  
  
  process (ra_1, rb_1, rc_1, rt_1, clk, opcode_1, ra_2, rb_2, rc_2, rt_2, opcode_2)

  begin   
 
    clk_s <= clk;
    if clk'event and  clk ='1' 
    then 
      -- begin execution on even pipe depending on the instruction and opcode
      if opcode_1(8) = '0'
      then
        -- connect to even pipe 
        ra_inpe <= ra_1;
        rb_inpe <= rb_1;
        rc_inpe <= rc_1;
        rt_inpe <= rt_1;
        sel_evenpipe <= '1';
        sel_oddpipe <= '0';
        opcode_inpe(0 to 7) <= opcode_1( 0 to 7);
        result_data1_1 <= result_data_inpe1;
        result_data2_1 <= result_data_inpe2;
        result_data3_1 <= result_data_inpe3;
      
        elsif opcode_1(8) = '1'
        then
          --connect to oddpipe
          ra_inpo <= ra_1;
          rb_inpo <= rb_1;
          rc_inpo <= rc_1;
          rt_inpo <= rt_1;
          sel_evenpipe <= '0';
          sel_oddpipe <= '1';
          opcode_inpo(0 to 7) <= opcode_1( 0 to 7);
          result_data_2 <= result_data_inpo1;
          result_address1_2 <= result_address_inpo1;
          result_address2_2 <= result_address_inpo2;
        end if; 

        if opcode_2(8) = '0'
        then
          -- connect to even pipe 
          ra_inpe <= ra_2;
          rb_inpe <= rb_2;
          rc_inpe <= rc_2;
          rt_inpe <= rt_2;
          sel_evenpipe <= '1';
          sel_oddpipe <= '0';
          opcode_inpe(0 to 7) <= opcode_2( 0 to 7);
          result_data1_1 <= result_data_inpe1;
          result_data2_1 <= result_data_inpe2;
          result_data3_1 <= result_data_inpe3;

        elsif opcode_2(8) = '1'
        then
          --connect to oddpipe
          ra_inpo <= ra_2;
          rb_inpo <= rb_2;
          rc_inpo <= rc_2;
          rt_inpo <= rt_2;
          sel_oddpipe <= '1';
          sel_evenpipe <= '0';
          opcode_inpo(0 to 7) <= opcode_2( 0 to 7);
          result_data_2 <= result_data_inpo1;
          result_address1_2 <= result_address_inpo1;
          result_address1_2 <= result_address_inpo2;
        end if; 

    end if; -- clk ends
  end process;
end behave;