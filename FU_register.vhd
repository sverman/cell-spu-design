library IEEE;
use IEEE.std_logic_1164.all;

entity FU_register is
   port (FUR_indata   : in std_logic_vector ( 0 to 127);
               reset  : in std_logic :='0';
               clk    : in std_logic;
          FUR_outdata : out std_logic_vector ( 0 to 127));
end FU_register;

architecture behavioral of FU_register is
  signal FUR_indata_s : std_logic_vector ( 0 to 127);
begin
  
  
   process(clk, reset)
   begin
      
      if clk'event and clk='1'
      
      then
        FUR_outdata <= FUR_indata_s;
      if reset = '0' then 
    -- FUR_outdata <= 
      FUR_indata_s <= FUR_indata;
      elsif 
      reset = '1' then
      --FUR_outdata <= (others => 'Z');
      FUR_indata_s <= (others => 'Z');
      end if;
    else
   -- FUR_outdata <= (others => 'Z');
    end if;
   end process;
end behavioral;