LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY decoder IS
  PORT(
    inst_0, inst_1 : IN STD_LOGIC_VECTOR(0 TO 31);
    opcode_0, opcode_1 : OUT STD_LOGIC_VECTOR(0 TO 8);
    
    ra1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    ra2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt2_addr  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_2    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_2    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_2   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_2   : OUT STD_LOGIC_VECTOR(0 TO 15);
    
    delayILB : OUT STD_ULOGIC;
    enable : IN STD_ULOGIC;
    clk: IN STD_ULOGIC);
END decoder;

ARCHITECTURE behave OF decoder IS
   SIGNAL in_0 : UNSIGNED(0 TO 31) := (others => '0');
    SIGNAL in_1 : UNSIGNED(0 TO 31) := (others => '0');
BEGIN
  PROCESS(clk)
    -- Declaration of Variables
   
    VARIABLE op_0 : UNSIGNED(0 TO 8);
    VARIABLE op_1 : UNSIGNED(0 TO 8);
    
    VARIABLE ra_0  : UNSIGNED(0 TO 6);
    VARIABLE rb_0  : UNSIGNED(0 TO 6);
    VARIABLE rc_0  : UNSIGNED(0 TO 6);
    VARIABLE rt_0  : UNSIGNED(0 TO 6);
    VARIABLE i10_0 : UNSIGNED(0 TO 9);
    VARIABLE i16_0 : UNSIGNED(0 TO 15);
    
    VARIABLE ra_1  : UNSIGNED(0 TO 6);
    VARIABLE rb_1  : UNSIGNED(0 TO 6);
    VARIABLE rc_1  : UNSIGNED(0 TO 6);
    VARIABLE rt_1  : UNSIGNED(0 TO 6); 
    VARIABLE i10_1 : UNSIGNED(0 TO 9);
    VARIABLE i16_1 : UNSIGNED(0 TO 15);
    
  BEGIN
     in_0 <=  UNSIGNED(inst_0);-- transport  after 1 ns;
      in_1 <=  UNSIGNED(inst_1);--  transport after 1 ns;
  IF clk'event AND clk = '1' THEN -- clk
    IF enable = '1' THEN   
  
      -- initialize
     
      
      -- Check Instruction 1
      -- Check for FXU
      IF in_0(0 TO 10) = "00011000000" THEN -- a
        op_0 := "000011010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00011001000" THEN  -- ah
        op_0 := "000101010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00011000001" THEN -- and
        op_0 := "000111010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00001000001" THEN -- or
        op_0 := "001001010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00011001001" THEN -- nand
        op_0 := "001011010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00001001001" THEN -- nor
        op_0 := "001101010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "01001000001" THEN -- xor
        op_0 := "001111010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00001001000" THEN -- sfh
        op_0 := "010001010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00001000000" THEN -- sf
        op_0 := "010011010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
    
      ELSIF in_0(0 TO 10) = "00001000010" THEN -- bg
        op_0 := "010101010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "01001001001" THEN -- eqv
        op_0 := "010111010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "01111000000" THEN -- ceq
        op_0 := "011001010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00001011011" THEN -- shl
        op_0 := "011011010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00001011000" THEN -- rot
        op_0 := "011101010";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      -- Check for Permute Instructions
      ELSIF in_0(0 TO 10) = "00111011011" THEN -- shlqbi
        op_0 := "000011001";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00111011000" THEN -- rotqbi
        op_0 := "000101001";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00110110000" THEN -- gb
        op_0 := "000111001";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00110110001" THEN -- gbh
        op_0 := "001001001";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00110110010" THEN -- gbb
        op_0 := "001011001";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      -- Check for Byte Instructions
      ELSIF in_0(0 TO 10) = "01010110100" THEN  -- cntb
        op_0 := "000011110";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "01001010011" THEN -- sumb
        op_0 := "000101110";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "01111010000" THEN -- ceqb
        op_0 := "000111110";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00111011100" THEN -- rotqby
        op_0 := "001001110";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "00111011111" THEN -- shlqby
        op_0 := "001011110";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      -- Check for FPU Instructions
      ELSIF in_0(0 TO 10) = "01011000100" THEN -- fa
        op_0 := "000010110";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "01011000101" THEN  -- fs
        op_0 := "000100110";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "01011000110" THEN -- fm
        op_0 := "000110110";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 10) = "01111000010" THEN -- fceq
        op_0 := "001010110";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      -- Load Instruction x-form
      ELSIF in_0(0 TO 10) = "00111000100" THEN -- lqx
        op_0 := "000100101";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      -- Store Instruction x-form
      ELSIF in_0(0 TO 10) = "00101000100" THEN -- stqx
        op_0 := "001100101";
        ra_0 := in_0(18 TO 24);
        rb_0 := in_0(11 TO 17);
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      
      -- Load Instruction a-form and  r-form
      ELSIF in_0(0 TO 8) = "001100001" THEN -- lqa
        op_0 := "000110101";
        ra_0 := (OTHERS => 'X');
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := in_0(9 TO 24);
      
      ELSIF in_0(0 TO 8) = "001100111" THEN -- lqr
        op_0 := "001000101";
        ra_0 := (OTHERS => 'X');
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := in_0(9 TO 24);
      
      -- Store Instruction a-form and  r-form
      ELSIF in_0(0 TO 8) = "001000001" THEN -- stqa
        op_0 := "001110101";
        ra_0 := (OTHERS => 'X');
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := in_0(9 TO 24);
      
      ELSIF in_0(0 TO 8) = "001000111" THEN -- stqr
        op_0 := "010000101";
        ra_0 := (OTHERS => 'X');
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := (OTHERS => 'X');
        i16_0 := in_0(9 TO 24);
      
      -- BRANCH UNIT Instructions
      ELSIF in_0(0 TO 8) = "001100100" THEN -- br
        op_0 := "000010011";
        ra_0 := (OTHERS => 'X');
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := (OTHERS => 'X');
        i10_0 := (OTHERS => 'X');
        i16_0 := in_0(9 TO 24);
      
      ELSIF in_0(0 TO 8) = "001100000" THEN -- bra
        op_0 := "000100011";
        ra_0 := (OTHERS => 'X');
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := (OTHERS => 'X');
        i10_0 := (OTHERS => 'X');
        i16_0 := in_0(9 TO 24);
      
      -- FXU Immediate Type Instructions
      ELSIF in_0(0 TO 7) = "00011101" THEN -- ahi
        op_0 := "011111010";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := in_0(8 TO 17);
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 7) = "00011100" THEN -- ai
        op_0 := "100001010";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := in_0(8 TO 17);
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 7) = "00001101" THEN -- sfhi
        op_0 := "100011010";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := in_0(8 TO 17);
        i16_0 := (OTHERS => 'X');
      
      ELSIF in_0(0 TO 7) = "00001100" THEN -- sfi
        op_0 := "100101010";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := in_0(8 TO 17);
        i16_0 := (OTHERS => 'X');
      
      -- Load Instruction d-form
      ELSIF in_0(0 TO 7) = "00110100" THEN -- lqd
        op_0 := "000010101";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := in_0(8 TO 17);
        i16_0 := (OTHERS => 'X');
      
      -- Store Instruction d-form      
      ELSIF in_0(0 TO 7) = "00100100" THEN -- stqd
        op_0 := "001010101";
        ra_0 := in_0(18 TO 24);
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := in_0(25 TO 31);
        i10_0 := in_0(8 TO 17);
        i16_0 := (OTHERS => 'X');
      
      ELSE
        op_0 := (OTHERS => 'X');
        ra_0 := (OTHERS => 'X');
        rb_0 := (OTHERS => 'X');
        rc_0 := (OTHERS => 'X');
        rt_0 := (OTHERS => 'X');
        i10_0 := (OTHERS => 'X');
        i16_0 := (OTHERS => 'X');
      END IF;
      
      -- Check for Instruction 2
      -- Check for FXU
      IF in_1(0 TO 10) = "00011000000" THEN -- a
        op_1 := "000011010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00011001000" THEN  -- ah
        op_1 := "000101010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00011000001" THEN -- and
        op_1 := "000111010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00001000001" THEN -- or
        op_1 := "001001010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00011001001" THEN -- nand
        op_1 := "001011010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00001001001" THEN -- nor
        op_1 := "001101010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "01001000001" THEN -- xor
        op_1 := "001111010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00001001000" THEN -- sfh
        op_1 := "010001010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00001000000" THEN -- sf
        op_1 := "010011010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
    
      ELSIF in_1(0 TO 10) = "00001000010" THEN -- bg
        op_1 := "010101010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "01001001001" THEN -- eqv
        op_1 := "010111010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "01111000000" THEN -- ceq
        op_1 := "011001010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00001011011" THEN -- shl
        op_1 := "011011010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00001011000" THEN -- rot
        op_1 := "011101010";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      -- Check for Permute Instructions
      ELSIF in_1(0 TO 10) = "00111011011" THEN -- shlqbi
        op_1 := "000011001";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00111011000" THEN -- rotqbi
        op_1 := "000101001";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00110110000" THEN -- gb
        op_1 := "000111001";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00110110001" THEN -- gbh
        op_1 := "001001001";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00110110010" THEN -- gbb
        op_1 := "001011001";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      -- Check for Byte Instructions
      ELSIF in_1(0 TO 10) = "01010110100" THEN  -- cntb
        op_1 := "000011110";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "01001010011" THEN -- sumb
        op_1 := "000101110";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "01111010000" THEN -- ceqb
        op_1 := "000111110";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00111011100" THEN -- rotqby
        op_1 := "001001110";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "00111011111" THEN -- shlqby
        op_1 := "001011110";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      -- Check for FPU Instructions
      ELSIF in_1(0 TO 10) = "01011000100" THEN -- fa
        op_1 := "000010110";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "01011000101" THEN  -- fs
        op_1 := "000100110";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "01011000110" THEN -- fm
        op_1 := "000110110";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 10) = "01111000010" THEN -- fceq
        op_1 := "001010110";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      -- Load Instruction x-form
      ELSIF in_1(0 TO 10) = "00111000100" THEN -- lqx
        op_1 := "000100101";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      -- Store Instruction x-form
      ELSIF in_1(0 TO 10) = "00101000100" THEN -- stqx
        op_1 := "001100101";
        ra_1 := in_1(18 TO 24);
        rb_1 := in_1(11 TO 17);
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      
      -- Load Instruction a-form and  r-form
      ELSIF in_1(0 TO 8) = "001100001" THEN -- lqa
        op_1 := "000110101";
        ra_1 := (OTHERS => 'X');
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := in_1(9 TO 24);
      
      ELSIF in_1(0 TO 8) = "001100111" THEN -- lqr
        op_1 := "001000101";
        ra_1 := (OTHERS => 'X');
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := in_1(9 TO 24);
      
      -- Store Instruction a-form and  r-form
      ELSIF in_1(0 TO 8) = "001000001" THEN -- stqa
        op_1 := "001110101";
        ra_1 := (OTHERS => 'X');
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := in_1(9 TO 24);
              
      ELSIF in_1(0 TO 8) = "001000111" THEN -- stqr
        op_1 := "010000101";
        ra_1 := (OTHERS => 'X');
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := (OTHERS => 'X');
        i16_1 := in_1(9 TO 24);
      
      -- BRANCH UNIT Instructions
      ELSIF in_1(0 TO 8) = "001100100" THEN -- br
        op_1 := "000010011";
        ra_1 := (OTHERS => 'X');
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := (OTHERS => 'X');
        i10_1 := (OTHERS => 'X');
        i16_1 := in_1(9 TO 24);
      
      ELSIF in_1(0 TO 8) = "001100000" THEN -- bra
        op_1 := "000100011";
        ra_1 := (OTHERS => 'X');
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := (OTHERS => 'X');
        i10_1 := (OTHERS => 'X');
        i16_1 := in_1(9 TO 24);
      
      -- FXU Immediate Type Instructions
      ELSIF in_1(0 TO 7) = "00011101" THEN -- ahi
        op_1 := "011111010";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := in_1(8 TO 17);
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 7) = "00011100" THEN -- ai
        op_1 := "100001010";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := in_1(8 TO 17);
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 7) = "00001101" THEN -- sfhi
        op_1 := "100011010";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := in_1(8 TO 17);
        i16_1 := (OTHERS => 'X');
      
      ELSIF in_1(0 TO 7) = "00001100" THEN -- sfi
        op_1 := "100101010";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := in_1(8 TO 17);
        i16_1 := (OTHERS => 'X');
      
      -- Load Instruction d-form
      ELSIF in_1(0 TO 7) = "00110100" THEN -- lqd
        op_1 := "000010101";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := in_1(8 TO 17);
        i16_1 := (OTHERS => 'X');
      
      -- Store Instruction d-form      
      ELSIF in_1(0 TO 7) = "00100100" THEN -- stqd
        op_1 := "001010101";
        ra_1 := in_1(18 TO 24);
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := in_1(25 TO 31);
        i10_1 := in_1(8 TO 17);
        i16_1 := (OTHERS => 'X');
      
      ELSE
        op_1 := (OTHERS => 'X');
        ra_1 := (OTHERS => 'X');
        rb_1 := (OTHERS => 'X');
        rc_1 := (OTHERS => 'X');
        rt_1 := (OTHERS => 'X');
        i10_1 := (OTHERS => 'X');
        i16_1 := (OTHERS => 'X');
      END IF;
      
      -- Check for Structural Hazard
      -- and assign variables to output signals
  --    IF op_0(8) = op_1(8) THEN
--        -- Structural Hazard
--        IF op_0(8) = '1' THEN
--          -- Both Instructions are for odd pipe
--          opcode_0 <= (OTHERS => 'Z');
--          ra1_addr <= (OTHERS => 'Z');
--          rb1_addr <= (OTHERS => 'Z');
--          rc1_addr <= (OTHERS => 'Z');
--          rt1_addr <= (OTHERS => 'Z');
--          imm7_1   <= (OTHERS => 'Z');
--          imm8_1   <= (OTHERS => 'Z');
--          imm10_1  <= (OTHERS => 'Z');
--          imm16_1  <= (OTHERS => 'Z');
--      
--          opcode_1 <= STD_LOGIC_VECTOR(op_1);
--          ra2_addr <= STD_LOGIC_VECTOR(ra_1);
--          rb2_addr <= STD_LOGIC_VECTOR(rb_1);
--          rc2_addr <= STD_LOGIC_VECTOR(rc_1);
--          rt2_addr <= STD_LOGIC_VECTOR(rt_1);
--          imm7_2   <= (OTHERS => 'X');
--          imm8_2   <= (OTHERS => 'X');
--          imm10_2  <= STD_LOGIC_VECTOR(i10_1);
--          imm16_2  <= STD_LOGIC_VECTOR(i16_1);
--          
--          delayILB <= '1';
--          --END
--          
--          --ISSUE opcode_0 in the next cycle
--          opcode_0 <= (OTHERS => 'Z') AFTER 10 ns;
--          ra1_addr <= (OTHERS => 'Z') AFTER 10 ns;
--          rb1_addr <= (OTHERS => 'Z') AFTER 10 ns;
--          rc1_addr <= (OTHERS => 'Z') AFTER 10 ns;
--          rt1_addr <= (OTHERS => 'Z') AFTER 10 ns;
--          imm7_1   <= (OTHERS => 'Z') AFTER 10 ns;
--          imm8_1   <= (OTHERS => 'Z') AFTER 10 ns;
--          imm10_1  <= (OTHERS => 'Z') AFTER 10 ns;
--          imm16_1  <= (OTHERS => 'Z') AFTER 10 ns;
--      
--          opcode_1 <= STD_LOGIC_VECTOR(op_0) AFTER 10 ns;
--          ra2_addr <= STD_LOGIC_VECTOR(ra_0) AFTER 10 ns;
--          rb2_addr <= STD_LOGIC_VECTOR(rb_0) AFTER 10 ns;
--          rc2_addr <= STD_LOGIC_VECTOR(rc_0) AFTER 10 ns;
--          rt2_addr <= STD_LOGIC_VECTOR(rt_0) AFTER 10 ns;
--          imm7_2   <= (OTHERS => 'X') AFTER 10 ns;
--          imm8_2   <= (OTHERS => 'X') AFTER 10 ns;
--          imm10_2  <= STD_LOGIC_VECTOR(i10_0) AFTER 10 ns;
--          imm16_2  <= STD_LOGIC_VECTOR(i16_0) AFTER 10 ns;
--          
--          delayILB <= '0' AFTER 10 ns;
--          --END
--        
--        ELSIF op_0(8)= '0' THEN
--          -- Both Instructions are for even pipe
--          opcode_0 <= STD_LOGIC_VECTOR(op_0);
--          ra1_addr <= STD_LOGIC_VECTOR(ra_0);
--          rb1_addr <= STD_LOGIC_VECTOR(rb_0);
--          rc1_addr <= STD_LOGIC_VECTOR(rc_0);
--          rt1_addr <= STD_LOGIC_VECTOR(rt_0);
--          imm7_1   <= (OTHERS => 'X');
--          imm8_1   <= (OTHERS => 'X');
--          imm10_1  <= STD_LOGIC_VECTOR(i10_0);
--          imm16_1  <= STD_LOGIC_VECTOR(i16_0);
--      
--          opcode_1 <= (OTHERS => 'Z');
--          ra2_addr <= (OTHERS => 'Z');
--          rb2_addr <= (OTHERS => 'Z');
--          rc2_addr <= (OTHERS => 'Z');
--          rt2_addr <= (OTHERS => 'Z');
--          imm7_2   <= (OTHERS => 'Z');
--          imm8_2   <= (OTHERS => 'Z');
--          imm10_2  <= (OTHERS => 'Z');
--          imm16_2  <= (OTHERS => 'Z');
--          
--          delayILB <= '1';
--          
--          --ISSUE opcode_1 in the next cycle
--          opcode_0 <= STD_LOGIC_VECTOR(op_1) AFTER 10 ns;
--          ra1_addr <= STD_LOGIC_VECTOR(ra_1) AFTER 10 ns;
--          rb1_addr <= STD_LOGIC_VECTOR(rb_1) AFTER 10 ns;
--          rc1_addr <= STD_LOGIC_VECTOR(rc_1) AFTER 10 ns;
--          rt1_addr <= STD_LOGIC_VECTOR(rt_1) AFTER 10 ns;
--          imm7_1   <= (OTHERS => 'X') AFTER 10 ns;
--          imm8_1   <= (OTHERS => 'X') AFTER 10 ns;
--          imm10_1  <= STD_LOGIC_VECTOR(i10_1) AFTER 10 ns;
--          imm16_1  <= STD_LOGIC_VECTOR(i16_1) AFTER 10 ns;
--      
--          opcode_1 <= (OTHERS => 'Z') AFTER 10 ns;
--          ra2_addr <= (OTHERS => 'Z') AFTER 10 ns;
--          rb2_addr <= (OTHERS => 'Z') AFTER 10 ns;
--          rc2_addr <= (OTHERS => 'Z') AFTER 10 ns;
--          rt2_addr <= (OTHERS => 'Z') AFTER 10 ns;
--          imm7_2   <= (OTHERS => 'Z') AFTER 10 ns;
--          imm8_2   <= (OTHERS => 'Z') AFTER 10 ns;
--          imm10_2  <= (OTHERS => 'Z') AFTER 10 ns;
--          imm16_2  <= (OTHERS => 'Z') AFTER 10 ns;
--          
--          delayILB <= '0' AFTER 10 ns;
--          --END
--          
--        END IF;       
--      ELSE
--        
        -- No Structural Hazard
        opcode_0 <= STD_LOGIC_VECTOR(op_0);
        ra1_addr <= STD_LOGIC_VECTOR(ra_0);
        rb1_addr <= STD_LOGIC_VECTOR(rb_0);
        rc1_addr <= STD_LOGIC_VECTOR(rc_0);
        rt1_addr <= STD_LOGIC_VECTOR(rt_0);
        imm7_1   <= (OTHERS => 'X');
        imm8_1   <= (OTHERS => 'X');
        imm10_1  <= STD_LOGIC_VECTOR(i10_0);
        imm16_1  <= STD_LOGIC_VECTOR(i16_0);
      
        opcode_1 <= STD_LOGIC_VECTOR(op_1);
        ra2_addr <= STD_LOGIC_VECTOR(ra_1);
        rb2_addr <= STD_LOGIC_VECTOR(rb_1);
        rc2_addr <= STD_LOGIC_VECTOR(rc_1);
        rt2_addr <= STD_LOGIC_VECTOR(rt_1);
        imm7_2   <= (OTHERS => 'X');
        imm8_2   <= (OTHERS => 'X');
        imm10_2  <= STD_LOGIC_VECTOR(i10_1);
        imm16_2  <= STD_LOGIC_VECTOR(i16_1);
        
        delayILB <= '0';
   --   END IF;
      
    ELSE -- Enable is 0
   --   opcode_0 <= (OTHERS => '0');
--      opcode_1 <= (OTHERS => '0');
--      ra1_addr <= (OTHERS => '0');
--      rb1_addr <= (OTHERS => '0');
--      rc1_addr <= (OTHERS => '0');
--      rt1_addr <= (OTHERS => '0');
--      ra2_addr <= (OTHERS => '0');
--      rb2_addr <= (OTHERS => '0');
--      rc2_addr <= (OTHERS => '0');
--      rt2_addr <= (OTHERS => '0');
--      
--      imm7_1   <= (OTHERS => '0');
--      imm8_1   <= (OTHERS => '0');
--      imm10_1  <= (OTHERS => '0');
--      imm16_1  <= (OTHERS => '0');
--      imm7_2   <= (OTHERS => '0');
--      imm8_2   <= (OTHERS => '0');
--      imm10_2  <= (OTHERS => '0');
--      imm16_2  <= (OTHERS => '0');
--      
--      delayILB <= 'X';
      
    END IF; -- ENABLE
  END IF; -- CLK
END PROCESS; 
END behave;