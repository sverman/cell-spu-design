--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   01:04:09 03/29/2013
-- Design Name:   
-- Module Name:   C:/Users/SAMBHAV/xilinx_work/ILB_SPU/test_ILB.vhd
-- Project Name:  ILB_SPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ILB_SPU
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_frwdmux IS
END test_frwdmux;
 ARCHITECTURE behavior OF test_frwdmux IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT frwd_mux
    PORT(
  		fwe1	: in	std_logic_vector ( 0 to 127);
		fwe2	: in	std_logic_vector ( 0 to 127);
		fwe3	: in	std_logic_vector ( 0 to 127);
		fwo1	: in	std_logic_vector ( 0 to 127);
		fwo2	: in	std_logic_vector ( 0 to 127);
		fwo3	: in	std_logic_vector ( 0 to 127);
		rf	  : in	std_logic_vector ( 0 to 127);
		frwd_cntrl	: in	bit_vector(0 to 2);
		regout : out	std_logic_vector (0 to 127)
        );
    END COMPONENT;
    

 	  signal fwe1	: 	std_logic_vector ( 0 to 127);
 	  signal fwe2	: 	std_logic_vector ( 0 to 127);
 	  signal fwe3	: 	std_logic_vector ( 0 to 127);
		signal fwo1	: 	std_logic_vector ( 0 to 127);
		signal fwo2	: 	std_logic_vector ( 0 to 127);
		signal fwo3	: 	std_logic_vector ( 0 to 127);
		signal rf	  : 	std_logic_vector ( 0 to 127);
	signal frwd_cntrl 	: 	bit_vector(0 to 2);
		signal regout : 	std_logic_vector (0 to 127);

   -- Clock period definitions
   constant clk_period : time := 50 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: frwd_mux PORT MAP (
 fwe1 => fwe1,
 fwe2 => fwe2,
 fwe3 => fwe3,
 fwo1 => fwo1,
 fwo2 => fwo2,
 fwo3 => fwo3,
 rf   => rf,
 frwd_cntrl => frwd_cntrl,
 regout => regout
 );

   -- Clock process definitions
 --  clk_process :process
 --  begin
	--	clk <= '0';
--		wait for clk_period/2;
--		clk <= '1';
--		wait for clk_period/2;
--   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
  
    
		  frwd_cntrl <= "000";
      fwe1 <= X"12ff345F6789F10212ff345F6789F102";
    wait for 50 ns;
	   frwd_cntrl <= "001";
      fwe2 <= X"12ff345F6789F1023987F546432F12ff";
  wait for 50 ns;
      frwd_cntrl <= "010";
      fwe3 <= X"12ff345F6789F1023987F54643201200";
    wait for 50 ns;
      frwd_cntrl <= "111";
      rf <= X"12ff345F6789F1023987F54643231211";
      wait for 50 ns;
      frwd_cntrl <= "110";
      rf <= X"12ff345F6789F1023987F54643661222";
    
      wait;
   end process;

END;
