LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY byte IS
  PORT (
    ra, rb, rc, rt : IN STD_LOGIC_VECTOR(0 TO 127);
    opcode : IN STD_LOGIC_VECTOR(0 TO 4);
    enable : IN STD_LOGIC;
    clk: IN STD_LOGIC;
    result : OUT STD_LOGIC_VECTOR(0 TO 127);
    zero : OUT STD_LOGIC);
END byte;

ARCHITECTURE behave OF byte IS
  --component registers


signal rt_uns_sig : std_logic_vector(0 TO 127);-- : = (OTHERS => '0');

BEGIN    
 
  PROCESS(clk)
   -- declaration of variables
  VARIABLE ra_uns : UNSIGNED(0 TO 127);
  VARIABLE rb_uns : UNSIGNED(0 TO 127);
  VARIABLE rc_uns : UNSIGNED(0 TO 127);
  VARIABLE rt_uns : UNSIGNED(0 TO 127); 
  VARIABLE z_uns : UNSIGNED(0 TO 0);
  VARIABLE c : UNSIGNED(0 TO 7);
  VARIABLE b : UNSIGNED(0 TO 7);
  VARIABLE r : UNSIGNED(0 TO 127);
  VARIABLE i : INTEGER;
  VARIABLE s : INTEGER;
  VARIABLE x : INTEGER;
  VARIABLE y : INTEGER;
  VARIABLE z : INTEGER;  
  
  BEGIN

  IF (RISING_EDGE(CLK)) THEN -- clk
   -- IF enable = '1' THEN
 
  -- initialize
  ra_uns := UNSIGNED(ra);
  rb_uns := UNSIGNED(rb); 
  rc_uns := UNSIGNED(rc);
  rt_uns := UNSIGNED(rt);
  z_uns(0) := '0';

  -- select desired operation
  CASE opcode IS

  -- (1) cntb (Count Ones in Bytes)
  WHEN "00001" =>  
  FOR i IN 0 TO 15 LOOP
    c(0 TO 7) := X"00";
    b(0 TO 7) := ra_uns((i * 8) TO (7 + (i * 8)));
  
    FOR m IN 0 TO 7 LOOP
      IF b(m) = '1' THEN
        c := c + 1;
      END IF;
    END LOOP;
    rt_uns((i * 8) TO (7 + (i * 8))) := c(0 TO 7);
  END LOOP;
  
  -- (2) sumb (Sum Bytes Into Halfwords)
  -- (Run time error. Assignment of 8bit value to 16bit value)
  WHEN "00010" =>
--  rt_uns(0 TO 15) := rb_uns(0 TO 7) + rb_uns(8 TO 15) + rb_uns(16 TO 23) + rb_uns(24 TO 31); 
--  rt_uns(16 TO 31) := ra_uns(0 TO 7) + ra_uns(8 TO 15) + ra_uns(16 TO 23) + ra_uns(24 TO 31);
--  rt_uns(32 TO 47) := rb_uns(32 TO 39) + rb_uns(40 TO 47) + rb_uns(48 TO 55) + rb_uns(56 TO 63); 
--  rt_uns(48 TO 63) := ra_uns(32 TO 39) + ra_uns(40 TO 47) + ra_uns(48 TO 55) + ra_uns(56 TO 63);
--  rt_uns(64 TO 79) := rb_uns(64 TO 71) + rb_uns(72 TO 79) + rb_uns(80 TO 87) + rb_uns(88 TO 95); 
--  rt_uns(80 TO 95) := ra_uns(64 TO 71) + ra_uns(72 TO 79) + ra_uns(80 TO 87) + ra_uns(88 TO 95);
--  rt_uns(96 TO 111) := rb_uns(96 TO 103) + rb_uns(104 TO 111) + rb_uns(112 TO 119) + rb_uns(120 TO 127); 
--  rt_uns(112 TO 127) := ra_uns(96 TO 103) + ra_uns(104 TO 111) + ra_uns(112 TO 119) + ra_uns(120 TO 127); 
  
  -- (3) ceqb (Compare Equal Bytes)
  WHEN "00011" =>
  FOR i IN 0 TO 15 LOOP  
    x := (i * 8);
    IF ra_uns(x TO (x + 7)) = rb_uns(x TO (x + 7)) THEN
      rt_uns(x TO (x + 7)) := X"FF";
    ELSE
      rt_uns(x TO (x + 7)) := X"00";
    END IF;
  END LOOP;
  
  -- (4) rotqby (Rotate Quadword by Bytes)
  WHEN "00100" =>
    s := TO_INTEGER(rb_uns(28 TO 31));
    FOR i IN 0 TO 15 LOOP
      
      x := (i * 8);
      y := ((i + s) * 8);
      z := ((i + s - 16) * 8);
      
      IF i + s < 16 THEN
        r(x TO (x + 7)) := ra_uns(y TO (y + 7));
      ELSE
        r(x TO (x + 7)) := ra_uns(z TO (z + 7));
      END IF;
    END LOOP;
    rt_uns(0 TO 127) := r(0 TO 127);

   -- (5) shlqby (Shift Left Quadword by Bytes)
  WHEN "00101" =>
    s := TO_INTEGER(rb_uns(27 TO 31));
    FOR i IN 0 TO 15 LOOP
      
      x := (i * 8);
      y := ((i + s) * 8);
      z := ((i + s - 16) * 8);
      
      IF i + s < 16 THEN
        r(x TO (x + 7)) := ra_uns(y TO (y + 7));
      ELSE
        r(x TO (x + 7)) := X"00";
      END IF;
    END LOOP;
    rt_uns(0 TO 127) := r(0 TO 127);
      
  -- others 
  WHEN OTHERS => rt_uns := (OTHERS => 'X');
  END CASE;

  -- set zero bit if result equals zero 
  IF TO_INTEGER(rt_uns) = 0 THEN
    z_uns(0) := '1';
  ELSE
    z_uns(0) := '0';
  END IF; 
  -- assign variables to output signals
    ----------------------------------------------  
  IF ENABLE = '1'
  THEN
  rt_uns_sig <= STD_LOGIC_VECTOR(rt_uns);
  ELSE
  rt_uns_sig <= (others => 'Z');
  END IF;
  result <= rt_uns_sig;-- after 10 ns;
  zero <= z_uns(0); 
  ELSE
  --result <= (OTHERS => 'Z');
  END IF; 
 -- END IF; -- enable
  END PROCESS; 
END behave;