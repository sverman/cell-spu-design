LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE work.int_array.ALL;
entity DH2 is
port (
    opcode_0_DH  : IN STD_LOGIC_VECTOR(0 TO 8);   
    ra0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt0_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_0_DH    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_0_DH    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_0_DH   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_0_DH   : IN STD_LOGIC_VECTOR(0 TO 15);
    RegWr_ins0_DH  : IN BIT;
    MemRd_ins0_DH  : IN BIT;
    MemWr_ins0_DH  : IN BIT;
    
    opcode_1_DH  : IN STD_LOGIC_VECTOR(0 TO 8);
    ra1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_DH  : IN STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_DH    : IN STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_DH    : IN STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_DH   : IN STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_DH   : IN STD_LOGIC_VECTOR(0 TO 15);    
    RegWr_ins1_DH  : IN BIT;
    MemRd_ins1_DH  : IN BIT;
    MemWr_ins1_DH  : IN BIT;
    
    PC_DH : IN STD_LOGIC_VECTOR(0 TO 31);
    
   
    opcode_0_ISS  : OUT STD_LOGIC_VECTOR(0 TO 8);    
    ra0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt0_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_0_ISS    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_0_ISS    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_0_ISS   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_0_ISS   : OUT STD_LOGIC_VECTOR(0 TO 15);
    RegWr_ins0_ISS  : OUT BIT;
    MemRd_ins0_ISS  : OUT BIT;
    MemWr_ins0_ISS  : OUT BIT;
    
    
    opcode_1_ISS  : OUT STD_LOGIC_VECTOR(0 TO 8);
    ra1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rb1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rc1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    rt1_addr_ISS  : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm7_1_ISS    : OUT STD_LOGIC_VECTOR(0 TO 6);
    imm8_1_ISS    : OUT STD_LOGIC_VECTOR(0 TO 7);
    imm10_1_ISS   : OUT STD_LOGIC_VECTOR(0 TO 9);
    imm16_1_ISS   : OUT STD_LOGIC_VECTOR(0 TO 15);
   
    RegWr_ins1_ISS  : OUT BIT;
    MemRd_ins1_ISS  : OUT BIT;
    MemWr_ins1_ISS  : OUT BIT;
    
    PC_ISS : OUT STD_LOGIC_VECTOR(0 TO 31);
  
    reset : in std_logic := '0';
    firstInstr : IN BIT;
    enable : IN STD_LOGIC;
    clk: IN STD_LOGIC
  );
end DH2;

architecture behavioral of DH2 is
type state_type is (s0,s1,s2,s3,s4,s5,s6,s7,s8);  --type of state machine.
signal current_s, next_s: state_type := s0;  --current and next state declaration.
signal counter_arr : int_arr := (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
signal fwd_signal  : int_arr := (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0); 
    signal stall0    : INTEGER :=0;
    signal stall1    : INTEGER :=0;
    signal inUse   : UNSIGNED(0 TO 127);
    
    signal issue0, issue1 : integer :=0;
    
        
    signal fwe_1_enable : BIT;
    signal fwe_2_enable : BIT;
    signal fwe_3_enable : BIT;
    
    signal fwo_1_enable : BIT;
    signal fwo_2_enable : BIT;
    signal fwo_3_enable : BIT;
    
    signal frwd_cntrl_even	: 	bit_vector (0 to 2);
    signal frwd_cntrl_odd	 : 	bit_vector (0 to 2);
    
    
    
    begin
    type1 : process (clk)--,reset,counter_arr,current_s,next_s)
    VARIABLE opcode0 : UNSIGNED(0 TO 8);
    VARIABLE opcode1 : UNSIGNED(0 TO 8);
    VARIABLE ra0 : UNSIGNED(0 TO 6);
    VARIABLE rb0 : UNSIGNED(0 TO 6);
    VARIABLE rc0 : UNSIGNED(0 TO 6);
    VARIABLE rt0 : UNSIGNED(0 TO 6);
    VARIABLE ra1 : UNSIGNED(0 TO 6);
    VARIABLE rb1 : UNSIGNED(0 TO 6);
    VARIABLE rc1 : UNSIGNED(0 TO 6);
    VARIABLE rt1 : UNSIGNED(0 TO 6);
    VARIABLE firstInst : BIT;

    
   
    begin

 ------------------------------------------------------------------------------------------- 

      opcode0 := UNSIGNED(opcode_0_DH);
      opcode1 := UNSIGNED(opcode_1_DH);
      ra0 := UNSIGNED(ra0_addr_DH);
      rb0 := UNSIGNED(rb0_addr_DH);
      rc0 := UNSIGNED(rc0_addr_DH);
      rt0 := UNSIGNED(rt0_addr_DH);
      ra1 := UNSIGNED(ra1_addr_DH);
      rb1 := UNSIGNED(rb1_addr_DH);
      rc1 := UNSIGNED(rc1_addr_DH);
      rt1 := UNSIGNED(rt1_addr_DH);
   
    
  FOR i IN 0 TO 127 LOOP
  IF counter_arr(i) > 0 THEN
  counter_arr(i) <= counter_arr(i) - 1;
  END IF;
  END LOOP;

    
  FOR j IN 0 TO 127 LOOP
  IF fwd_signal(j) > 0 THEN
  fwd_signal(j) <= fwd_signal(j) - 1;
  END IF;
  END LOOP;
 
  
                                    ------------------------------------------------------------
   
                                    ------------------------------------------------------------  
  --if opcode_0_DH'event or opcode_1_DH'event then
-- current_s <= s0;  --default state on reset.
--else
   if clk'event then
current_s <= next_s;   --state change.
--end if;
                                                        --state machine process.
  case current_s is
   ---------------------------------------------------------------------------------------------------------
     when s0 =>        --when current state is "s0"
     ----------------------------------------------------------- -- check for instruction type and set counter value accordindly
  
 
  ----------------  --- byte , perm
  
 
 
        IF rt1 = ra0 OR rt1 = rb0 OR rt1 = rc0 THEN

      --   counter_arr(TO_INTEGER(rt1)) <= 1;
          next_s <= s2;
        elsIF rt0 = ra1 OR rt0 = rb1 OR rt0 = rc1 
       THEN
          -- Stall instruction 1 -- Issue instruction 2 -- Issue instruction 1 in the next cycle
          -- counter_arr(TO_INTEGER(rt0)) <= 1;
          next_s <= s3;
        else -- issue both instructions no hazard found
        
          next_s <= s1;
        END IF;

------------------------------------------------------------------------------------------------------------
     when s1 =>        --when current state is "s1"
 --   if counter_arr(TO_INTEGER(rt0)) = 0 and counter_arr(TO_INTEGER(rt1)) = 0 then
      next_s <= s8;
   -- else
     -- next_s <= s0;
   --end if;
--------------------------------------------------------------------------------------------------------
    when s2 =>       --when current state is "s2"
    next_s <= s4;
---------------------------------------------------------------------------------
  when s3 =>         --when current state is "s3"
-- if  counter_arr(TO_INTEGER(rt0)) > 0
 --then
  next_s <= s5;
--else 
 -- next_s <= s6;
--end if;
    ------------------------------------------------------------------------------------   
    when s4 =>         --when current state is "s4"
    if counter_arr(TO_INTEGER(rt1)) > 0
    then
    if fwd_signal (TO_INTEGER(rt1))>0
    then
    next_s <= s4;
    elsif
    fwd_signal(TO_INTEGER(rt1)) =0 then
    next_s <= s7;
    end if;
    else
    next_s <= s7;
    end if;
    ------------------------------------------------------------------------------------   
    when s5 =>         --when current state is "s5"
    if counter_arr(TO_INTEGER(rt0)) > 0 -- check  condition
    then
    if fwd_signal (TO_INTEGER(rt0))>0
    then
  
    next_s <= s5;
    elsif
    fwd_signal(TO_INTEGER(rt0)) =0 then
    next_s <= s6;
    end if;
    else
    next_s <= s6;
    end if;
    ---------------------------------------------------------------------------------------  
   when s6 =>         --when current state is "s6"
   next_s <= s8;
    --------------------------------------------------------------------------------------
   when s7 =>         --when current state is "s7"
   next_s <= s8;
    ---------------------------------------------------------------------------------------  
   when s8 =>         --when current state is "s8"
   if enable ='1' then
   if opcode_0_DH (5 to 7) = "111" then -- byte
   counter_arr(TO_INTEGER(rt0)) <= 9;  -- output avaiable at forwarding unit 1 after 5 cycles
   fwd_signal(TO_INTEGER(rt0)) <=5;
   end if;  
   if opcode_1_DH (5 to 7) = "001" then -- perm
   counter_arr(TO_INTEGER(rt1)) <= 9;  -- output avaiable at forwarding unit 1 after 5 cycles 
   fwd_signal(TO_INTEGER(rt0)) <=5;
   end if;
   if opcode_0_DH (5 to 7) = "101" then -- fxu
   counter_arr(TO_INTEGER(rt0)) <= 10;  -- output avaiable at forwarding unit 1 after 5 cycles
   fwd_signal(TO_INTEGER(rt0)) <=7;
   end if;  
   if opcode_1_DH (5 to 7) = "100" then -- bra
   counter_arr(TO_INTEGER(rt1)) <= 10;  -- output avaiable at forwarding unit 1 after 5 cycles
   fwd_signal(TO_INTEGER(rt0)) <=7;
   end if;
   if opcode_0_DH (5 to 7) = "011" then -- fpu
   counter_arr(TO_INTEGER(rt0)) <= 12;  -- output avaiable at forwarding unit 1 after 5 cycles
   fwd_signal(TO_INTEGER(rt0)) <=9;
   end if;  
   if opcode_1_DH (5 to 7) = "010" then -- lsu
   counter_arr(TO_INTEGER(rt1)) <= 12;  -- output avaiable at forwarding unit 1 after 5 cycles
   fwd_signal(TO_INTEGER(rt0)) <=9;
   end if;
-----
if issue0 =1 and  issue1 =1 then
next_s <= s0;
end if;
end if;
  ------------------------------------------------------------------------------------------
  when others =>
  -- next_s <= s8;
 ---------------------------------------------------------------------------------------  
  end case;
  -------------------------------------------------------------------------------------
  --DEPENDING UPON THE STATE OUTPUTS ISSUE THE INSTRUCTIONS
  --CASE issue_0 IS 1
       IF issue0 = 1 THEN
        opcode_0_ISS <= opcode_0_DH; 
        ra0_addr_ISS <= ra0_addr_DH;
        rb0_addr_ISS <= rb0_addr_DH;
        rc0_addr_ISS <= rc0_addr_DH;
        rt0_addr_ISS <= rt0_addr_DH;
        imm7_0_ISS   <= imm7_0_DH;
        imm8_0_ISS   <= imm8_0_DH;
        imm10_0_ISS  <= imm10_0_DH;
        imm16_0_ISS  <= imm16_0_DH;
        RegWr_ins0_ISS <= RegWr_ins0_DH;
        MemRd_ins0_ISS <= MemRd_ins0_DH;
        MemWr_ins0_ISS <= MemWr_ins0_DH;
        PC_ISS <= PC_DH;
      else
        opcode_0_ISS <= (OTHERS => 'Z');  
        ra0_addr_ISS <= (OTHERS => 'Z');
        rb0_addr_ISS <= (OTHERS => 'Z');
        rc0_addr_ISS <= (OTHERS => 'Z');
        rt0_addr_ISS <= (OTHERS => 'Z');
        imm7_0_ISS   <= (OTHERS => 'Z');
        imm8_0_ISS   <= (OTHERS => 'Z');
        imm10_0_ISS  <= (OTHERS => 'Z');
        imm16_0_ISS  <= (OTHERS => 'Z');
        RegWr_ins0_ISS <= '0';
        MemRd_ins0_ISS <= '0';
        MemWr_ins0_ISS <= '0';
        PC_ISS <= (OTHERS => 'Z');
      end if;
  -------------------------------------------------------------------------------------------------------------      
       IF issue1 = 1 THEN
       opcode_1_ISS <= opcode_1_DH;
        ra1_addr_ISS <= ra1_addr_DH;
        rb1_addr_ISS <= rb1_addr_DH;
        rc1_addr_ISS <= rc1_addr_DH;
        rt1_addr_ISS <= rt1_addr_DH; 
        imm7_1_ISS   <= imm7_1_DH;
        imm8_1_ISS   <= imm8_1_DH;
        imm10_1_ISS  <= imm10_1_DH;
        imm16_1_ISS  <= imm16_1_DH;
        RegWr_ins1_ISS <= RegWr_ins1_DH;
        MemRd_ins1_ISS <= MemRd_ins1_DH;
        MemWr_ins1_ISS <= MemWr_ins1_DH;
        PC_ISS <= PC_DH;
        else
        opcode_1_ISS <= (OTHERS => 'Z');
        ra1_addr_ISS <= (OTHERS => 'Z');
        rb1_addr_ISS <= (OTHERS => 'Z');
        rc1_addr_ISS <= (OTHERS => 'Z');
        rt1_addr_ISS <= (OTHERS => 'Z');
        imm7_1_ISS   <= (OTHERS => 'Z');
        imm8_1_ISS   <= (OTHERS => 'Z');
        imm10_1_ISS  <= (OTHERS => 'Z');
        imm16_1_ISS  <= (OTHERS => 'Z');
        RegWr_ins1_ISS  <= '0';
        MemRd_ins1_ISS  <= '0';
        MemWr_ins1_ISS  <= '0';
        PC_ISS <= (OTHERS => 'Z');
        end if; 
       ---------------------------------------------------------------------------------------------------------------
end if; 
end process;

type2 : process (clk,current_s,counter_arr,next_s)
begin
if (rising_edge(clk))
then  
case current_s is

when S0 => issue0 <= 0; issue1 <= 0;
when S1 => issue0 <= 1; issue1 <= 1;
when S2 => issue0 <= 1; issue1 <= 0;
when S3 => issue0 <= 0; issue1 <= 1;
when S4 => issue0 <= 0; issue1 <= 0;
when S5 => issue0 <= 0; issue1 <= 0;
when S6 => issue0 <= 1; issue1 <= 0;
when S7 => issue0 <= 0; issue1 <= 1;
when S8 => issue0 <= 0; issue1 <= 0;
end case;
end  if;
end process;
 end behavioral;