----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:27:49 02/14/2013 
-- Design Name: 
-- Module Name:    ILB_SPU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--library avm;
--USE avm.ALL;
--library verilog;
--use verilog.vl_types.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ILB_SPU_dump is
    Port (	clk : in STD_LOGIC;
				ins_in : in  STD_LOGIC_VECTOR (511 downto 0); -- 64 B read from Latch. 
				in_enable : in STD_LOGIC;
				
				out_enable : in STD_LOGIC;
				ins_out : out STD_LOGIC_VECTOR (63 downto 0)); -- 2 instructions in 1 cycle
				
end ILB_SPU;

architecture Behavioral of ILB_SPU is
constant clk_period : time := 50 ns;
signal sig_datain : STD_LOGIC_VECTOR (511 downto 0);
signal sig_dataout : STD_LOGIC_VECTOR (63 downto 0);
signal sig_chk1 : STD_LOGIC_VECTOR (63 downto 0);
signal sig_chk2 : STD_LOGIC_VECTOR (63 downto 0);
signal sig_chk3 : STD_LOGIC_VECTOR (63 downto 0);
signal d : INTEGER := 0;
begin

sig_chk2 <= sig_datain (127 downto 64);
sig_chk1 <= sig_datain (63 downto 0);

sig_chk3 <= sig_chk2;
sig_chk3 <= sig_chk1;	


mycode: process (clk, ins_in)
--variable i : INTEGER:=0;
--variable count : INTEGER:= -64;



begin

if clk = '1' then

--if sig_datain(127 downto 64) = (127 downto 64 => 'Z')
--then
--sig_chk1 <= sig_datain (63 downto 0);
--elsif sig_datain(63 downto 0) = (63 downto 0 => 'Z')
--then
--sig_chk2 <= sig_datain (127 downto 64);
--end if;

--sig_chk1 <= sig_chk2;
--sig_chk1 <= sig_chk3;
			
		if in_enable = '1' then
				
		sig_datain <= ins_in;
		
		else
		sig_datain <= (sig_datain'range =>'X');
		end if;
		if out_enable = '1'
	then
	ins_out(31 downto 0) <= sig_chk3( 31 downto 0);
	ins_out(63 downto 32) <= sig_chk3( 63 downto 32);
		
		end if;
	
		else 
	
		ins_out (63 downto 0) <= (OTHERS =>'Z');
		
		

end if;
--end if;
end process mycode;

end Behavioral;

